<?php
namespace app_virama_karya\console\controllers;

use yii\console\Controller;
use technosmart\models\Permission;

class InitController extends Controller
{
    public $dir = [
        'app_virama_karya\controllers' => '@app_virama_karya/controllers',
    ];

    public function actionIndex()
    {
        \Yii::$app->runAction('cache/flush-all');

        \Yii::$app->cache->cachePath = \Yii::getAlias('@app_virama_karya/runtime/cache');
        \Yii::$app->runAction('cache/flush-all');

        if (Permission::initAllController($this->dir))
            return 0;
        else
            return 1;
    }
}