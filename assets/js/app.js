if(typeof Vue == 'function') {
    if(typeof VueDefaultValue == 'object') {
        Vue.use(VueDefaultValue);
    }

    if(typeof Vue.http == 'function') {
        Vue.http.headers.common['X-CSRF-Token'] = csrfToken;
    }

    Vue.directive('init', {
        inserted: function(el) {
            pluginInit(el);
        },
        componentUpdated: function(el) {
            pluginInit(el);
        },
    });

    var vm = new Vue({
        el: '#app',
        data: {
            tenaga_ahli: {
                tenagaAhliPendidikans: [],
                tenagaAhliKeahlians: [],
            },
            proyek: {
                proyekTenagaAhlis: [],
            },
            option_tenaga_ahli: [],
            option_penugasan: [],
        },
        methods: {
            addTenagaAhliPendidikan: function() {
                pluginInit();
                this.tenaga_ahli.tenagaAhliPendidikans.push({
                    id: null,
                    id_tenaga_ahli: null,
                    jenjang: null,
                    jurusan: null,
                    tahun: null,
                    nomor_ijazah: null,
                });
            },
            removeTenagaAhliPendidikan: function(i, isNew) {
                pluginInit();
                if (this.tenaga_ahli.tenagaAhliPendidikans[i].id == null)
                    this.tenaga_ahli.tenagaAhliPendidikans.splice(i, 1);
                else
                    this.tenaga_ahli.tenagaAhliPendidikans[i].id*=-1;
            },
            addTenagaAhliKeahlian: function() {
                pluginInit();
                this.tenaga_ahli.tenagaAhliKeahlians.push({
                    id: null,
                    id_tenaga_ahli: null,
                    keahlian: null,
                    sertifikat: null,
                });
            },
            removeTenagaAhliKeahlian: function(i, isNew) {
                pluginInit();
                if (this.tenaga_ahli.tenagaAhliKeahlians[i].id == null)
                    this.tenaga_ahli.tenagaAhliKeahlians.splice(i, 1);
                else
                    this.tenaga_ahli.tenagaAhliKeahlians[i].id*=-1;
            },
            addProyekTenagaAhli: function() {
                pluginInit();
                this.proyek.proyekTenagaAhlis.push({
                    id: null,
                    id_proyek: null,
                    id_tenaga_ahli: null,
                    id_penugasan: null,
                    periode_awal: null,
                    periode_akhir: null,
                });
            },
            removeProyekTenagaAhli: function(i, isNew) {
                pluginInit();
                if (this.proyek.proyekTenagaAhlis[i].id == null)
                    this.proyek.proyekTenagaAhlis.splice(i, 1);
                else
                    this.proyek.proyekTenagaAhlis[i].id*=-1;
            },
        },
    });
}