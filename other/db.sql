-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE DATABASE `virama-karya` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `virama-karya`;

DROP TABLE IF EXISTS `auth_assignment`;
CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('admin',	'1',	1501062910);

DROP TABLE IF EXISTS `auth_item`;
CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('admin',	1,	'Administrator',	NULL,	NULL,	1461048991,	1461048991),
('menu:create',	2,	'Menu Create',	NULL,	NULL,	1461053534,	1461053534),
('menu:delete',	2,	'Menu Delete',	NULL,	NULL,	1461053534,	1461053534),
('menu:update',	2,	'Menu Update',	NULL,	NULL,	1461053534,	1461053534),
('menu:view',	2,	'Menu View',	NULL,	NULL,	1461053534,	1461053534),
('user:create',	2,	'User Create',	NULL,	NULL,	1461053534,	1461053534),
('user:permissions',	2,	'User Permissions',	NULL,	NULL,	1461048991,	1461048991),
('user:stat',	2,	'Change Status User',	NULL,	NULL,	1461053534,	1461053534),
('user:update',	2,	'User Update',	NULL,	NULL,	1461053534,	1461053534),
('user:view',	2,	'User View',	NULL,	NULL,	1461053534,	1461053534);

DROP TABLE IF EXISTS `auth_item_child`;
CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('admin',	'menu:create'),
('admin',	'menu:delete'),
('admin',	'menu:update'),
('admin',	'menu:view'),
('admin',	'user:create'),
('admin',	'user:permissions'),
('admin',	'user:stat'),
('admin',	'user:update'),
('admin',	'user:view');

DROP TABLE IF EXISTS `auth_rule`;
CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `bidang`;
CREATE TABLE `bidang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_bidang` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `cabang`;
CREATE TABLE `cabang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_cabang` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `configuration`;
CREATE TABLE `configuration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `type` varchar(8) DEFAULT NULL,
  `value` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO `configuration` (`id`, `name`, `type`, `value`) VALUES
(1,	'app.name',	'var',	'Virama Karya'),
(2,	'app.description',	'var',	'Technosmart is a yii2 project starter'),
(3,	'app.keywords',	'var',	'php, yii2, mysql, vue, technopro, technosmart, technoart'),
(4,	'app.owner',	'var',	'Technopro'),
(5,	'app.author',	'var',	'Technopro'),
(6,	'enable.permission',	'bool',	'false'),
(7,	'enable.init-permission',	'bool',	'false');

DROP TABLE IF EXISTS `configuration_file`;
CREATE TABLE `configuration_file` (
  `target` varchar(32) NOT NULL,
  `alias_upload_root` varchar(64) NOT NULL,
  `alias_download_base_url` varchar(64) NOT NULL,
  PRIMARY KEY (`target`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `configuration_file` (`target`, `alias_upload_root`, `alias_download_base_url`) VALUES
('dev-file',	'@upload-dev-file',	'@download-dev-file');

DROP TABLE IF EXISTS `divisi`;
CREATE TABLE `divisi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_divisi` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `identitas_perusahaan`;
CREATE TABLE `identitas_perusahaan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_perusahaan` varchar(64) NOT NULL COMMENT 'data administrasi',
  `status` enum('Pusat','Cabang') NOT NULL COMMENT 'data administrasi',
  `alamat` text NOT NULL COMMENT 'data administrasi',
  `nomor_telpon` varchar(16) NOT NULL COMMENT 'data administrasi',
  `nomor_fax` varchar(16) NOT NULL COMMENT 'data administrasi',
  `email_1` varchar(32) NOT NULL COMMENT 'data administrasi',
  `email_2` varchar(32) NOT NULL COMMENT 'data administrasi',
  `akta_pendirian_perusahaan_nomor_akta` varchar(4) NOT NULL COMMENT 'landasan hukum pendirian badan usaha',
  `akta_pendirian_perusahaan_tanggal` date NOT NULL COMMENT 'landasan hukum pendirian badan usaha',
  `akta_pendirian_perusahaan_nama_notaris` varchar(64) NOT NULL COMMENT 'landasan hukum pendirian badan usaha',
  `akta_pendirian_perusahaan_nomor_pengesahan` varchar(64) NOT NULL COMMENT 'landasan hukum pendirian badan usaha',
  `akta_pendirian_perusahaan_tanggal_pengesahan` varchar(64) NOT NULL COMMENT 'landasan hukum pendirian badan usaha',
  `akta_perubahan_terakhir_nomor_akta` varchar(4) NOT NULL COMMENT 'landasan hukum pendirian badan usaha',
  `akta_perubahan_terakhir_nama_notaris` varchar(64) NOT NULL COMMENT 'landasan hukum pendirian badan usaha',
  `akta_perubahan_terakhir_tanggal` date NOT NULL COMMENT 'landasan hukum pendirian badan usaha',
  `akta_perubahan_terakhir_nomor_pengesahan` varchar(64) NOT NULL COMMENT 'landasan hukum pendirian badan usaha',
  `akta_perubahan_terakhir_tanggal_pengesahan` varchar(64) NOT NULL COMMENT 'landasan hukum pendirian badan usaha',
  `iujk_masa_berlaku_dari` date NOT NULL COMMENT 'ijin_usaha',
  `iujk_masa_berlaku_sampai` date NOT NULL COMMENT 'ijin_usaha',
  `iujk_instansi_pemberi` varchar(64) NOT NULL COMMENT 'ijin_usaha',
  `iujk_perencanaan` varchar(64) NOT NULL COMMENT 'ijin_usaha',
  `iujk_pengawasan` varchar(64) NOT NULL COMMENT 'ijin_usaha',
  `iujk_konsultansi` varchar(64) NOT NULL COMMENT 'ijin_usaha',
  `sbu_konstruksi_masa_berlaku_dari` date NOT NULL COMMENT 'ijin_usaha',
  `sbu_konstruksi_masa_berlaku_sampai` date NOT NULL COMMENT 'ijin_usaha',
  `sbu_konstruksi_instansi_pemberi_izin` varchar(128) NOT NULL COMMENT 'ijin_usaha',
  `sbu_konstruksi_perencanaan_arsitektur` varchar(128) NOT NULL COMMENT 'ijin_usaha',
  `sbu_konstruksi_pengawasan_arsitektur` varchar(128) NOT NULL COMMENT 'ijin_usaha',
  `sbu_konstruksi_jasa_konsultansi_lainnya` varchar(128) NOT NULL COMMENT 'ijin_usaha',
  `sbu_konstruksi_perencana_penataan_ruang` varchar(128) NOT NULL COMMENT 'ijin_usaha',
  `sbu_konstruksi_pengawasan_penataan_ruang` varchar(128) NOT NULL COMMENT 'ijin_usaha',
  `sbu_konstruksi_perencanaan_rekayasa` varchar(128) NOT NULL COMMENT 'ijin_usaha',
  `sbu_konstruksi_pengawasan_rekayasa` varchar(128) NOT NULL COMMENT 'ijin_usaha',
  `sbu_konstruksi_konsultan_spesialis` varchar(128) NOT NULL COMMENT 'ijin_usaha',
  `sbu_non_konstruksi_ljspdbt` varchar(64) NOT NULL COMMENT 'ijin_usaha_sbu_non_konstruksi',
  `sbu_non_konstruksi_ljskm` varchar(64) NOT NULL COMMENT 'ijin_usaha_sbu_non_konstruksi',
  `sbu_non_konstruksi_ljskm_masa_berlaku_dari` date NOT NULL COMMENT 'ijin_usaha_sbu_non_konstruksi',
  `sbu_non_konstruksi_ljskm_masa_berlaku_sampai` date NOT NULL COMMENT 'ijin_usaha_sbu_non_konstruksi',
  `sbu_non_konstruksi_ljskm_instansi_pemberi_izin` varchar(64) NOT NULL COMMENT 'ijin_usaha_sbu_non_konstruksi',
  `sbu_non_konstruksi_pengembangan_pertanian_dan_pedesaan` varchar(64) NOT NULL COMMENT 'ijin_usaha_sbu_non_konstruksi',
  `sbu_non_konstruksi_transportasi` varchar(64) NOT NULL COMMENT 'ijin_usaha_sbu_non_konstruksi',
  `sbu_non_konstruksi_telematika` varchar(64) NOT NULL COMMENT 'ijin_usaha_sbu_non_konstruksi',
  `sbu_non_konstruksi_pendidikan` varchar(64) NOT NULL COMMENT 'ijin_usaha_sbu_non_konstruksi',
  `sbu_non_konstruksi_pendidikan_masa_berlaku_dari` date NOT NULL COMMENT 'ijin_usaha_sbu_non_konstruksi',
  `sbu_non_konstruksi_pendidikan_masa_berlaku_sampai` date NOT NULL COMMENT 'ijin_usaha_sbu_non_konstruksi',
  `sbu_non_konstruksi_pendidikan_instansi_pemberi_izin` varchar(64) NOT NULL COMMENT 'ijin_usaha_sbu_non_konstruksi',
  `pajak_nomor_pokok_wajib_pajak` varchar(64) NOT NULL COMMENT 'pajak',
  `pajak_surat_pengukuhan_kena_pajak` varchar(64) NOT NULL COMMENT 'pajak',
  `pajak_surat_keterangan_terdaftar` varchar(64) NOT NULL COMMENT 'pajak',
  `pajak_bpptt_nomor` varchar(64) NOT NULL COMMENT 'pajak',
  `pajak_bpptt_tanggal` date NOT NULL COMMENT 'pajak',
  `pajak_skf_nomor` varchar(64) NOT NULL COMMENT 'pajak',
  `pajak_skf_tanggal` date NOT NULL COMMENT 'pajak',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `instansi`;
CREATE TABLE `instansi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_instansi` varchar(64) NOT NULL,
  `tahun_anggaran` year(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `code` varchar(32) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `enable` tinyint(4) NOT NULL DEFAULT '1',
  `title` varchar(64) NOT NULL,
  `icon` varchar(64) DEFAULT NULL,
  `url` varchar(32) DEFAULT NULL,
  `url_controller` varchar(32) DEFAULT NULL,
  `url_action` varchar(32) DEFAULT NULL,
  `param_key_1` varchar(32) DEFAULT NULL,
  `param_value_1` varchar(32) DEFAULT NULL,
  `param_key_2` varchar(32) DEFAULT NULL,
  `param_value_2` varchar(32) DEFAULT NULL,
  `param_key_3` varchar(32) DEFAULT NULL,
  `param_value_3` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

INSERT INTO `menu` (`code`, `id`, `parent`, `order`, `enable`, `title`, `icon`, `url`, `url_controller`, `url_action`, `param_key_1`, `param_value_1`, `param_key_2`, `param_value_2`, `param_key_3`, `param_value_3`) VALUES
('sidebar',	1,	NULL,	0,	1,	'Instansi',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
('sidebar',	2,	1,	0,	1,	'Daftar Instansi',	NULL,	NULL,	'instansi',	'index',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
('sidebar',	3,	1,	1,	1,	'Input Instansi',	NULL,	NULL,	'instansi',	'create',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
('sidebar',	4,	NULL,	0,	1,	'Satminkal',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
('sidebar',	5,	4,	0,	1,	'Daftar Satminkal',	NULL,	NULL,	'satminkal',	'index',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
('sidebar',	6,	4,	1,	1,	'Input Satminkal',	NULL,	NULL,	'satminkal',	'create',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
('sidebar',	7,	NULL,	0,	1,	'Satuan Kerja',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
('sidebar',	8,	7,	0,	1,	'Daftar Satuan Kerja',	NULL,	NULL,	'satuan-kerja',	'index',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
('sidebar',	9,	7,	1,	1,	'Input Satuan Kerja',	NULL,	NULL,	'satuan-kerja',	'create',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
('sidebar',	10,	NULL,	0,	1,	'Bidang',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
('sidebar',	11,	10,	0,	1,	'Daftar Bidang',	NULL,	NULL,	'bidang',	'index',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
('sidebar',	12,	10,	1,	1,	'Input Bidang',	NULL,	NULL,	'bidang',	'create',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
('sidebar',	13,	NULL,	0,	1,	'Sub Bidang',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
('sidebar',	14,	13,	0,	1,	'Daftar Sub Bidang',	NULL,	NULL,	'sub-bidang',	'index',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
('sidebar',	15,	13,	1,	1,	'Input Sub Bidang',	NULL,	NULL,	'sub-bidang',	'create',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
('sidebar',	16,	NULL,	0,	1,	'Penugasan',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
('sidebar',	17,	16,	0,	1,	'Daftar Penugasan',	NULL,	NULL,	'penugasan',	'index',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
('sidebar',	18,	16,	1,	1,	'Input Penugasan',	NULL,	NULL,	'penugasan',	'create',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
('sidebar',	19,	NULL,	0,	1,	'Cabang',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
('sidebar',	20,	19,	0,	1,	'Daftar Cabang',	NULL,	NULL,	'cabang',	'index',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
('sidebar',	21,	19,	1,	1,	'Input Cabang',	NULL,	NULL,	'cabang',	'create',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
('sidebar',	22,	NULL,	0,	1,	'Divisi',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
('sidebar',	23,	22,	0,	1,	'Daftar Divisi',	NULL,	NULL,	'divisi',	'index',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
('sidebar',	24,	22,	1,	1,	'Input Divisi',	NULL,	NULL,	'divisi',	'create',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
('sidebar',	25,	NULL,	0,	1,	'Mitra',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
('sidebar',	26,	25,	0,	1,	'Daftar Mitra',	NULL,	NULL,	'mitra',	'index',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
('sidebar',	27,	25,	1,	1,	'Input Mitra',	NULL,	NULL,	'mitra',	'create',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
('sidebar',	28,	NULL,	0,	1,	'Identitas Perusahaan',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
('sidebar',	29,	28,	0,	1,	'Lihat Data',	NULL,	NULL,	'identitas-perusahaan',	'index',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
('sidebar',	30,	28,	1,	1,	'Update Data',	NULL,	NULL,	'identitas-perusahaan',	'update',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL);

DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base',	1460611363),
('m130524_201442_init',	1461048991),
('m140506_102106_rbac_init',	1460611365);

DROP TABLE IF EXISTS `mitra_kerja`;
CREATE TABLE `mitra_kerja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_mitra_kerja` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `penugasan`;
CREATE TABLE `penugasan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_penugasan` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `satminkal`;
CREATE TABLE `satminkal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_satminkal` varchar(64) NOT NULL,
  `id_instansi` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_instansi` (`id_instansi`),
  CONSTRAINT `satminkal_ibfk_1` FOREIGN KEY (`id_instansi`) REFERENCES `instansi` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `satuan_kerja`;
CREATE TABLE `satuan_kerja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_satuan_kerja` varchar(64) NOT NULL,
  `id_instansi` int(11) NOT NULL,
  `id_satminkal` int(11) NOT NULL,
  `alamat` text NOT NULL,
  `telpon` varchar(16) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_instansi` (`id_instansi`),
  KEY `id_satminkal` (`id_satminkal`),
  CONSTRAINT `satuan_kerja_ibfk_1` FOREIGN KEY (`id_instansi`) REFERENCES `instansi` (`id`),
  CONSTRAINT `satuan_kerja_ibfk_2` FOREIGN KEY (`id_satminkal`) REFERENCES `satminkal` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `sequence`;
CREATE TABLE `sequence` (
  `target` varchar(32) NOT NULL,
  `last_sequence` varchar(64) DEFAULT NULL,
  `first` char(1) NOT NULL,
  `second` char(1) DEFAULT NULL,
  `third` char(1) DEFAULT NULL,
  PRIMARY KEY (`target`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `sequence` (`target`, `last_sequence`, `first`, `second`, `third`) VALUES
('dev-id_combination',	'8D',	'0',	'A',	'');

DROP TABLE IF EXISTS `sub_bidang`;
CREATE TABLE `sub_bidang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_sub_bidang` varchar(64) NOT NULL,
  `id_bidang` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_bidang` (`id_bidang`),
  CONSTRAINT `sub_bidang_ibfk_1` FOREIGN KEY (`id_bidang`) REFERENCES `bidang` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tenaga_ahli`;
CREATE TABLE `tenaga_ahli` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(128) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jabatan_yang_diusulkan` varchar(255) NOT NULL,
  `pengalaman_kerja` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO `tenaga_ahli` (`id`, `nama`, `tanggal_lahir`, `jabatan_yang_diusulkan`, `pengalaman_kerja`) VALUES
(1,	'Bambang Adi Riyanto, S.T., M.Eng.',	'1955-02-09',	'Ketua Tim',	30),
(3,	'Ir., Christophores Kristijatno, CES',	'1949-01-07',	'Ahli Bangunan Air',	32);

DROP TABLE IF EXISTS `tenaga_ahli_keahlian`;
CREATE TABLE `tenaga_ahli_keahlian` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_tenaga_ahli` int(11) NOT NULL,
  `keahlian` varchar(64) NOT NULL,
  `sertifikat` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_tenaga_ahli` (`id_tenaga_ahli`),
  CONSTRAINT `tenaga_ahli_keahlian_ibfk_1` FOREIGN KEY (`id_tenaga_ahli`) REFERENCES `tenaga_ahli` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO `tenaga_ahli_keahlian` (`id`, `id_tenaga_ahli`, `keahlian`, `sertifikat`) VALUES
(1,	1,	'Ahli Sumber Daya, Madya',	'SKA No: 1.2.211.2.088.10.1052300 / 19 Sep 2014'),
(4,	3,	'Ahli Teknik Sumber Daya Air - Madya',	'SKA No: 1.2.211.2.088.09.1126731 / 01 Maret 2014'),
(5,	3,	'Ahli Teknik Bendungan Besar - Madya',	'SKA No: 1.2.211.2.088.09.1126731 / 08 Agustus 2014');

DROP TABLE IF EXISTS `tenaga_ahli_pendidikan`;
CREATE TABLE `tenaga_ahli_pendidikan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_tenaga_ahli` int(11) NOT NULL,
  `jenjang` varchar(4) NOT NULL,
  `jurusan` varchar(64) NOT NULL,
  `tahun` year(4) NOT NULL,
  `nomor_ijazah` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_tenaga_ahli` (`id_tenaga_ahli`),
  CONSTRAINT `tenaga_ahli_pendidikan_ibfk_1` FOREIGN KEY (`id_tenaga_ahli`) REFERENCES `tenaga_ahli` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO `tenaga_ahli_pendidikan` (`id`, `id_tenaga_ahli`, `jenjang`, `jurusan`, `tahun`, `nomor_ijazah`) VALUES
(1,	1,	'S1',	'Teknik Sipil',	'1981',	'UGM, 12021/BS-WR/81/TS-ST/45-1213'),
(3,	1,	'S2',	'Master of Engineering',	'1989',	'Asian of Technology'),
(6,	3,	'S1',	'Teknik Sipil',	'1984',	'Univ. Atmajaya, Yogyakarta, 1884'),
(7,	3,	'S2',	'Master of Engineering',	'1988',	'Asian of Technology');

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(16) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `auth_key` varchar(32) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `status` enum('-1','0','1') NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO `user` (`id`, `username`, `name`, `email`, `auth_key`, `password_hash`, `password_reset_token`, `status`, `created_at`, `updated_at`) VALUES
(1,	'admin',	'admin pradana',	'admin@site.com',	'GxfuBv73JJP_H2M11GMroaT9AJNokAfa',	'$2y$13$6ISIMPjMz2ilR1d5B35Pbu8KhXrTt/Ncdho9DqDO0zOVeJPjAt27C',	NULL,	'1',	'0000-00-00 00:00:00',	'2017-07-26 09:55:00'),
(2,	'',	NULL,	'asdf@asdf.com',	'EzqalJr0Zuie9gYYSDchZ4K6kQpgtgGB',	'$2y$13$VTbmnS2r1YiYLLpyhod1ZeQMc/6m/t4jBTf1B7vzdalNai9zE3YHW',	NULL,	'1',	'2017-11-06 09:05:25',	'2017-11-06 09:05:25');

-- 2017-12-07 14:07:14
