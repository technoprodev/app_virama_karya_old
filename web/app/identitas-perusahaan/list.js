$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        $.fn.dataTableExt.oStdClasses.sWrapper = 'dataTables_wrapper ff-default';
        $.fn.dataTableExt.oStdClasses.sLength = 'dataTables_length form-group form-group-sm pull-left';
        $.fn.dataTableExt.oStdClasses.sLengthSelect = 'form-control';
        $.fn.dataTableExt.oStdClasses.sFilter = 'dataTables_filter form-group form-group-sm pull-right';
        $.fn.dataTableExt.oStdClasses.sFilterInput = 'form-control';
        $.fn.dataTableExt.oStdClasses.sInfo = 'dataTables_info pull-left';
        $.fn.dataTableExt.oStdClasses.sPaging = 'dataTables_paginate pull-right paging_';
        $.fn.dataTableExt.oStdClasses.sPageButtonActive = 'active';
        var el = $('.datatables');
        $.each(el, function() {
            var table = $(this).DataTable({
                "autoWidth": false,
                "deferRender": true,
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "url": fn.urlTo('identitas-perusahaan/datatables'),
                    "type": "POST"
                },
                "columns": [
                    {
                        'data': 'id',
                        'searchable': false,
                        'orderable': false,
                        render: function ( data, type, row ) {
                            return '<a href="' + fn.urlTo('identitas-perusahaan/' + data) + '" class="text-azure" modal-md="" modal-title="Data Keterangan Identitas-perusahaan ' + row.id + '"><i class="fa fa-eye"></i></a>&nbsp;&nbsp;' +
                                '<a href="' + fn.urlTo('identitas-perusahaan/update/' + data) + '" class="text-spring"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;' +
                                '<a href="' + fn.urlTo('identitas-perusahaan/delete/' + data) + '" class="text-rose" data-confirm="Are you sure you want to delete this item?" data-method="post"><i class="fa fa-trash-o"></i></a>';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        'defaultContent':'&nbsp;',
                    },
                    {'data': 'nama_perusahaan'},
                    {'data': 'status'},
                    {'data': 'alamat'},
                    {'data': 'nomor_telpon'},
                    {'data': 'nomor_fax'},
                    {'data': 'email_1'},
                    {'data': 'email_2'},
                    {'data': 'akta_pendirian_perusahaan_nomor_akta'},
                    {'data': 'akta_pendirian_perusahaan_tanggal'},
                    {'data': 'akta_pendirian_perusahaan_nama_notaris'},
                    {'data': 'akta_pendirian_perusahaan_nomor_pengesahan'},
                    {'data': 'akta_pendirian_perusahaan_tanggal_pengesahan'},
                    {'data': 'akta_perubahan_terakhir_nomor_akta'},
                    {'data': 'akta_perubahan_terakhir_nama_notaris'},
                    {'data': 'akta_perubahan_terakhir_tanggal'},
                    {'data': 'akta_perubahan_terakhir_nomor_pengesahan'},
                    {'data': 'akta_perubahan_terakhir_tanggal_pengesahan'},
                    {'data': 'iujk_masa_berlaku_dari'},
                    {'data': 'iujk_masa_berlaku_sampai'},
                    {'data': 'iujk_instansi_pemberi'},
                    {'data': 'iujk_perencanaan'},
                    {'data': 'iujk_pengawasan'},
                    {'data': 'iujk_konsultansi'},
                    {'data': 'sbu_konstruksi_masa_berlaku_dari'},
                    {'data': 'sbu_konstruksi_masa_berlaku_sampai'},
                    {'data': 'sbu_konstruksi_instansi_pemberi_izin'},
                    {'data': 'sbu_konstruksi_perencanaan_arsitektur'},
                    {'data': 'sbu_konstruksi_pengawasan_arsitektur'},
                    {'data': 'sbu_konstruksi_jasa_konsultansi_lainnya'},
                    {'data': 'sbu_konstruksi_perencana_penataan_ruang'},
                    {'data': 'sbu_konstruksi_pengawasan_penataan_ruang'},
                    {'data': 'sbu_konstruksi_perencanaan_rekayasa'},
                    {'data': 'sbu_konstruksi_pengawasan_rekayasa'},
                    {'data': 'sbu_konstruksi_konsultan_spesialis'},
                    {'data': 'sbu_non_konstruksi_ljspdbt'},
                    {'data': 'sbu_non_konstruksi_ljskm'},
                    {'data': 'sbu_non_konstruksi_ljskm_masa_berlaku_dari'},
                    {'data': 'sbu_non_konstruksi_ljskm_masa_berlaku_sampai'},
                    {'data': 'sbu_non_konstruksi_ljskm_instansi_pemberi_izin'},
                    {'data': 'sbu_non_konstruksi_pengembangan_pertanian_dan_pedesaan'},
                    {'data': 'sbu_non_konstruksi_transportasi'},
                    {'data': 'sbu_non_konstruksi_telematika'},
                    {'data': 'sbu_non_konstruksi_pendidikan'},
                    {'data': 'sbu_non_konstruksi_pendidikan_masa_berlaku_dari'},
                    {'data': 'sbu_non_konstruksi_pendidikan_masa_berlaku_sampai'},
                    {'data': 'sbu_non_konstruksi_pendidikan_instansi_pemberi_izin'},
                    {'data': 'pajak_nomor_pokok_wajib_pajak'},
                    {'data': 'pajak_surat_pengukuhan_kena_pajak'},
                    {'data': 'pajak_surat_keterangan_terdaftar'},
                    {'data': 'pajak_bpptt_nomor'},
                    {'data': 'pajak_bpptt_tanggal'},
                    {'data': 'pajak_skf_nomor'},
                    {'data': 'pajak_skf_tanggal'},
                ],
                dom: '<"clearfix"lBf><"clearfix margin-bottom-10 scroll-x"rt><"clearfix"ip>',
                "lengthMenu": [[10, 25, 50, 100, -1], ['10 entries', '25 entries', '50 entries', '100 entries', "All entries"]],
                "orderCellsTop": true,
                // "ordering": false, //
                // "order": [], //
                // "order": [[1, 'asc'], [2, 'asc']], //

                buttons: {
                    dom: {
                        container: {
                            className: 'dt-buttons pull-left hidden-sm-less'
                        },
                        button: {
                            className: 'margin-left-5 margin-bottom-5 btn btn-default btn-sm',
                            active: 'bg-azure border-azure'
                        }
                    },
                    buttons: [
                        {
                            extend: 'colvis',
                            title: 'Data show/hide',
                            text: 'Show/hide <i class="fa fa-angle-down"></i>'
                        },
                        {
                            extend: 'copy',
                            title: 'Data export',
                            text: 'Copy'
                        },
                        {
                            extend: 'csv',
                            title: 'Data export',
                            text: 'Csv'
                        },
                        {
                            extend: 'excel',
                            title: 'Data export',
                            text: 'Excel'
                        },
                        {
                            extend: 'pdf',
                            title: 'Data export',
                            text: 'Pdf'
                        },
                        {
                            extend: 'print',
                            title: 'Data export',
                            text: 'Print'
                        }/*,
                        {
                            text: 'My button',
                            action: function ( e, dt, node, config ) {
                                alert( 'Button activated' );
                            }
                        },
                        {
                            extend: 'collection',
                            text: 'Table control',
                            autoClose: true,
                            buttons: [
                                {
                                    text: 'Toggle start date',
                                    action: function ( e, dt, node, config ) {
                                        dt.column( -2 ).visible( ! dt.column( -2 ).visible() );
                                    }
                                },
                                {
                                    text: 'Toggle salary',
                                    action: function ( e, dt, node, config ) {
                                        dt.column( -1 ).visible( ! dt.column( -1 ).visible() );
                                    }
                                }
                            ]
                        }*/
                    ]
                },
                language: {
                    lengthMenu : "_MENU_",
                    search: "",
                    searchPlaceholder: "Search here",
                    buttons: {
                        copyTitle: 'Title',
                        copyKeys: 'copy keys',
                        copySuccess: {
                            _: '%d rows copied',
                            1: '1 row copied'
                        }
                    }
                },
                colReorder: true
            });

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                var that = this;
                
                var timer = null;
                dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                    var val = {
                        that : that,
                        this : this,
                    };

                    clearTimeout(timer); 
                       timer = setTimeout(function () {
                        if(val.that.search() !== val.this.value){
                            val.that.search( val.this.value ).draw();
                        }
                    }, 500);
                });
            });
        });
        // $('th', el).unbind('click.DT'); //
        // $('th', el).remove(); //
    }
});