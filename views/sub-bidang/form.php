<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['sub-bidang']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['sub-bidang'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-left-30 m-margin-left-0">
    <div class="box-8">
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model['sub-bidang'], 'nama_sub_bidang', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['sub-bidang'], 'nama_sub_bidang', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['sub-bidang'], 'nama_sub_bidang', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['sub-bidang'], 'nama_sub_bidang', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['sub-bidang'], 'nama_sub_bidang')->end(); ?>

    <?= $form->field($model['sub-bidang'], 'id_bidang', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['sub-bidang'], 'id_bidang', ['class' => 'control-label fs-12', 'label' => 'Bidang']); ?>
        <?= Html::activeDropDownList($model['sub-bidang'], 'id_bidang', ArrayHelper::map(\app_virama_karya\models\Bidang::find()->asArray()->all(), 'id', 'nama_bidang'), ['prompt' => 'Choose one please', 'class' => 'form-control']); ?>
        <?= Html::error($model['sub-bidang'], 'id_bidang', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['sub-bidang'], 'id_bidang')->end(); ?>


    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['sub-bidang']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>