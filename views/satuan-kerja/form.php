<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['satuan-kerja']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['satuan-kerja'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-left-30 m-margin-left-0">
    <div class="box-8">
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model['satuan-kerja'], 'nama_satuan_kerja', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['satuan-kerja'], 'nama_satuan_kerja', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['satuan-kerja'], 'nama_satuan_kerja', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['satuan-kerja'], 'nama_satuan_kerja', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['satuan-kerja'], 'nama_satuan_kerja')->end(); ?>

    <?= $form->field($model['satuan-kerja'], 'id_instansi', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['satuan-kerja'], 'id_instansi', ['class' => 'control-label fs-12', 'label' => 'Instansi']); ?>
        <?= Html::activeDropDownList($model['satuan-kerja'], 'id_instansi', ArrayHelper::map(\app_virama_karya\models\Instansi::find()->asArray()->all(), 'id', 'nama_instansi'), ['prompt' => 'Choose one please', 'class' => 'form-control']); ?>
        <?= Html::error($model['satuan-kerja'], 'id_instansi', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['satuan-kerja'], 'id_instansi')->end(); ?>

    <?= $form->field($model['satuan-kerja'], 'id_satminkal', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['satuan-kerja'], 'id_satminkal', ['class' => 'control-label fs-12', 'label' => 'Satminkal']); ?>
        <?= Html::activeDropDownList($model['satuan-kerja'], 'id_satminkal', ArrayHelper::map(\app_virama_karya\models\Satminkal::find()->asArray()->all(), 'id', 'nama_satminkal'), ['prompt' => 'Choose one please', 'class' => 'form-control']); ?>
        <?= Html::error($model['satuan-kerja'], 'id_satminkal', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['satuan-kerja'], 'id_satminkal')->end(); ?>

    <?= $form->field($model['satuan-kerja'], 'alamat', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['satuan-kerja'], 'alamat', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextArea($model['satuan-kerja'], 'alamat', ['class' => 'form-control', 'rows' => 6]) ?>
        <?= Html::error($model['satuan-kerja'], 'alamat', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['satuan-kerja'], 'alamat')->end(); ?>

    <?= $form->field($model['satuan-kerja'], 'telpon', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['satuan-kerja'], 'telpon', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['satuan-kerja'], 'telpon', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['satuan-kerja'], 'telpon', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['satuan-kerja'], 'telpon')->end(); ?>


    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['satuan-kerja']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>