<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

//
$tenagaAhliPendidikans = [];
if (isset($model['tenaga_ahli_pendidikan']))
    foreach ($model['tenaga_ahli_pendidikan'] as $key => $tenagaAhliPendidikan)
        $tenagaAhliPendidikans[] = $tenagaAhliPendidikan->attributes;

$tenagaAhliKeahlians = [];
if (isset($model['tenaga_ahli_keahlian']))
    foreach ($model['tenaga_ahli_keahlian'] as $key => $tenagaAhliKeahlian)
        $tenagaAhliKeahlians[] = $tenagaAhliKeahlian->attributes;

$this->registerJs(
    'vm.$data.tenaga_ahli.tenagaAhliKeahlians = vm.$data.tenaga_ahli.tenagaAhliKeahlians.concat(' . json_encode($tenagaAhliKeahlians) . ');' .
    'vm.$data.tenaga_ahli.tenagaAhliPendidikans = vm.$data.tenaga_ahli.tenagaAhliPendidikans.concat(' . json_encode($tenagaAhliPendidikans) . ');',
    // 'vm.$data.tenaga_ahli.tenagaAhliPendidikans = Object.assign({}, vm.$data.tenaga_ahli.tenagaAhliPendidikans, ' . json_encode($tenagaAhliPendidikans) . ');',
    3
);

//
$error = false;
$errorMessage = '';
$errorVue = false;
if ($model['tenaga_ahli']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['tenaga_ahli'], ['class' => '']);
}

if (isset($model['tenaga_ahli_keahlian'])) foreach ($model['tenaga_ahli_keahlian'] as $key => $tenagaAhliKeahlian) {
    if ($tenagaAhliKeahlian->hasErrors()) {
        $error = true;
        $errorMessage .= Html::errorSummary($tenagaAhliKeahlian, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['tenaga_ahli_pendidikan'])) foreach ($model['tenaga_ahli_pendidikan'] as $key => $tenagaAhliPendidikan) {
    if ($tenagaAhliPendidikan->hasErrors()) {
        $error = true;
        $errorMessage .= Html::errorSummary($tenagaAhliPendidikan, ['class' => '']);
        $errorVue = true; 
    }
}
if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-left-30 m-margin-left-0">
    <div class="box-8">
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <div class="box box-break-sm">
        <div class="box-9 padding-x-0">
            <?= $form->field($model['tenaga_ahli'], 'nama', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
                <?= Html::activeLabel($model['tenaga_ahli'], 'nama', ['class' => 'control-label fs-12']); ?>
                <?= Html::activeTextInput($model['tenaga_ahli'], 'nama', ['class' => 'form-control', 'maxlength' => true]) ?>
                <?= Html::error($model['tenaga_ahli'], 'nama', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
            <?= $form->field($model['tenaga_ahli'], 'nama')->end(); ?>
        </div>
        <div class="box-3 padding-x-0">
            <?= $form->field($model['tenaga_ahli'], 'tanggal_lahir', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
                <?= Html::activeLabel($model['tenaga_ahli'], 'tanggal_lahir', ['class' => 'control-label fs-12']); ?>
                <?= Html::activeTextInput($model['tenaga_ahli'], 'tanggal_lahir', ['class' => 'form-control']) ?>
                <?= Html::error($model['tenaga_ahli'], 'tanggal_lahir', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
            <?= $form->field($model['tenaga_ahli'], 'tanggal_lahir')->end(); ?>
        </div>
    </div>

    <div class="box box-break-sm">
        <div class="box-9 padding-x-0">
            <?= $form->field($model['tenaga_ahli'], 'jabatan_yang_diusulkan', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
                <?= Html::activeLabel($model['tenaga_ahli'], 'jabatan_yang_diusulkan', ['class' => 'control-label fs-12']); ?>
                <?= Html::activeTextInput($model['tenaga_ahli'], 'jabatan_yang_diusulkan', ['class' => 'form-control', 'maxlength' => true]) ?>
                <?= Html::error($model['tenaga_ahli'], 'jabatan_yang_diusulkan', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
            <?= $form->field($model['tenaga_ahli'], 'jabatan_yang_diusulkan')->end(); ?>
        </div>
        <div class="box-3 padding-x-0">
            <?= $form->field($model['tenaga_ahli'], 'pengalaman_kerja', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
                <?= Html::activeLabel($model['tenaga_ahli'], 'pengalaman_kerja', ['class' => 'control-label fs-12', 'label' => 'Pengalaman (tahun)']); ?>
                <?= Html::activeTextInput($model['tenaga_ahli'], 'pengalaman_kerja', ['class' => 'form-control']) ?>
                <?= Html::error($model['tenaga_ahli'], 'pengalaman_kerja', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
            <?= $form->field($model['tenaga_ahli'], 'pengalaman_kerja')->end(); ?>
        </div>
    </div>
    
    <hr class="margin-y-15">

    <!--<?php if (isset($model['tenaga_ahli_pendidikan'])) foreach ($model['tenaga_ahli_pendidikan'] as $key => $value): ?>
        <?= $form->field($model['tenaga_ahli_pendidikan'][$key], "[$key]jenjang")->begin(); ?>
        <?= $form->field($model['tenaga_ahli_pendidikan'][$key], "[$key]jenjang")->end(); ?>
        <?= $form->field($model['tenaga_ahli_pendidikan'][$key], "[$key]jurusan")->begin(); ?>
        <?= $form->field($model['tenaga_ahli_pendidikan'][$key], "[$key]jurusan")->end(); ?>
        <?= $form->field($model['tenaga_ahli_pendidikan'][$key], "[$key]tahun")->begin(); ?>
        <?= $form->field($model['tenaga_ahli_pendidikan'][$key], "[$key]tahun")->end(); ?>
        <?= $form->field($model['tenaga_ahli_pendidikan'][$key], "[$key]nomor_ijazah")->begin(); ?>
        <?= $form->field($model['tenaga_ahli_pendidikan'][$key], "[$key]nomor_ijazah")->end(); ?>
    <?php endforeach; ?>-->

    <template v-if="typeof tenaga_ahli.tenagaAhliPendidikans == 'object'">
        <template v-for="(value, key, index) in tenaga_ahli.tenagaAhliPendidikans">
            <div v-show="!(value.id < 0)">
                <div class="margin-bottom-10 clearfix">
                    <b>Pendidikan ke-{{key+1}}</b>
                    <div v-on:click="removeTenagaAhliPendidikan(key)" class="pull-right hover-pointer text-red">hapus</div>
                </div>
                <input type="hidden" v-bind:id="'tenagaahlipendidikan-' + key + '-id'" v-bind:name="'TenagaAhliPendidikan[' + key + '][id]'" class="form-control" type="text" v-model="tenaga_ahli.tenagaAhliPendidikans[key].id">
                <div class="box box-break-sm">
                    <div class="box-1 padding-x-0">
                        <div v-bind:class="'form-group margin-bottom-10 form-group-sm field-tenagaahlipendidikan-' + key + '-jenjang'">
                            <label v-bind:for="'tenagaahlipendidikan-' + key + '-jenjang'" class="control-label fs-12">Jenjang</label>
                            <input v-bind:id="'tenagaahlipendidikan-' + key + '-jenjang'" v-bind:name="'TenagaAhliPendidikan[' + key + '][jenjang]'" class="form-control" type="text" v-model="tenaga_ahli.tenagaAhliPendidikans[key].jenjang">
                            <div class="help-block fs-11 margin-bottom-0"></div>
                        </div>
                    </div>
                    <div class="box-4 padding-x-0">
                        <div v-bind:class="'form-group margin-bottom-10 form-group-sm field-tenagaahlipendidikan-' + key + '-jurusan'">
                            <label v-bind:for="'tenagaahlipendidikan-' + key + '-jurusan'" class="control-label fs-12">Jurusan</label>
                            <input v-bind:id="'tenagaahlipendidikan-' + key + '-jurusan'" v-bind:name="'TenagaAhliPendidikan[' + key + '][jurusan]'" class="form-control" type="text" v-model="tenaga_ahli.tenagaAhliPendidikans[key].jurusan">
                            <div class="help-block fs-11 margin-bottom-0"></div>
                        </div>
                    </div>
                    <div class="box-1 padding-x-0">
                        <div v-bind:class="'form-group margin-bottom-10 form-group-sm field-tenagaahlipendidikan-' + key + '-tahun'">
                            <label v-bind:for="'tenagaahlipendidikan-' + key + '-tahun'" class="control-label fs-12">Tahun</label>
                            <input v-bind:id="'tenagaahlipendidikan-' + key + '-tahun'" v-bind:name="'TenagaAhliPendidikan[' + key + '][tahun]'" class="form-control" type="text" v-model="tenaga_ahli.tenagaAhliPendidikans[key].tahun">
                            <div class="help-block fs-11 margin-bottom-0"></div>
                        </div>
                    </div>
                    <div class="box-6 padding-x-0">
                        <div v-bind:class="'form-group margin-bottom-10 form-group-sm field-tenagaahlipendidikan-' + key + '-nomor_ijazah'">
                            <label v-bind:for="'tenagaahlipendidikan-' + key + '-nomor_ijazah'" class="control-label fs-12">Nomor Ijazah</label>
                            <input v-bind:id="'tenagaahlipendidikan-' + key + '-nomor_ijazah'" v-bind:name="'TenagaAhliPendidikan[' + key + '][nomor_ijazah]'" class="form-control" type="text" v-model="tenaga_ahli.tenagaAhliPendidikans[key].nomor_ijazah">
                            <div class="help-block fs-11 margin-bottom-0"></div>
                        </div>
                    </div>
                </div>
            </div>
        </template>
    </template>

    <a v-on:click="addTenagaAhliPendidikan" class="btn btn-default btn-sm">Tambah Pendidikan</a>

    <hr class="margin-y-15">

    <!--<?php if (isset($model['tenaga_ahli_keahlian'])) foreach ($model['tenaga_ahli_keahlian'] as $key => $value): ?>
        <?= $form->field($model['tenaga_ahli_keahlian'][$key], "[$key]keahlian")->begin(); ?>
        <?= $form->field($model['tenaga_ahli_keahlian'][$key], "[$key]keahlian")->end(); ?>
        <?= $form->field($model['tenaga_ahli_keahlian'][$key], "[$key]sertifikat")->begin(); ?>
        <?= $form->field($model['tenaga_ahli_keahlian'][$key], "[$key]sertifikat")->end(); ?>
    <?php endforeach; ?>-->

    <template v-if="typeof tenaga_ahli.tenagaAhliKeahlians == 'object'">
        <template v-for="(value, key, index) in tenaga_ahli.tenagaAhliKeahlians">
            <div v-show="!(value.id < 0)">
                <div class="margin-bottom-10 clearfix">
                    <b>Keahlian ke-{{key+1}}</b>
                    <div v-on:click="removeTenagaAhliKeahlian(key)" class="pull-right hover-pointer text-red">hapus</div>
                </div>
                <input type="hidden" v-bind:id="'tenagaahlikeahlian-' + key + '-id'" v-bind:name="'TenagaAhliKeahlian[' + key + '][id]'" class="form-control" type="text" v-model="tenaga_ahli.tenagaAhliKeahlians[key].id">
                <div class="box box-break-sm">
                    <div class="box-6 padding-x-0">
                        <div v-bind:class="'form-group margin-bottom-10 form-group-sm field-tenagaahlikeahlian-' + key + '-keahlian'">
                            <label v-bind:for="'tenagaahlikeahlian-' + key + '-keahlian'" class="control-label fs-12">Keahlian</label>
                            <input v-bind:id="'tenagaahlikeahlian-' + key + '-keahlian'" v-bind:name="'TenagaAhliKeahlian[' + key + '][keahlian]'" class="form-control" type="text" v-model="tenaga_ahli.tenagaAhliKeahlians[key].keahlian">
                            <div class="help-block fs-11 margin-bottom-0"></div>
                        </div>
                    </div>
                    <div class="box-6 padding-x-0">
                        <div v-bind:class="'form-group margin-bottom-10 form-group-sm field-tenagaahlikeahlian-' + key + '-sertifikat'">
                            <label v-bind:for="'tenagaahlikeahlian-' + key + '-sertifikat'" class="control-label fs-12">Sertifikat</label>
                            <input v-bind:id="'tenagaahlikeahlian-' + key + '-sertifikat'" v-bind:name="'TenagaAhliKeahlian[' + key + '][sertifikat]'" class="form-control" type="text" v-model="tenaga_ahli.tenagaAhliKeahlians[key].sertifikat">
                            <div class="help-block fs-11 margin-bottom-0"></div>
                        </div>
                    </div>
                </div>
            </div>
        </template>
    </template>

    <a v-on:click="addTenagaAhliKeahlian" class="btn btn-default btn-sm">Tambah Keahlian</a>

    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['tenaga_ahli']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>