<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm">
    <div class="box-8 margin-top-15">
<?php endif; ?>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-3 padding-x-0 text-right m-text-left"><?= $model['tenaga_ahli']->attributeLabels()['nama'] ?></div>
    <div class="box-9 m-padding-x-0 text-dark"><?= $model['tenaga_ahli']->nama ? $model['tenaga_ahli']->nama : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-3 padding-x-0 text-right m-text-left"><?= $model['tenaga_ahli']->attributeLabels()['tanggal_lahir'] ?></div>
    <div class="box-9 m-padding-x-0 text-dark"><?= $model['tenaga_ahli']->tanggal_lahir ? $model['tenaga_ahli']->tanggal_lahir : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-3 padding-x-0 text-right m-text-left"><?= $model['tenaga_ahli']->attributeLabels()['jabatan_yang_diusulkan'] ?></div>
    <div class="box-9 m-padding-x-0 text-dark"><?= $model['tenaga_ahli']->jabatan_yang_diusulkan ? $model['tenaga_ahli']->jabatan_yang_diusulkan : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-3 padding-x-0 text-right m-text-left"><?= $model['tenaga_ahli']->attributeLabels()['pengalaman_kerja'] ?></div>
    <div class="box-9 m-padding-x-0 text-dark"><?= $model['tenaga_ahli']->pengalaman_kerja ? $model['tenaga_ahli']->pengalaman_kerja . ' tahun': '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>

<div class="box box-break-sm margin-bottom-10">
    <div class="box-3 padding-x-0 text-right m-text-left">Pendidikan</div>
    <div class="box-9 m-padding-x-0 text-dark">
        <?php if ($model['tenaga_ahli']->tenagaAhliPendidikans) {
            foreach ($model['tenaga_ahli']->tenagaAhliPendidikans as $key => $tenagaAhliPendidikan) {
                echo '• ' . $tenagaAhliPendidikan->jenjang . ' ' . $tenagaAhliPendidikan->jurusan . ' ' . $tenagaAhliPendidikan->tahun . ' (' . $tenagaAhliPendidikan->nomor_ijazah, ')<br>';
            }
        } else echo '<span class="text-gray f-italic">(kosong)</span>' ?>
    </div>
</div>

<div class="box box-break-sm margin-bottom-10">
    <div class="box-3 padding-x-0 text-right m-text-left">Keahlian</div>
    <div class="box-9 m-padding-x-0 text-dark">
        <?php if ($model['tenaga_ahli']->tenagaAhliKeahlians) {
            foreach ($model['tenaga_ahli']->tenagaAhliKeahlians as $key => $tenagaAhliKeahlian) {
                echo '• ' . $tenagaAhliKeahlian->keahlian . ' (' . $tenagaAhliKeahlian->sertifikat, ')<br>';
            }
        } else echo '<span class="text-gray f-italic">(kosong)</span>' ?>
    </div>
</div>
    
<?php if (!Yii::$app->request->isAjax) : ?>
        <hr class="margin-y-15">
        <div class="form-group clearfix">
            <?= Html::a('Update', ['update', 'id' => $model['tenaga_ahli']->id], ['class' => 'btn btn-sm btn-default bg-azure rounded-xs border-azure']) ?>&nbsp;
            <?= Html::a('Delete', ['delete', 'id' => $model['tenaga_ahli']->id], [
                'class' => 'btn btn-sm btn-default bg-lighter rounded-xs',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item ?',
                    'method' => 'post',
                ],
            ]) ?>
            <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-sm btn-default bg-lighter rounded-xs pull-right']) ?>
        </div>
    </div>
</div>
<?php endif; ?>