<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['aset']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['aset'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-left-30 m-margin-left-0">
    <div class="box-8">
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model['aset'], 'nama_aset')->begin(); ?>
        <?= Html::activeLabel($model['aset'], 'nama_aset', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['aset'], 'nama_aset', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['aset'], 'nama_aset', ['class' => 'help-block']); ?>
    <?= $form->field($model['aset'], 'nama_aset')->end(); ?>

    <?= $form->field($model['aset'], 'jumlah')->begin(); ?>
        <?= Html::activeLabel($model['aset'], 'jumlah', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['aset'], 'jumlah', ['class' => 'form-control']) ?>
        <?= Html::error($model['aset'], 'jumlah', ['class' => 'help-block']); ?>
    <?= $form->field($model['aset'], 'jumlah')->end(); ?>

    <?= $form->field($model['aset'], 'keterangan')->begin(); ?>
        <?= Html::activeLabel($model['aset'], 'keterangan', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['aset'], 'keterangan', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['aset'], 'keterangan', ['class' => 'help-block']); ?>
    <?= $form->field($model['aset'], 'keterangan')->end(); ?>

    <?= $form->field($model['aset'], 'merk')->begin(); ?>
        <?= Html::activeLabel($model['aset'], 'merk', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['aset'], 'merk', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['aset'], 'merk', ['class' => 'help-block']); ?>
    <?= $form->field($model['aset'], 'merk')->end(); ?>

    <?= $form->field($model['aset'], 'tahun_pembuatan')->begin(); ?>
        <?= Html::activeLabel($model['aset'], 'tahun_pembuatan', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['aset'], 'tahun_pembuatan', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['aset'], 'tahun_pembuatan', ['class' => 'help-block']); ?>
    <?= $form->field($model['aset'], 'tahun_pembuatan')->end(); ?>

    <?= $form->field($model['aset'], 'kondisi')->begin(); ?>
        <?= Html::activeLabel($model['aset'], 'kondisi', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['aset'], 'kondisi', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['aset'], 'kondisi', ['class' => 'help-block']); ?>
    <?= $form->field($model['aset'], 'kondisi')->end(); ?>

    <?= $form->field($model['aset'], 'lokasi_sekarang')->begin(); ?>
        <?= Html::activeLabel($model['aset'], 'lokasi_sekarang', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['aset'], 'lokasi_sekarang', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['aset'], 'lokasi_sekarang', ['class' => 'help-block']); ?>
    <?= $form->field($model['aset'], 'lokasi_sekarang')->end(); ?>

    <?= $form->field($model['aset'], 'bukti_kepemilikan')->begin(); ?>
        <?= Html::activeLabel($model['aset'], 'bukti_kepemilikan', ['class' => 'control-label']); ?>
        <?= Html::activeDropDownList($model['aset'], 'bukti_kepemilikan', [ 'Ada' => 'Ada', 'Tidak ada' => 'Tidak ada', ], ['prompt' => 'Choose one please', 'class' => 'form-control']) ?>
        <?= Html::error($model['aset'], 'bukti_kepemilikan', ['class' => 'help-block']); ?>
    <?= $form->field($model['aset'], 'bukti_kepemilikan')->end(); ?>


    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['aset']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>