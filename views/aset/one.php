<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-top-15">
    <div class="box-6">
<?php endif; ?>

<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['aset']->attributeLabels()['id'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['aset']->id ? $model['aset']->id : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['aset']->attributeLabels()['nama_aset'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['aset']->nama_aset ? $model['aset']->nama_aset : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['aset']->attributeLabels()['jumlah'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['aset']->jumlah ? $model['aset']->jumlah : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['aset']->attributeLabels()['keterangan'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['aset']->keterangan ? $model['aset']->keterangan : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['aset']->attributeLabels()['merk'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['aset']->merk ? $model['aset']->merk : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['aset']->attributeLabels()['tahun_pembuatan'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['aset']->tahun_pembuatan ? $model['aset']->tahun_pembuatan : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['aset']->attributeLabels()['kondisi'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['aset']->kondisi ? $model['aset']->kondisi : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['aset']->attributeLabels()['lokasi_sekarang'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['aset']->lokasi_sekarang ? $model['aset']->lokasi_sekarang : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['aset']->attributeLabels()['bukti_kepemilikan'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['aset']->bukti_kepemilikan ? $model['aset']->bukti_kepemilikan : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<?php if (!Yii::$app->request->isAjax) : ?>
        <hr class="margin-y-15">
        <div class="form-group clearfix">
            <?= Html::a('Update', ['update', 'id' => $model['aset']->id], ['class' => 'btn btn-sm btn-default bg-azure rounded-xs border-azure']) ?>&nbsp;
            <?= Html::a('Delete', ['delete', 'id' => $model['aset']->id], [
                'class' => 'btn btn-sm btn-default bg-lighter rounded-xs',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item ?',
                    'method' => 'post',
                ],
            ]) ?>
            <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-sm btn-default bg-lighter rounded-xs pull-right']) ?>
        </div>
    </div>
</div>
<?php endif; ?>