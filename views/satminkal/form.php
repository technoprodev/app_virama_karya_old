<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['satminkal']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['satminkal'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-left-30 m-margin-left-0">
    <div class="box-8">
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model['satminkal'], 'nama_satminkal', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['satminkal'], 'nama_satminkal', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['satminkal'], 'nama_satminkal', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['satminkal'], 'nama_satminkal', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['satminkal'], 'nama_satminkal')->end(); ?>

    <?= $form->field($model['satminkal'], 'id_instansi', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['satminkal'], 'id_instansi', ['class' => 'control-label fs-12', 'label' => 'Instansi']); ?>
        <?= Html::activeDropDownList($model['satminkal'], 'id_instansi', ArrayHelper::map(\app_virama_karya\models\Instansi::find()->asArray()->all(), 'id', 'nama_instansi'), ['prompt' => 'Choose one please', 'class' => 'form-control']); ?>
        <?= Html::error($model['satminkal'], 'id_instansi', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['satminkal'], 'id_instansi')->end(); ?>


    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['satminkal']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>