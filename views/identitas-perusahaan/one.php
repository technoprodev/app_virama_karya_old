<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm">
    <div class="box-9 margin-top-15">
<?php endif; ?>

<div class="border-azure margin-bottom-30">
    <div class="bg-azure padding-x-15 padding-y-10">
        <span class="fs-16">Data Administrasi</span>
    </div>
    <div class="padding-x-15 padding-top-20">
        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['nama_perusahaan'] ?></div>
            <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->nama_perusahaan ? $model['identitas_perusahaan']->nama_perusahaan : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
        </div>
            
        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['status'] ?></div>
            <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->status ? $model['identitas_perusahaan']->status : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
        </div>
            
        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['alamat'] ?></div>
            <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->alamat ? $model['identitas_perusahaan']->alamat : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
        </div>
            
        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['nomor_telpon'] ?></div>
            <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->nomor_telpon ? $model['identitas_perusahaan']->nomor_telpon : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
        </div>
            
        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['nomor_fax'] ?></div>
            <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->nomor_fax ? $model['identitas_perusahaan']->nomor_fax : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
        </div>
            
        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['email_1'] ?></div>
            <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->email_1 ? $model['identitas_perusahaan']->email_1 : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
        </div>
            
        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['email_2'] ?></div>
            <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->email_2 ? $model['identitas_perusahaan']->email_2 : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
        </div>
    </div>
</div>

<div class="border-azure margin-bottom-30">
    <div class="bg-azure padding-x-15 padding-y-10">
        <span class="fs-16">Landasan Hukum Pendirian Badan Usaha</span>
    </div>
    <div class="padding-x-15 padding-top-20">
        <div class="">
            <div class="fs-15 text-azure margin-bottom-10">
                <span class="margin-right-10"><i class="fa fa-angle-right"></i></span>Akta Pendirian Perusahaan
            </div>

            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['akta_pendirian_perusahaan_nomor_akta'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->akta_pendirian_perusahaan_nomor_akta ? $model['identitas_perusahaan']->akta_pendirian_perusahaan_nomor_akta : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['akta_pendirian_perusahaan_tanggal'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->akta_pendirian_perusahaan_tanggal ? $model['identitas_perusahaan']->akta_pendirian_perusahaan_tanggal : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['akta_pendirian_perusahaan_nama_notaris'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->akta_pendirian_perusahaan_nama_notaris ? $model['identitas_perusahaan']->akta_pendirian_perusahaan_nama_notaris : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['akta_pendirian_perusahaan_nomor_pengesahan'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->akta_pendirian_perusahaan_nomor_pengesahan ? $model['identitas_perusahaan']->akta_pendirian_perusahaan_nomor_pengesahan : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['akta_pendirian_perusahaan_tanggal_pengesahan'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->akta_pendirian_perusahaan_tanggal_pengesahan ? $model['identitas_perusahaan']->akta_pendirian_perusahaan_tanggal_pengesahan : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
        </div>

        <hr class="margin-y-15">

        <div class="">
            <div class="fs-15 text-azure margin-bottom-10">
                <span class="margin-right-10"><i class="fa fa-angle-right"></i></span>Akta Pendirian Perusahaan
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['akta_perubahan_terakhir_nomor_akta'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->akta_perubahan_terakhir_nomor_akta ? $model['identitas_perusahaan']->akta_perubahan_terakhir_nomor_akta : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['akta_perubahan_terakhir_nama_notaris'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->akta_perubahan_terakhir_nama_notaris ? $model['identitas_perusahaan']->akta_perubahan_terakhir_nama_notaris : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['akta_perubahan_terakhir_tanggal'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->akta_perubahan_terakhir_tanggal ? $model['identitas_perusahaan']->akta_perubahan_terakhir_tanggal : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['akta_perubahan_terakhir_nomor_pengesahan'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->akta_perubahan_terakhir_nomor_pengesahan ? $model['identitas_perusahaan']->akta_perubahan_terakhir_nomor_pengesahan : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['akta_perubahan_terakhir_tanggal_pengesahan'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->akta_perubahan_terakhir_tanggal_pengesahan ? $model['identitas_perusahaan']->akta_perubahan_terakhir_tanggal_pengesahan : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
        </div>
    </div>
</div>

<div class="border-azure margin-bottom-30">
    <div class="bg-azure padding-x-15 padding-y-10">
        <span class="fs-16">Pengurus Badan Usaha</span>
    </div>
    <div class="padding-x-15 padding-top-20">
        <div class="">
            <div class="fs-15 text-azure margin-bottom-10">
                <span class="margin-right-10"><i class="fa fa-angle-right"></i></span>Komisaris
            </div>

            <div class="margin-bottom-10 padding-x-15">
                Sedang dalam proses upload
            </div>
        </div>

        <hr class="margin-y-15">

        <div class="">
            <div class="fs-15 text-azure margin-bottom-10">
                <span class="margin-right-10"><i class="fa fa-angle-right"></i></span>Direksi
            </div>

            <div class="margin-bottom-10 padding-x-15">
                Sedang dalam proses upload
            </div>
        </div>
    </div>
</div>

<div class="border-azure margin-bottom-30">
    <div class="bg-azure padding-x-15 padding-y-10">
        <span class="fs-16">Ijin Usaha</span>
    </div>
    <div class="padding-x-15 padding-top-20">
        <div class="">
            <div class="fs-15 text-azure margin-bottom-10">
                <span class="margin-right-10"><i class="fa fa-angle-right"></i></span>Izin Usaha Jasa Konstruksi/Konsultan (IUJK)
            </div>

            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['iujk_masa_berlaku_dari'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->iujk_masa_berlaku_dari ? $model['identitas_perusahaan']->iujk_masa_berlaku_dari : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['iujk_masa_berlaku_sampai'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->iujk_masa_berlaku_sampai ? $model['identitas_perusahaan']->iujk_masa_berlaku_sampai : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['iujk_instansi_pemberi'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->iujk_instansi_pemberi ? $model['identitas_perusahaan']->iujk_instansi_pemberi : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['iujk_perencanaan'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->iujk_perencanaan ? $model['identitas_perusahaan']->iujk_perencanaan : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['iujk_pengawasan'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->iujk_pengawasan ? $model['identitas_perusahaan']->iujk_pengawasan : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['iujk_konsultansi'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->iujk_konsultansi ? $model['identitas_perusahaan']->iujk_konsultansi : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
        </div>

        <hr class="margin-y-15">

        <div class="">
            <div class="fs-15 text-azure margin-bottom-10">
                <span class="margin-right-10"><i class="fa fa-angle-right"></i></span>Sertifikat Badan Usaha (SBU) Konstruksi
            </div>

            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['sbu_konstruksi_masa_berlaku_dari'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->sbu_konstruksi_masa_berlaku_dari ? $model['identitas_perusahaan']->sbu_konstruksi_masa_berlaku_dari : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['sbu_konstruksi_masa_berlaku_sampai'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->sbu_konstruksi_masa_berlaku_sampai ? $model['identitas_perusahaan']->sbu_konstruksi_masa_berlaku_sampai : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['sbu_konstruksi_instansi_pemberi_izin'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->sbu_konstruksi_instansi_pemberi_izin ? $model['identitas_perusahaan']->sbu_konstruksi_instansi_pemberi_izin : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['sbu_konstruksi_perencanaan_arsitektur'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->sbu_konstruksi_perencanaan_arsitektur ? $model['identitas_perusahaan']->sbu_konstruksi_perencanaan_arsitektur : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['sbu_konstruksi_pengawasan_arsitektur'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->sbu_konstruksi_pengawasan_arsitektur ? $model['identitas_perusahaan']->sbu_konstruksi_pengawasan_arsitektur : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['sbu_konstruksi_jasa_konsultansi_lainnya'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->sbu_konstruksi_jasa_konsultansi_lainnya ? $model['identitas_perusahaan']->sbu_konstruksi_jasa_konsultansi_lainnya : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['sbu_konstruksi_perencana_penataan_ruang'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->sbu_konstruksi_perencana_penataan_ruang ? $model['identitas_perusahaan']->sbu_konstruksi_perencana_penataan_ruang : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['sbu_konstruksi_pengawasan_penataan_ruang'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->sbu_konstruksi_pengawasan_penataan_ruang ? $model['identitas_perusahaan']->sbu_konstruksi_pengawasan_penataan_ruang : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['sbu_konstruksi_perencanaan_rekayasa'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->sbu_konstruksi_perencanaan_rekayasa ? $model['identitas_perusahaan']->sbu_konstruksi_perencanaan_rekayasa : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['sbu_konstruksi_pengawasan_rekayasa'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->sbu_konstruksi_pengawasan_rekayasa ? $model['identitas_perusahaan']->sbu_konstruksi_pengawasan_rekayasa : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['sbu_konstruksi_konsultan_spesialis'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->sbu_konstruksi_konsultan_spesialis ? $model['identitas_perusahaan']->sbu_konstruksi_konsultan_spesialis : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
        </div>

        <hr class="margin-y-15">

        <div class="">
            <div class="fs-15 text-azure margin-bottom-10">
                <span class="margin-right-10"><i class="fa fa-angle-right"></i></span>Sertifikat Badan Usaha (SBU) Non Konstruksi
            </div>

            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['sbu_non_konstruksi_ljspdbt'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->sbu_non_konstruksi_ljspdbt ? $model['identitas_perusahaan']->sbu_non_konstruksi_ljspdbt : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['sbu_non_konstruksi_ljskm'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->sbu_non_konstruksi_ljskm ? $model['identitas_perusahaan']->sbu_non_konstruksi_ljskm : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['sbu_non_konstruksi_ljskm_masa_berlaku_dari'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->sbu_non_konstruksi_ljskm_masa_berlaku_dari ? $model['identitas_perusahaan']->sbu_non_konstruksi_ljskm_masa_berlaku_dari : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['sbu_non_konstruksi_ljskm_masa_berlaku_sampai'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->sbu_non_konstruksi_ljskm_masa_berlaku_sampai ? $model['identitas_perusahaan']->sbu_non_konstruksi_ljskm_masa_berlaku_sampai : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['sbu_non_konstruksi_ljskm_instansi_pemberi_izin'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->sbu_non_konstruksi_ljskm_instansi_pemberi_izin ? $model['identitas_perusahaan']->sbu_non_konstruksi_ljskm_instansi_pemberi_izin : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['sbu_non_konstruksi_pengembangan_pertanian_dan_pedesaan'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->sbu_non_konstruksi_pengembangan_pertanian_dan_pedesaan ? $model['identitas_perusahaan']->sbu_non_konstruksi_pengembangan_pertanian_dan_pedesaan : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['sbu_non_konstruksi_transportasi'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->sbu_non_konstruksi_transportasi ? $model['identitas_perusahaan']->sbu_non_konstruksi_transportasi : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['sbu_non_konstruksi_telematika'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->sbu_non_konstruksi_telematika ? $model['identitas_perusahaan']->sbu_non_konstruksi_telematika : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['sbu_non_konstruksi_pendidikan'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->sbu_non_konstruksi_pendidikan ? $model['identitas_perusahaan']->sbu_non_konstruksi_pendidikan : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['sbu_non_konstruksi_pendidikan_masa_berlaku_dari'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->sbu_non_konstruksi_pendidikan_masa_berlaku_dari ? $model['identitas_perusahaan']->sbu_non_konstruksi_pendidikan_masa_berlaku_dari : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['sbu_non_konstruksi_pendidikan_masa_berlaku_sampai'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->sbu_non_konstruksi_pendidikan_masa_berlaku_sampai ? $model['identitas_perusahaan']->sbu_non_konstruksi_pendidikan_masa_berlaku_sampai : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['sbu_non_konstruksi_pendidikan_instansi_pemberi_izin'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->sbu_non_konstruksi_pendidikan_instansi_pemberi_izin ? $model['identitas_perusahaan']->sbu_non_konstruksi_pendidikan_instansi_pemberi_izin : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
        </div>
    </div>
</div>

<div class="border-azure margin-bottom-30">
    <div class="bg-azure padding-x-15 padding-y-10">
        <span class="fs-16">Ijin Usaha Lainnya</span>
    </div>
    <div class="padding-x-15 padding-top-20">
        <div class="">
            <div class="fs-15 text-azure margin-bottom-10">
                <span class="margin-right-10"><i class="fa fa-angle-right"></i></span>Nama Ijin
            </div>

            <div class="margin-bottom-10 padding-x-15">
                Sedang dalam proses upload
            </div>
        </div>

        <hr class="margin-y-15">

        <div class="">
            <div class="fs-15 text-azure margin-bottom-10">
                <span class="margin-right-10"><i class="fa fa-angle-right"></i></span>Nama Ijin
            </div>

            <div class="margin-bottom-10 padding-x-15">
                Sedang dalam proses upload
            </div>
        </div>
    </div>
</div>

<div class="border-azure margin-bottom-30">
    <div class="bg-azure padding-x-15 padding-y-10">
        <span class="fs-16">Susunan Kepemilikan Saham <?= $model['identitas_perusahaan']->nama_perusahaan ?></span>
    </div>
    <div class="padding-x-15 padding-top-20">
        <div class="">
            <div class="fs-15 text-azure margin-bottom-10">
                <span class="margin-right-10"><i class="fa fa-angle-right"></i></span>Data
            </div>

            <div class="margin-bottom-10 padding-x-15">
                Sedang dalam proses upload
            </div>
        </div>

        <hr class="margin-y-15">

        <div class="">
            <div class="fs-15 text-azure margin-bottom-10">
                <span class="margin-right-10"><i class="fa fa-angle-right"></i></span>Data
            </div>

            <div class="margin-bottom-10 padding-x-15">
                Sedang dalam proses upload
            </div>
        </div>
    </div>
</div>

<div class="border-azure margin-bottom-30">
    <div class="bg-azure padding-x-15 padding-y-10">
        <span class="fs-16">Pajak</span>
    </div>
    <div class="padding-x-15 padding-top-20">
        <div class="">
            <div class="fs-15 text-azure margin-bottom-10">
                <span class="margin-right-10"><i class="fa fa-angle-right"></i></span><?= $model['identitas_perusahaan']->attributeLabels()['pajak_nomor_pokok_wajib_pajak'] ?>
            </div>

            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left">Nomor</div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->pajak_nomor_pokok_wajib_pajak ? $model['identitas_perusahaan']->pajak_nomor_pokok_wajib_pajak : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
        </div>

        <hr class="margin-y-15">

        <div class="">
            <div class="fs-15 text-azure margin-bottom-10">
                <span class="margin-right-10"><i class="fa fa-angle-right"></i></span><?= $model['identitas_perusahaan']->attributeLabels()['pajak_surat_pengukuhan_kena_pajak'] ?>
            </div>

            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left">Nomor</div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->pajak_surat_pengukuhan_kena_pajak ? $model['identitas_perusahaan']->pajak_surat_pengukuhan_kena_pajak : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
        </div>

        <hr class="margin-y-15">

        <div class="">
            <div class="fs-15 text-azure margin-bottom-10">
                <span class="margin-right-10"><i class="fa fa-angle-right"></i></span><?= $model['identitas_perusahaan']->attributeLabels()['pajak_surat_keterangan_terdaftar'] ?>
            </div>

            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left">Nomor</div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->pajak_surat_keterangan_terdaftar ? $model['identitas_perusahaan']->pajak_surat_keterangan_terdaftar : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
        </div>

        <hr class="margin-y-15">

        <div class="">
            <div class="fs-15 text-azure margin-bottom-10">
                <span class="margin-right-10"><i class="fa fa-angle-right"></i></span>Bukti Pelunasan Pajak Tahun Terakhir
            </div>

            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['pajak_bpptt_nomor'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->pajak_bpptt_nomor ? $model['identitas_perusahaan']->pajak_bpptt_nomor : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['identitas_perusahaan']->attributeLabels()['pajak_bpptt_tanggal'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['identitas_perusahaan']->pajak_bpptt_tanggal ? $model['identitas_perusahaan']->pajak_bpptt_tanggal : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
        </div>

        <hr class="margin-y-15">

        <div class="">
            <div class="fs-15 text-azure margin-bottom-10">
                <span class="margin-right-10"><i class="fa fa-angle-right"></i></span>Bukti Pelunasan Pajak 3 Bulan Terakhir
            </div>

            <div class="margin-bottom-10 padding-x-15">
                Sedang dalam proses upload
            </div>
        </div>
    </div>
</div>


    
<?php if (!Yii::$app->request->isAjax) : ?>
        <hr class="margin-y-15">
        <div class="form-group clearfix">
            <?= Html::a('Update', ['update', 'id' => $model['identitas_perusahaan']->id], ['class' => 'btn btn-sm btn-default bg-azure rounded-xs border-azure']) ?>&nbsp;
            <?= Html::a('Delete', ['delete', 'id' => $model['identitas_perusahaan']->id], [
                'class' => 'btn btn-sm btn-default bg-lighter rounded-xs',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item ?',
                    'method' => 'post',
                ],
            ]) ?>
            <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-sm btn-default bg-lighter rounded-xs pull-right']) ?>
        </div>
    </div>
</div>
<?php endif; ?>