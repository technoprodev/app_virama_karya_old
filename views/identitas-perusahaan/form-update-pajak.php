<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['identitas_perusahaan']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['identitas_perusahaan'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-left-30 m-margin-left-0">
    <div class="box-8">
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <div class="">
        <div class="fs-15 text-azure margin-bottom-10">
            <span class="margin-right-10"><i class="fa fa-angle-right"></i></span><?= $model['identitas_perusahaan']->attributeLabels()['pajak_nomor_pokok_wajib_pajak'] ?>
        </div>

        <?= $form->field($model['identitas_perusahaan'], 'pajak_nomor_pokok_wajib_pajak', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'pajak_nomor_pokok_wajib_pajak', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'pajak_nomor_pokok_wajib_pajak', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'pajak_nomor_pokok_wajib_pajak', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'pajak_nomor_pokok_wajib_pajak')->end(); ?>
    </div>

    <hr class="margin-y-15">

    <div class="">
        <div class="fs-15 text-azure margin-bottom-10">
            <span class="margin-right-10"><i class="fa fa-angle-right"></i></span><?= $model['identitas_perusahaan']->attributeLabels()['pajak_surat_pengukuhan_kena_pajak'] ?>
        </div>

        <?= $form->field($model['identitas_perusahaan'], 'pajak_surat_pengukuhan_kena_pajak', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'pajak_surat_pengukuhan_kena_pajak', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'pajak_surat_pengukuhan_kena_pajak', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'pajak_surat_pengukuhan_kena_pajak', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'pajak_surat_pengukuhan_kena_pajak')->end(); ?>
    </div>

    <hr class="margin-y-15">

    <div class="">
        <div class="fs-15 text-azure margin-bottom-10">
            <span class="margin-right-10"><i class="fa fa-angle-right"></i></span><?= $model['identitas_perusahaan']->attributeLabels()['pajak_surat_keterangan_terdaftar'] ?>
        </div>

        <?= $form->field($model['identitas_perusahaan'], 'pajak_surat_keterangan_terdaftar', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'pajak_surat_keterangan_terdaftar', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'pajak_surat_keterangan_terdaftar', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'pajak_surat_keterangan_terdaftar', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'pajak_surat_keterangan_terdaftar')->end(); ?>
    </div>

    <hr class="margin-y-15">

    <div class="">
        <div class="fs-15 text-azure margin-bottom-10">
            <span class="margin-right-10"><i class="fa fa-angle-right"></i></span>Bukti Pelunasan Pajak Tahun Terakhir
        </div>

        <?= $form->field($model['identitas_perusahaan'], 'pajak_bpptt_nomor', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'pajak_bpptt_nomor', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'pajak_bpptt_nomor', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'pajak_bpptt_nomor', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'pajak_bpptt_nomor')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'pajak_bpptt_tanggal', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'pajak_bpptt_tanggal', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'pajak_bpptt_tanggal', ['class' => 'form-control']) ?>
            <?= Html::error($model['identitas_perusahaan'], 'pajak_bpptt_tanggal', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'pajak_bpptt_tanggal')->end(); ?>
    </div>

    <hr class="margin-y-15">

    <div class="">
        <div class="fs-15 text-azure margin-bottom-10">
            <span class="margin-right-10"><i class="fa fa-angle-right"></i></span>Bukti Pelunasan Pajak 3 Bulan Terakhir
        </div>

        <?= $form->field($model['identitas_perusahaan'], 'pajak_skf_nomor', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'pajak_skf_nomor', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'pajak_skf_nomor', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'pajak_skf_nomor', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'pajak_skf_nomor')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'pajak_skf_tanggal', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'pajak_skf_tanggal', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'pajak_skf_tanggal', ['class' => 'form-control']) ?>
            <?= Html::error($model['identitas_perusahaan'], 'pajak_skf_tanggal', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'pajak_skf_tanggal')->end(); ?>
    </div>
    
    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['identitas_perusahaan']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>