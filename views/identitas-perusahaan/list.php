<?php

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/identitas-perusahaan/list.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<table class="datatables display nowrap table table-striped table-hover table-condensed">
    <thead>
        <tr>
            <th class="text-dark f-normal" style="border-bottom: 1px">Action</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Nama Perusahaan</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Status</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Alamat</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Nomor Telpon</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Nomor Fax</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Email 1</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Email 2</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Akta Pendirian Perusahaan Nomor Akta</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Akta Pendirian Perusahaan Tanggal</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Akta Pendirian Perusahaan Nama Notaris</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Akta Pendirian Perusahaan Nomor Pengesahan</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Akta Pendirian Perusahaan Tanggal Pengesahan</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Akta Perubahan Terakhir Nomor Akta</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Akta Perubahan Terakhir Nama Notaris</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Akta Perubahan Terakhir Tanggal</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Akta Perubahan Terakhir Nomor Pengesahan</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Akta Perubahan Terakhir Tanggal Pengesahan</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Iujk Masa Berlaku Dari</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Iujk Masa Berlaku Sampai</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Iujk Instansi Pemberi</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Iujk Perencanaan</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Iujk Pengawasan</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Iujk Konsultansi</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Sbu Konstruksi Masa Berlaku Dari</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Sbu Konstruksi Masa Berlaku Sampai</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Sbu Konstruksi Instansi Pemberi Izin</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Sbu Konstruksi Perencanaan Arsitektur</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Sbu Konstruksi Pengawasan Arsitektur</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Sbu Konstruksi Jasa Konsultansi Lainnya</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Sbu Konstruksi Perencana Penataan Ruang</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Sbu Konstruksi Pengawasan Penataan Ruang</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Sbu Konstruksi Perencanaan Rekayasa</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Sbu Konstruksi Pengawasan Rekayasa</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Sbu Konstruksi Konsultan Spesialis</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Sbu Non Konstruksi Ljspdbt</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Sbu Non Konstruksi Ljskm</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Sbu Non Konstruksi Ljskm Masa Berlaku Dari</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Sbu Non Konstruksi Ljskm Masa Berlaku Sampai</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Sbu Non Konstruksi Ljskm Instansi Pemberi Izin</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Sbu Non Konstruksi Pengembangan Pertanian Dan Pedesaan</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Sbu Non Konstruksi Transportasi</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Sbu Non Konstruksi Telematika</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Sbu Non Konstruksi Pendidikan</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Sbu Non Konstruksi Pendidikan Masa Berlaku Dari</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Sbu Non Konstruksi Pendidikan Masa Berlaku Sampai</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Sbu Non Konstruksi Pendidikan Instansi Pemberi Izin</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Pajak Nomor Pokok Wajib Pajak</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Pajak Surat Pengukuhan Kena Pajak</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Pajak Surat Keterangan Terdaftar</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Pajak Bpptt Nomor</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Pajak Bpptt Tanggal</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Pajak Skf Nomor</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Pajak Skf Tanggal</th>
        </tr>
        <tr class="dt-search">
            <th class="padding-0"></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search nama perusahaan" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search status" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search alamat" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search nomor telpon" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search nomor fax" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search email 1" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search email 2" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search akta pendirian perusahaan nomor akta" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search akta pendirian perusahaan tanggal" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search akta pendirian perusahaan nama notaris" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search akta pendirian perusahaan nomor pengesahan" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search akta pendirian perusahaan tanggal pengesahan" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search akta perubahan terakhir nomor akta" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search akta perubahan terakhir nama notaris" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search akta perubahan terakhir tanggal" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search akta perubahan terakhir nomor pengesahan" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search akta perubahan terakhir tanggal pengesahan" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search iujk masa berlaku dari" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search iujk masa berlaku sampai" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search iujk instansi pemberi" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search iujk perencanaan" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search iujk pengawasan" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search iujk konsultansi" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search sbu konstruksi masa berlaku dari" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search sbu konstruksi masa berlaku sampai" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search sbu konstruksi instansi pemberi izin" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search sbu konstruksi perencanaan arsitektur" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search sbu konstruksi pengawasan arsitektur" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search sbu konstruksi jasa konsultansi lainnya" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search sbu konstruksi perencana penataan ruang" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search sbu konstruksi pengawasan penataan ruang" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search sbu konstruksi perencanaan rekayasa" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search sbu konstruksi pengawasan rekayasa" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search sbu konstruksi konsultan spesialis" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search sbu non konstruksi ljspdbt" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search sbu non konstruksi ljskm" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search sbu non konstruksi ljskm masa berlaku dari" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search sbu non konstruksi ljskm masa berlaku sampai" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search sbu non konstruksi ljskm instansi pemberi izin" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search sbu non konstruksi pengembangan pertanian dan pedesaan" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search sbu non konstruksi transportasi" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search sbu non konstruksi telematika" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search sbu non konstruksi pendidikan" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search sbu non konstruksi pendidikan masa berlaku dari" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search sbu non konstruksi pendidikan masa berlaku sampai" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search sbu non konstruksi pendidikan instansi pemberi izin" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search pajak nomor pokok wajib pajak" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search pajak surat pengukuhan kena pajak" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search pajak surat keterangan terdaftar" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search pajak bpptt nomor" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search pajak bpptt tanggal" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search pajak skf nomor" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search pajak skf tanggal" class="form-control border-none f-normal padding-x-5"/></th>
        </tr>
    </thead>
</table>

<?php if ($this->context->can('create')): ?>
    <hr class="margin-y-15">
    <div>
        <?= Html::a('Create Data', ['create'], ['class' => 'btn btn-sm btn-default bg-azure rounded-xs border-azure']) ?>
    </div>
<?php endif; ?>