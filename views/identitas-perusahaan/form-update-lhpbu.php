<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['identitas_perusahaan']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['identitas_perusahaan'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-left-30 m-margin-left-0">
    <div class="box-8">
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <div class="">
        <div class="fs-15 text-azure margin-bottom-10">
            <span class="margin-right-10"><i class="fa fa-angle-right"></i></span>Akta Pendirian Perusahaan
        </div>

        <?= $form->field($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_nomor_akta', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_nomor_akta', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_nomor_akta', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_nomor_akta', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_nomor_akta')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_tanggal', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_tanggal', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_tanggal', ['class' => 'form-control']) ?>
            <?= Html::error($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_tanggal', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_tanggal')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_nama_notaris', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_nama_notaris', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_nama_notaris', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_nama_notaris', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_nama_notaris')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_nomor_pengesahan', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_nomor_pengesahan', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_nomor_pengesahan', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_nomor_pengesahan', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_nomor_pengesahan')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_tanggal_pengesahan', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_tanggal_pengesahan', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_tanggal_pengesahan', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_tanggal_pengesahan', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_tanggal_pengesahan')->end(); ?>
    </div>

    <hr class="margin-y-15">

    <div class="">
        <div class="fs-15 text-azure margin-bottom-10">
            <span class="margin-right-10"><i class="fa fa-angle-right"></i></span>Akta Pendirian Perusahaan
        </div>
            
        <?= $form->field($model['identitas_perusahaan'], 'akta_perubahan_terakhir_nomor_akta', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'akta_perubahan_terakhir_nomor_akta', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'akta_perubahan_terakhir_nomor_akta', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'akta_perubahan_terakhir_nomor_akta', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'akta_perubahan_terakhir_nomor_akta')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'akta_perubahan_terakhir_nama_notaris', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'akta_perubahan_terakhir_nama_notaris', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'akta_perubahan_terakhir_nama_notaris', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'akta_perubahan_terakhir_nama_notaris', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'akta_perubahan_terakhir_nama_notaris')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'akta_perubahan_terakhir_tanggal', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'akta_perubahan_terakhir_tanggal', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'akta_perubahan_terakhir_tanggal', ['class' => 'form-control']) ?>
            <?= Html::error($model['identitas_perusahaan'], 'akta_perubahan_terakhir_tanggal', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'akta_perubahan_terakhir_tanggal')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'akta_perubahan_terakhir_nomor_pengesahan', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'akta_perubahan_terakhir_nomor_pengesahan', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'akta_perubahan_terakhir_nomor_pengesahan', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'akta_perubahan_terakhir_nomor_pengesahan', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'akta_perubahan_terakhir_nomor_pengesahan')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'akta_perubahan_terakhir_tanggal_pengesahan', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'akta_perubahan_terakhir_tanggal_pengesahan', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'akta_perubahan_terakhir_tanggal_pengesahan', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'akta_perubahan_terakhir_tanggal_pengesahan', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'akta_perubahan_terakhir_tanggal_pengesahan')->end(); ?>
    </div>
    
    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['identitas_perusahaan']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>