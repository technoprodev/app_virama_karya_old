<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['identitas_perusahaan']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['identitas_perusahaan'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-left-30 m-margin-left-0">
    <div class="box-8">
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <div class="">
        <?= $form->field($model['identitas_perusahaan'], 'nama_perusahaan', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'nama_perusahaan', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'nama_perusahaan', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'nama_perusahaan', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'nama_perusahaan')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'status', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'status', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeDropDownList($model['identitas_perusahaan'], 'status', [ 'Pusat' => 'Pusat', 'Cabang' => 'Cabang', ], ['prompt' => 'Choose one please', 'class' => 'form-control']) ?>
            <?= Html::error($model['identitas_perusahaan'], 'status', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'status')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'alamat', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'alamat', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextArea($model['identitas_perusahaan'], 'alamat', ['class' => 'form-control', 'rows' => 6]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'alamat', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'alamat')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'nomor_telpon', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'nomor_telpon', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'nomor_telpon', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'nomor_telpon', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'nomor_telpon')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'nomor_fax', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'nomor_fax', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'nomor_fax', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'nomor_fax', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'nomor_fax')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'email_1', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'email_1', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'email_1', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'email_1', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'email_1')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'email_2', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'email_2', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'email_2', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'email_2', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'email_2')->end(); ?>
    </div>
    
    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['identitas_perusahaan']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>