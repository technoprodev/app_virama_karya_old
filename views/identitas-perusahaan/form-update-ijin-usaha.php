<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['identitas_perusahaan']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['identitas_perusahaan'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-left-30 m-margin-left-0">
    <div class="box-8">
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <div class="">
        <div class="fs-15 text-azure margin-bottom-10">
            <span class="margin-right-10"><i class="fa fa-angle-right"></i></span>Izin Usaha Jasa Konstruksi/Konsultan (IUJK)
        </div>

        <?= $form->field($model['identitas_perusahaan'], 'iujk_masa_berlaku_dari', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'iujk_masa_berlaku_dari', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'iujk_masa_berlaku_dari', ['class' => 'form-control']) ?>
            <?= Html::error($model['identitas_perusahaan'], 'iujk_masa_berlaku_dari', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'iujk_masa_berlaku_dari')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'iujk_masa_berlaku_sampai', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'iujk_masa_berlaku_sampai', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'iujk_masa_berlaku_sampai', ['class' => 'form-control']) ?>
            <?= Html::error($model['identitas_perusahaan'], 'iujk_masa_berlaku_sampai', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'iujk_masa_berlaku_sampai')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'iujk_instansi_pemberi', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'iujk_instansi_pemberi', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'iujk_instansi_pemberi', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'iujk_instansi_pemberi', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'iujk_instansi_pemberi')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'iujk_perencanaan', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'iujk_perencanaan', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'iujk_perencanaan', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'iujk_perencanaan', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'iujk_perencanaan')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'iujk_pengawasan', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'iujk_pengawasan', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'iujk_pengawasan', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'iujk_pengawasan', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'iujk_pengawasan')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'iujk_konsultansi', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'iujk_konsultansi', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'iujk_konsultansi', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'iujk_konsultansi', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'iujk_konsultansi')->end(); ?>
    </div>

    <hr class="margin-y-15">

    <div class="">
        <div class="fs-15 text-azure margin-bottom-10">
            <span class="margin-right-10"><i class="fa fa-angle-right"></i></span>Sertifikat Badan Usaha (SBU) Konstruksi
        </div>

        <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_masa_berlaku_dari', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_konstruksi_masa_berlaku_dari', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_konstruksi_masa_berlaku_dari', ['class' => 'form-control']) ?>
            <?= Html::error($model['identitas_perusahaan'], 'sbu_konstruksi_masa_berlaku_dari', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_masa_berlaku_dari')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_masa_berlaku_sampai', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_konstruksi_masa_berlaku_sampai', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_konstruksi_masa_berlaku_sampai', ['class' => 'form-control']) ?>
            <?= Html::error($model['identitas_perusahaan'], 'sbu_konstruksi_masa_berlaku_sampai', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_masa_berlaku_sampai')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_instansi_pemberi_izin', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_konstruksi_instansi_pemberi_izin', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_konstruksi_instansi_pemberi_izin', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'sbu_konstruksi_instansi_pemberi_izin', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_instansi_pemberi_izin')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_perencanaan_arsitektur', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_konstruksi_perencanaan_arsitektur', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_konstruksi_perencanaan_arsitektur', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'sbu_konstruksi_perencanaan_arsitektur', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_perencanaan_arsitektur')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_pengawasan_arsitektur', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_konstruksi_pengawasan_arsitektur', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_konstruksi_pengawasan_arsitektur', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'sbu_konstruksi_pengawasan_arsitektur', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_pengawasan_arsitektur')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_jasa_konsultansi_lainnya', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_konstruksi_jasa_konsultansi_lainnya', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_konstruksi_jasa_konsultansi_lainnya', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'sbu_konstruksi_jasa_konsultansi_lainnya', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_jasa_konsultansi_lainnya')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_perencana_penataan_ruang', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_konstruksi_perencana_penataan_ruang', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_konstruksi_perencana_penataan_ruang', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'sbu_konstruksi_perencana_penataan_ruang', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_perencana_penataan_ruang')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_pengawasan_penataan_ruang', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_konstruksi_pengawasan_penataan_ruang', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_konstruksi_pengawasan_penataan_ruang', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'sbu_konstruksi_pengawasan_penataan_ruang', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_pengawasan_penataan_ruang')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_perencanaan_rekayasa', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_konstruksi_perencanaan_rekayasa', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_konstruksi_perencanaan_rekayasa', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'sbu_konstruksi_perencanaan_rekayasa', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_perencanaan_rekayasa')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_pengawasan_rekayasa', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_konstruksi_pengawasan_rekayasa', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_konstruksi_pengawasan_rekayasa', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'sbu_konstruksi_pengawasan_rekayasa', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_pengawasan_rekayasa')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_konsultan_spesialis', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_konstruksi_konsultan_spesialis', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_konstruksi_konsultan_spesialis', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'sbu_konstruksi_konsultan_spesialis', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_konsultan_spesialis')->end(); ?>
    </div>

    <hr class="margin-y-15">

    <div class="">
        <div class="fs-15 text-azure margin-bottom-10">
            <span class="margin-right-10"><i class="fa fa-angle-right"></i></span>Sertifikat Badan Usaha (SBU) Non Konstruksi
        </div>

        <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljspdbt', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljspdbt', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljspdbt', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljspdbt', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljspdbt')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm_masa_berlaku_dari', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm_masa_berlaku_dari', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm_masa_berlaku_dari', ['class' => 'form-control']) ?>
            <?= Html::error($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm_masa_berlaku_dari', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm_masa_berlaku_dari')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm_masa_berlaku_sampai', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm_masa_berlaku_sampai', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm_masa_berlaku_sampai', ['class' => 'form-control']) ?>
            <?= Html::error($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm_masa_berlaku_sampai', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm_masa_berlaku_sampai')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm_instansi_pemberi_izin', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm_instansi_pemberi_izin', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm_instansi_pemberi_izin', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm_instansi_pemberi_izin', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm_instansi_pemberi_izin')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_pengembangan_pertanian_dan_pedesaan', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_non_konstruksi_pengembangan_pertanian_dan_pedesaan', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_non_konstruksi_pengembangan_pertanian_dan_pedesaan', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'sbu_non_konstruksi_pengembangan_pertanian_dan_pedesaan', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_pengembangan_pertanian_dan_pedesaan')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_transportasi', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_non_konstruksi_transportasi', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_non_konstruksi_transportasi', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'sbu_non_konstruksi_transportasi', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_transportasi')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_telematika', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_non_konstruksi_telematika', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_non_konstruksi_telematika', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'sbu_non_konstruksi_telematika', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_telematika')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan_masa_berlaku_dari', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan_masa_berlaku_dari', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan_masa_berlaku_dari', ['class' => 'form-control']) ?>
            <?= Html::error($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan_masa_berlaku_dari', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan_masa_berlaku_dari')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan_masa_berlaku_sampai', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan_masa_berlaku_sampai', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan_masa_berlaku_sampai', ['class' => 'form-control']) ?>
            <?= Html::error($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan_masa_berlaku_sampai', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan_masa_berlaku_sampai')->end(); ?>

        <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan_instansi_pemberi_izin', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
            <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan_instansi_pemberi_izin', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan_instansi_pemberi_izin', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan_instansi_pemberi_izin', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
        <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan_instansi_pemberi_izin')->end(); ?>
    </div>
    
    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['identitas_perusahaan']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>