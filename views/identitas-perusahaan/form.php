<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['identitas_perusahaan']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['identitas_perusahaan'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-left-30 m-margin-left-0">
    <div class="box-8">
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model['identitas_perusahaan'], 'nama_perusahaan', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'nama_perusahaan', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'nama_perusahaan', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'nama_perusahaan', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'nama_perusahaan')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'status', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'status', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeDropDownList($model['identitas_perusahaan'], 'status', [ 'Pusat' => 'Pusat', 'Cabang' => 'Cabang', ], ['prompt' => 'Choose one please', 'class' => 'form-control']) ?>
        <?= Html::error($model['identitas_perusahaan'], 'status', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'status')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'alamat', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'alamat', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextArea($model['identitas_perusahaan'], 'alamat', ['class' => 'form-control', 'rows' => 6]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'alamat', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'alamat')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'nomor_telpon', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'nomor_telpon', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'nomor_telpon', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'nomor_telpon', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'nomor_telpon')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'nomor_fax', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'nomor_fax', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'nomor_fax', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'nomor_fax', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'nomor_fax')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'email_1', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'email_1', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'email_1', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'email_1', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'email_1')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'email_2', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'email_2', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'email_2', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'email_2', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'email_2')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_nomor_akta', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_nomor_akta', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_nomor_akta', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_nomor_akta', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_nomor_akta')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_tanggal', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_tanggal', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_tanggal', ['class' => 'form-control']) ?>
        <?= Html::error($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_tanggal', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_tanggal')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_nama_notaris', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_nama_notaris', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_nama_notaris', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_nama_notaris', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_nama_notaris')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_nomor_pengesahan', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_nomor_pengesahan', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_nomor_pengesahan', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_nomor_pengesahan', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_nomor_pengesahan')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_tanggal_pengesahan', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_tanggal_pengesahan', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_tanggal_pengesahan', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_tanggal_pengesahan', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'akta_pendirian_perusahaan_tanggal_pengesahan')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'akta_perubahan_terakhir_nomor_akta', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'akta_perubahan_terakhir_nomor_akta', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'akta_perubahan_terakhir_nomor_akta', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'akta_perubahan_terakhir_nomor_akta', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'akta_perubahan_terakhir_nomor_akta')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'akta_perubahan_terakhir_nama_notaris', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'akta_perubahan_terakhir_nama_notaris', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'akta_perubahan_terakhir_nama_notaris', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'akta_perubahan_terakhir_nama_notaris', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'akta_perubahan_terakhir_nama_notaris')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'akta_perubahan_terakhir_tanggal', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'akta_perubahan_terakhir_tanggal', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'akta_perubahan_terakhir_tanggal', ['class' => 'form-control']) ?>
        <?= Html::error($model['identitas_perusahaan'], 'akta_perubahan_terakhir_tanggal', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'akta_perubahan_terakhir_tanggal')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'akta_perubahan_terakhir_nomor_pengesahan', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'akta_perubahan_terakhir_nomor_pengesahan', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'akta_perubahan_terakhir_nomor_pengesahan', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'akta_perubahan_terakhir_nomor_pengesahan', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'akta_perubahan_terakhir_nomor_pengesahan')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'akta_perubahan_terakhir_tanggal_pengesahan', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'akta_perubahan_terakhir_tanggal_pengesahan', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'akta_perubahan_terakhir_tanggal_pengesahan', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'akta_perubahan_terakhir_tanggal_pengesahan', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'akta_perubahan_terakhir_tanggal_pengesahan')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'iujk_masa_berlaku_dari', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'iujk_masa_berlaku_dari', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'iujk_masa_berlaku_dari', ['class' => 'form-control']) ?>
        <?= Html::error($model['identitas_perusahaan'], 'iujk_masa_berlaku_dari', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'iujk_masa_berlaku_dari')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'iujk_masa_berlaku_sampai', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'iujk_masa_berlaku_sampai', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'iujk_masa_berlaku_sampai', ['class' => 'form-control']) ?>
        <?= Html::error($model['identitas_perusahaan'], 'iujk_masa_berlaku_sampai', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'iujk_masa_berlaku_sampai')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'iujk_instansi_pemberi', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'iujk_instansi_pemberi', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'iujk_instansi_pemberi', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'iujk_instansi_pemberi', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'iujk_instansi_pemberi')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'iujk_perencanaan', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'iujk_perencanaan', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'iujk_perencanaan', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'iujk_perencanaan', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'iujk_perencanaan')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'iujk_pengawasan', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'iujk_pengawasan', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'iujk_pengawasan', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'iujk_pengawasan', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'iujk_pengawasan')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'iujk_konsultansi', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'iujk_konsultansi', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'iujk_konsultansi', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'iujk_konsultansi', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'iujk_konsultansi')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_masa_berlaku_dari', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_konstruksi_masa_berlaku_dari', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_konstruksi_masa_berlaku_dari', ['class' => 'form-control']) ?>
        <?= Html::error($model['identitas_perusahaan'], 'sbu_konstruksi_masa_berlaku_dari', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_masa_berlaku_dari')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_masa_berlaku_sampai', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_konstruksi_masa_berlaku_sampai', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_konstruksi_masa_berlaku_sampai', ['class' => 'form-control']) ?>
        <?= Html::error($model['identitas_perusahaan'], 'sbu_konstruksi_masa_berlaku_sampai', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_masa_berlaku_sampai')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_instansi_pemberi_izin', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_konstruksi_instansi_pemberi_izin', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_konstruksi_instansi_pemberi_izin', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'sbu_konstruksi_instansi_pemberi_izin', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_instansi_pemberi_izin')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_perencanaan_arsitektur', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_konstruksi_perencanaan_arsitektur', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_konstruksi_perencanaan_arsitektur', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'sbu_konstruksi_perencanaan_arsitektur', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_perencanaan_arsitektur')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_pengawasan_arsitektur', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_konstruksi_pengawasan_arsitektur', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_konstruksi_pengawasan_arsitektur', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'sbu_konstruksi_pengawasan_arsitektur', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_pengawasan_arsitektur')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_jasa_konsultansi_lainnya', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_konstruksi_jasa_konsultansi_lainnya', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_konstruksi_jasa_konsultansi_lainnya', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'sbu_konstruksi_jasa_konsultansi_lainnya', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_jasa_konsultansi_lainnya')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_perencana_penataan_ruang', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_konstruksi_perencana_penataan_ruang', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_konstruksi_perencana_penataan_ruang', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'sbu_konstruksi_perencana_penataan_ruang', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_perencana_penataan_ruang')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_pengawasan_penataan_ruang', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_konstruksi_pengawasan_penataan_ruang', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_konstruksi_pengawasan_penataan_ruang', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'sbu_konstruksi_pengawasan_penataan_ruang', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_pengawasan_penataan_ruang')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_perencanaan_rekayasa', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_konstruksi_perencanaan_rekayasa', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_konstruksi_perencanaan_rekayasa', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'sbu_konstruksi_perencanaan_rekayasa', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_perencanaan_rekayasa')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_pengawasan_rekayasa', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_konstruksi_pengawasan_rekayasa', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_konstruksi_pengawasan_rekayasa', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'sbu_konstruksi_pengawasan_rekayasa', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_pengawasan_rekayasa')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_konsultan_spesialis', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_konstruksi_konsultan_spesialis', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_konstruksi_konsultan_spesialis', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'sbu_konstruksi_konsultan_spesialis', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'sbu_konstruksi_konsultan_spesialis')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljspdbt', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljspdbt', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljspdbt', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljspdbt', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljspdbt')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm_masa_berlaku_dari', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm_masa_berlaku_dari', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm_masa_berlaku_dari', ['class' => 'form-control']) ?>
        <?= Html::error($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm_masa_berlaku_dari', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm_masa_berlaku_dari')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm_masa_berlaku_sampai', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm_masa_berlaku_sampai', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm_masa_berlaku_sampai', ['class' => 'form-control']) ?>
        <?= Html::error($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm_masa_berlaku_sampai', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm_masa_berlaku_sampai')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm_instansi_pemberi_izin', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm_instansi_pemberi_izin', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm_instansi_pemberi_izin', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm_instansi_pemberi_izin', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_ljskm_instansi_pemberi_izin')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_pengembangan_pertanian_dan_pedesaan', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_non_konstruksi_pengembangan_pertanian_dan_pedesaan', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_non_konstruksi_pengembangan_pertanian_dan_pedesaan', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'sbu_non_konstruksi_pengembangan_pertanian_dan_pedesaan', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_pengembangan_pertanian_dan_pedesaan')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_transportasi', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_non_konstruksi_transportasi', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_non_konstruksi_transportasi', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'sbu_non_konstruksi_transportasi', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_transportasi')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_telematika', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_non_konstruksi_telematika', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_non_konstruksi_telematika', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'sbu_non_konstruksi_telematika', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_telematika')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan_masa_berlaku_dari', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan_masa_berlaku_dari', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan_masa_berlaku_dari', ['class' => 'form-control']) ?>
        <?= Html::error($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan_masa_berlaku_dari', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan_masa_berlaku_dari')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan_masa_berlaku_sampai', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan_masa_berlaku_sampai', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan_masa_berlaku_sampai', ['class' => 'form-control']) ?>
        <?= Html::error($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan_masa_berlaku_sampai', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan_masa_berlaku_sampai')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan_instansi_pemberi_izin', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan_instansi_pemberi_izin', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan_instansi_pemberi_izin', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan_instansi_pemberi_izin', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'sbu_non_konstruksi_pendidikan_instansi_pemberi_izin')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'pajak_nomor_pokok_wajib_pajak', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'pajak_nomor_pokok_wajib_pajak', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'pajak_nomor_pokok_wajib_pajak', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'pajak_nomor_pokok_wajib_pajak', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'pajak_nomor_pokok_wajib_pajak')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'pajak_surat_pengukuhan_kena_pajak', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'pajak_surat_pengukuhan_kena_pajak', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'pajak_surat_pengukuhan_kena_pajak', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'pajak_surat_pengukuhan_kena_pajak', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'pajak_surat_pengukuhan_kena_pajak')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'pajak_surat_keterangan_terdaftar', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'pajak_surat_keterangan_terdaftar', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'pajak_surat_keterangan_terdaftar', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'pajak_surat_keterangan_terdaftar', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'pajak_surat_keterangan_terdaftar')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'pajak_bpptt_nomor', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'pajak_bpptt_nomor', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'pajak_bpptt_nomor', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'pajak_bpptt_nomor', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'pajak_bpptt_nomor')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'pajak_bpptt_tanggal', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'pajak_bpptt_tanggal', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'pajak_bpptt_tanggal', ['class' => 'form-control']) ?>
        <?= Html::error($model['identitas_perusahaan'], 'pajak_bpptt_tanggal', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'pajak_bpptt_tanggal')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'pajak_skf_nomor', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'pajak_skf_nomor', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'pajak_skf_nomor', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['identitas_perusahaan'], 'pajak_skf_nomor', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'pajak_skf_nomor')->end(); ?>

    <?= $form->field($model['identitas_perusahaan'], 'pajak_skf_tanggal', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
        <?= Html::activeLabel($model['identitas_perusahaan'], 'pajak_skf_tanggal', ['class' => 'control-label fs-12']); ?>
        <?= Html::activeTextInput($model['identitas_perusahaan'], 'pajak_skf_tanggal', ['class' => 'form-control']) ?>
        <?= Html::error($model['identitas_perusahaan'], 'pajak_skf_tanggal', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
    <?= $form->field($model['identitas_perusahaan'], 'pajak_skf_tanggal')->end(); ?>


    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['identitas_perusahaan']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>