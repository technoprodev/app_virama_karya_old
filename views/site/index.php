<?php

use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Home';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="margin-bottom-30">
	<div class="text-rose fs-18 margin-bottom-15">Early Warning</div>
	<div class="padding-left-10 m-padding-left-10">Sedang dalam proses upload</div>
</div>

<div class="margin-bottom-30">
	<div class="text-rose fs-18 margin-bottom-15">Daftar Project Menang</div>
	<div class="padding-left-10 m-padding-left-10">Sedang dalam proses upload</div>
</div>

<div class="margin-bottom-30">
	<div class="text-rose fs-18 margin-bottom-15">Daftar Project Kalah</div>
	<div class="padding-left-10 m-padding-left-10">Sedang dalam proses upload</div>
</div>

<div class="margin-bottom-30">
	<div class="text-rose fs-18 margin-bottom-15">Daftar Project On Progres</div>
	<div class="padding-left-10 m-padding-left-10">Sedang dalam proses upload</div>
</div>