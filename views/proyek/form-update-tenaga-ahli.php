<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

//
$proyekTenagaAhli = [];
if (isset($model['proyek_tenaga_ahli']))
    foreach ($model['proyek_tenaga_ahli'] as $key => $proyekTenagaAhli)
        $proyekTenagaAhli[] = $proyekTenagaAhli->attributes;

$tenagaAhlis = ArrayHelper::map(\app_virama_karya\models\TenagaAhli::find()->asArray()->all(), 'id', 'nama');
$option_tenaga_ahli = [];
foreach ($tenagaAhlis as $key => $tenaga_ahli) {
    $temp['value'] = $key;
    $temp['text'] = $tenaga_ahli;

    $option_tenaga_ahli[] = $temp;
}

$penugasans = ArrayHelper::map(\app_virama_karya\models\Penugasan::find()->asArray()->all(), 'id', 'nama_penugasan');
$option_penugasan = [];
foreach ($penugasans as $key => $penugasan) {
    $temp['value'] = $key;
    $temp['text'] = $penugasan;

    $option_penugasan[] = $temp;
}

// Yii::$app->d->ddx($penugasans);

$this->registerJs(
    'vm.$data.option_tenaga_ahli = vm.$data.option_tenaga_ahli.concat(' . json_encode($option_tenaga_ahli) . ');' .
    'vm.$data.option_penugasan = vm.$data.option_penugasan.concat(' . json_encode($option_penugasan) . ');' .
    'vm.$data.proyek.proyekTenagaAhlis = vm.$data.proyek.proyekTenagaAhlis.concat(' . json_encode($proyekTenagaAhli) . ');',
    3
);

//
$error = false;
$errorMessage = '';
$errorVue = false;
if ($model['proyek']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['proyek'], ['class' => '']);
}

if (isset($model['proyek_tenaga_ahli'])) foreach ($model['proyek_tenaga_ahli'] as $key => $proyekTenagaAhli) {
    if ($proyekTenagaAhli->hasErrors()) {
        $error = true;
        $errorMessage .= Html::errorSummary($proyekTenagaAhli, ['class' => '']);
        $errorVue = true; 
    }
}
if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-left-30 m-margin-left-0">
    <div class="box-8">
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <div class="margin-bottom-10">
        <div class=""><?= $model['proyek']->attributeLabels()['kode_lelang'] ?></div>
        <div class="text-azure"><?= $model['proyek']->kode_lelang ? $model['proyek']->kode_lelang : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
    </div>
        
    <div class="margin-bottom-10">
        <div class=""><?= $model['proyek']->attributeLabels()['nama_proyek'] ?></div>
        <div class="text-azure"><?= $model['proyek']->nama_proyek ? $model['proyek']->nama_proyek : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
    </div>

    <hr>

    <?php if (isset($model['proyek_tenaga_ahli'])) foreach ($model['proyek_tenaga_ahli'] as $key => $value): ?>
        <?= $form->field($model['proyek_tenaga_ahli'][$key], "[$key]id_tenaga_ahli")->begin(); ?>
        <?= $form->field($model['proyek_tenaga_ahli'][$key], "[$key]id_tenaga_ahli")->end(); ?>
    <?php endforeach; ?>

    <template v-if="typeof proyek.proyekTenagaAhlis == 'object'">
        <template v-for="(value, key, index) in proyek.proyekTenagaAhlis">
            <div v-show="!(value.id < 0)">
                <div class="fs-16 margin-bottom-15 clearfix">
                        Tenaga Ahli {{key+1}} : <span class="f-bold">{{proyek.proyekTenagaAhlis[key].nama_tenaga_ahli}}</span>
                    <div v-on:click="removeProyekTenagaAhli(key)"  class="pull-right hover-pointer text-red">delete</div>
                </div>
                <div class="padding-left-5">
                    <input type="hidden" v-bind:id="'proyektenagaahli-' + key + '-id'" v-bind:name="'ProyekTenagaAhli[' + key + '][id]'" class="form-control" type="text" v-model="proyek.proyekTenagaAhlis[key].id">

                    <div v-bind:class="'form-group field-proyektenagaahli-' + key + '-id_tenaga_ahli'">
                        <label v-bind:for="'proyektenagaahli-' + key + '-id_tenaga_ahli'" class="control-label">Tenaga Ahli</label>
                        <select v-bind:id="'proyektenagaahli-' + key + '-id_tenaga_ahli'" v-bind:name="'ProyekTenagaAhli[' + key + '][id_tenaga_ahli]'" class="form-control" v-model="proyek.proyekTenagaAhlis[key].id_tenaga_ahli">
                            <option v-for="option in option_tenaga_ahli" v-bind:value="option.value">
                                {{ option.text }}
                            </option>
                        </select>
                        <div class="help-block"></div>
                    </div>

                    <div v-bind:class="'form-group field-proyektenagaahli-' + key + '-id_penugasan'">
                        <label v-bind:for="'proyektenagaahli-' + key + '-id_penugasan'" class="control-label">Penugasan</label>
                        <select v-bind:id="'proyektenagaahli-' + key + '-id_penugasan'" v-bind:name="'ProyekTenagaAhli[' + key + '][id_penugasan]'" class="form-control" v-model="proyek.proyekTenagaAhlis[key].id_penugasan">
                            <option v-for="option in option_penugasan" v-bind:value="option.value">
                                {{ option.text }}
                            </option>
                        </select>
                        <div class="help-block"></div>
                    </div>

                    <div v-bind:class="'form-group field-proyektenagaahli-' + key + '-periode_awal'">
                        <label v-bind:for="'proyektenagaahli-' + key + '-periode_awal'" class="control-label">Periode Awal</label>
                        <input v-bind:id="'proyektenagaahli-' + key + '-periode_awal'" v-bind:name="'ProyekTenagaAhli[' + key + '][periode_awal]'" class="form-control" type="text" v-model="proyek.proyekTenagaAhlis[key].periode_awal">
                        <div class="help-block"></div>
                    </div>

                    <div v-bind:class="'form-group field-proyektenagaahli-' + key + '-periode_akhir'">
                        <label v-bind:for="'proyektenagaahli-' + key + '-periode_akhir'" class="control-label">Periode Akhir</label>
                        <input v-bind:id="'proyektenagaahli-' + key + '-periode_akhir'" v-bind:name="'ProyekTenagaAhli[' + key + '][periode_akhir]'" class="form-control" type="text" v-model="proyek.proyekTenagaAhlis[key].periode_akhir">
                        <div class="help-block"></div>
                    </div>
                </div>
            </div>
        </template>
    </template>

    <a v-on:click="addProyekTenagaAhli" class="btn btn-default">Add Tenaga Ahli</a>

    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['proyek']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>