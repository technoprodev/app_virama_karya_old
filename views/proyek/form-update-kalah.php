<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['proyek']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['proyek'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-left-30 m-margin-left-0">
    <div class="box-8">
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <div class="margin-bottom-10">
        <div class=""><?= $model['proyek']->attributeLabels()['kode_lelang'] ?></div>
        <div class="text-azure"><?= $model['proyek']->kode_lelang ? $model['proyek']->kode_lelang : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
    </div>
        
    <div class="margin-bottom-10">
        <div class=""><?= $model['proyek']->attributeLabels()['nama_proyek'] ?></div>
        <div class="text-azure"><?= $model['proyek']->nama_proyek ? $model['proyek']->nama_proyek : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
    </div>

    <?= $form->field($model['proyek'], 'score')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'score', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['proyek'], 'score', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['proyek'], 'score', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'score')->end(); ?>


    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton('Kalah', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>