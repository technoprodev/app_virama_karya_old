<?php

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/proyek/list-lelang-kalah.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<table class="datatables display nowrap table table-striped table-hover table-condensed">
    <thead>
        <tr>
            <th class="text-dark f-normal" style="border-bottom: 1px">Action</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Kode Lelang</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Nama Proyek</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Nomor Kontrak</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Tanggal Kontrak</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Periode Awal</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Periode Akhir</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Status</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Status Keterangan</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Keterangan</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Id Instansi</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Id Satuan Kerja</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Id Sub Bidang</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Lokasi</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Metode Pengadaan</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Metode Kualifikasi</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Metode Dokumen</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Metode Evaluasi</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Cara Pembayaran</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Pembebanan Tahun Anggaran</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Sumber Dana</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Nilai Pagu Paket</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Nilai Hps Paket</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Nilai Kontrak</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Anggaran</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Bobot Teknis</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Bobot Biaya</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Dokumen Pakta Integritas</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Dokumen Surat Minat</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Dokumen Surat Pernyataan</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Sharing</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Tanggal Mulai</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Tanggal Selesai</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Uraian Pekerjaan</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Lingkup Pekerjaan</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Nomor Bast</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Tanggal Bast</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Nomor Spmk</th>
        </tr>
        <tr class="dt-search">
            <th class="padding-0"></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search kode lelang" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search nama proyek" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search nomor kontrak" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search tanggal kontrak" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search periode awal" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search periode akhir" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search status" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search status keterangan" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search keterangan" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search id instansi" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search id satuan kerja" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search id sub bidang" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search lokasi" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search metode pengadaan" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search metode kualifikasi" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search metode dokumen" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search metode evaluasi" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search cara pembayaran" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search pembebanan tahun anggaran" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search sumber dana" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search nilai pagu paket" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search nilai hps paket" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search nilai kontrak" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search anggaran" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search bobot teknis" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search bobot biaya" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search dokumen pakta integritas" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search dokumen surat minat" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search dokumen surat pernyataan" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search sharing" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search tanggal mulai" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search tanggal selesai" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search uraian pekerjaan" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search lingkup pekerjaan" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search nomor bast" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search tanggal bast" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search nomor spmk" class="form-control border-none f-normal padding-x-5"/></th>
        </tr>
    </thead>
</table>