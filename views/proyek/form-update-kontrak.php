<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['proyek']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['proyek'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-left-30 m-margin-left-0">
    <div class="box-8">
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <div class="box box-break-sm margin-bottom-10">
        <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['kode_lelang'] ?></div>
        <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->kode_lelang ? $model['proyek']->kode_lelang : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
    </div>
        
    <div class="box box-break-sm margin-bottom-10">
        <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['nama_proyek'] ?></div>
        <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->nama_proyek ? $model['proyek']->nama_proyek : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
    </div>

    <?= $form->field($model['proyek'], 'nomor_kontrak')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'nomor_kontrak', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['proyek'], 'nomor_kontrak', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['proyek'], 'nomor_kontrak', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'nomor_kontrak')->end(); ?>

    <?= $form->field($model['proyek'], 'tanggal_kontrak')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'tanggal_kontrak', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['proyek'], 'tanggal_kontrak', ['class' => 'form-control']) ?>
        <?= Html::error($model['proyek'], 'tanggal_kontrak', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'tanggal_kontrak')->end(); ?>

    <?= $form->field($model['proyek'], 'nilai_kontrak')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'nilai_kontrak', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['proyek'], 'nilai_kontrak', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['proyek'], 'nilai_kontrak', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'nilai_kontrak')->end(); ?>

    <?= $form->field($model['proyek'], 'periode_awal')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'periode_awal', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['proyek'], 'periode_awal', ['class' => 'form-control']) ?>
        <?= Html::error($model['proyek'], 'periode_awal', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'periode_awal')->end(); ?>

    <?= $form->field($model['proyek'], 'periode_akhir')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'periode_akhir', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['proyek'], 'periode_akhir', ['class' => 'form-control']) ?>
        <?= Html::error($model['proyek'], 'periode_akhir', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'periode_akhir')->end(); ?>

    <?= $form->field($model['proyek'], 'status')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'status', ['class' => 'control-label']); ?>
        <?= Html::activeDropDownList($model['proyek'], 'status', [ 'Calon' => 'Calon', 'Lelang' => 'Lelang', 'Kalah' => 'Kalah', 'Kontrak' => 'Kontrak', 'Selesai' => 'Selesai', ], ['prompt' => 'Choose one please', 'class' => 'form-control']) ?>
        <?= Html::error($model['proyek'], 'status', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'status')->end(); ?>

    <?= $form->field($model['proyek'], 'status_keterangan')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'status_keterangan', ['class' => 'control-label']); ?>
        <?= Html::activeTextArea($model['proyek'], 'status_keterangan', ['class' => 'form-control', 'rows' => 6]) ?>
        <?= Html::error($model['proyek'], 'status_keterangan', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'status_keterangan')->end(); ?>

    <?= $form->field($model['proyek'], 'keterangan')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'keterangan', ['class' => 'control-label']); ?>
        <?= Html::activeTextArea($model['proyek'], 'keterangan', ['class' => 'form-control', 'rows' => 6]) ?>
        <?= Html::error($model['proyek'], 'keterangan', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'keterangan')->end(); ?>

    <?= $form->field($model['proyek'], 'nilai_kontrak')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'nilai_kontrak', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['proyek'], 'nilai_kontrak', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['proyek'], 'nilai_kontrak', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'nilai_kontrak')->end(); ?>

    <?= $form->field($model['proyek'], 'sharing')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'sharing', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['proyek'], 'sharing', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['proyek'], 'sharing', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'sharing')->end(); ?>

    <?= $form->field($model['proyek'], 'tanggal_mulai')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'tanggal_mulai', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['proyek'], 'tanggal_mulai', ['class' => 'form-control']) ?>
        <?= Html::error($model['proyek'], 'tanggal_mulai', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'tanggal_mulai')->end(); ?>

    <?= $form->field($model['proyek'], 'tanggal_selesai')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'tanggal_selesai', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['proyek'], 'tanggal_selesai', ['class' => 'form-control']) ?>
        <?= Html::error($model['proyek'], 'tanggal_selesai', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'tanggal_selesai')->end(); ?>

    <?= $form->field($model['proyek'], 'uraian_pekerjaan')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'uraian_pekerjaan', ['class' => 'control-label']); ?>
        <?= Html::activeTextArea($model['proyek'], 'uraian_pekerjaan', ['class' => 'form-control', 'rows' => 6]) ?>
        <?= Html::error($model['proyek'], 'uraian_pekerjaan', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'uraian_pekerjaan')->end(); ?>

    <?= $form->field($model['proyek'], 'lingkup_pekerjaan')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'lingkup_pekerjaan', ['class' => 'control-label']); ?>
        <?= Html::activeTextArea($model['proyek'], 'lingkup_pekerjaan', ['class' => 'form-control', 'rows' => 6]) ?>
        <?= Html::error($model['proyek'], 'lingkup_pekerjaan', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'lingkup_pekerjaan')->end(); ?>

    <?= $form->field($model['proyek'], 'nomor_bast')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'nomor_bast', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['proyek'], 'nomor_bast', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['proyek'], 'nomor_bast', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'nomor_bast')->end(); ?>

    <?= $form->field($model['proyek'], 'tanggal_bast')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'tanggal_bast', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['proyek'], 'tanggal_bast', ['class' => 'form-control']) ?>
        <?= Html::error($model['proyek'], 'tanggal_bast', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'tanggal_bast')->end(); ?>

    <?= $form->field($model['proyek'], 'nomor_spmk')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'nomor_spmk', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['proyek'], 'nomor_spmk', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['proyek'], 'nomor_spmk', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'nomor_spmk')->end(); ?>


    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['proyek']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>