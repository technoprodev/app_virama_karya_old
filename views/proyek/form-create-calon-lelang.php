<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['proyek']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['proyek'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-left-30 m-margin-left-0">
    <div class="box-8">
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model['proyek'], 'kode_lelang')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'kode_lelang', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['proyek'], 'kode_lelang', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['proyek'], 'kode_lelang', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'kode_lelang')->end(); ?>

    <?= $form->field($model['proyek'], 'nama_proyek')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'nama_proyek', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['proyek'], 'nama_proyek', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['proyek'], 'nama_proyek', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'nama_proyek')->end(); ?>

    <!-- <?= $form->field($model['proyek'], 'status_keterangan')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'status_keterangan', ['class' => 'control-label']); ?>
        <?= Html::activeTextArea($model['proyek'], 'status_keterangan', ['class' => 'form-control', 'rows' => 6]) ?>
        <?= Html::error($model['proyek'], 'status_keterangan', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'status_keterangan')->end(); ?> -->

    <?= $form->field($model['proyek'], 'keterangan')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'keterangan', ['class' => 'control-label']); ?>
        <?= Html::activeTextArea($model['proyek'], 'keterangan', ['class' => 'form-control', 'rows' => 6]) ?>
        <?= Html::error($model['proyek'], 'keterangan', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'keterangan')->end(); ?>

    <?= $form->field($model['proyek'], 'id_instansi')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'id_instansi', ['class' => 'control-label', 'label' => 'Instansi']); ?>
        <?= Html::activeDropDownList($model['proyek'], 'id_instansi', ArrayHelper::map(\app_virama_karya\models\Instansi::find()->asArray()->all(), 'id', 'nama_instansi'), ['prompt' => 'Choose one please', 'class' => 'form-control']); ?>
        <?= Html::error($model['proyek'], 'id_instansi', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'id_instansi')->end(); ?>

    <?= $form->field($model['proyek'], 'id_satminkal')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'id_satminkal', ['class' => 'control-label', 'label' => 'Satminkal']); ?>
        <?= Html::activeDropDownList($model['proyek'], 'id_satminkal', ArrayHelper::map(\app_virama_karya\models\Satminkal::find()->asArray()->all(), 'id', 'nama_satminkal'), ['prompt' => 'Choose one please', 'class' => 'form-control']); ?>
        <?= Html::error($model['proyek'], 'id_satminkal', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'id_satminkal')->end(); ?>

    <?= $form->field($model['proyek'], 'id_satuan_kerja')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'id_satuan_kerja', ['class' => 'control-label', 'label' => 'Satuan Kerja']); ?>
        <?= Html::activeDropDownList($model['proyek'], 'id_satuan_kerja', ArrayHelper::map(\app_virama_karya\models\SatuanKerja::find()->asArray()->all(), 'id', 'nama_satuan_kerja'), ['prompt' => 'Choose one please', 'class' => 'form-control']); ?>
        <?= Html::error($model['proyek'], 'id_satuan_kerja', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'id_satuan_kerja')->end(); ?>

    <?= $form->field($model['proyek'], 'id_sub_bidang')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'id_sub_bidang', ['class' => 'control-label', 'label' => 'Kategori']); ?>
        <?= Html::activeDropDownList($model['proyek'], 'id_sub_bidang', ArrayHelper::map(\app_virama_karya\models\SubBidang::find()->asArray()->all(), 'id', 'nama_sub_bidang'), ['prompt' => 'Choose one please', 'class' => 'form-control']); ?>
        <?= Html::error($model['proyek'], 'id_sub_bidang', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'id_sub_bidang')->end(); ?>

    <?= $form->field($model['proyek'], 'aanwizjing')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'aanwizjing', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['proyek'], 'aanwizjing', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['proyek'], 'aanwizjing', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'aanwizjing')->end(); ?>

    <?= $form->field($model['proyek'], 'penandatanganan_shortlist')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'penandatanganan_shortlist', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['proyek'], 'penandatanganan_shortlist', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['proyek'], 'penandatanganan_shortlist', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'penandatanganan_shortlist')->end(); ?>

    <?= $form->field($model['proyek'], 'lokasi')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'lokasi', ['class' => 'control-label']); ?>
        <?= Html::activeTextArea($model['proyek'], 'lokasi', ['class' => 'form-control', 'rows' => 6]) ?>
        <?= Html::error($model['proyek'], 'lokasi', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'lokasi')->end(); ?>

    <?= $form->field($model['proyek'], 'metode_pengadaan')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'metode_pengadaan', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['proyek'], 'metode_pengadaan', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['proyek'], 'metode_pengadaan', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'metode_pengadaan')->end(); ?>

    <?= $form->field($model['proyek'], 'metode_kualifikasi')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'metode_kualifikasi', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['proyek'], 'metode_kualifikasi', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['proyek'], 'metode_kualifikasi', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'metode_kualifikasi')->end(); ?>

    <?= $form->field($model['proyek'], 'metode_dokumen')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'metode_dokumen', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['proyek'], 'metode_dokumen', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['proyek'], 'metode_dokumen', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'metode_dokumen')->end(); ?>

    <?= $form->field($model['proyek'], 'metode_evaluasi')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'metode_evaluasi', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['proyek'], 'metode_evaluasi', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['proyek'], 'metode_evaluasi', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'metode_evaluasi')->end(); ?>

    <?= $form->field($model['proyek'], 'cara_pembayaran')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'cara_pembayaran', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['proyek'], 'cara_pembayaran', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['proyek'], 'cara_pembayaran', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'cara_pembayaran')->end(); ?>

    <?= $form->field($model['proyek'], 'pembebanan_tahun_anggaran')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'pembebanan_tahun_anggaran', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['proyek'], 'pembebanan_tahun_anggaran', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['proyek'], 'pembebanan_tahun_anggaran', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'pembebanan_tahun_anggaran')->end(); ?>

    <?= $form->field($model['proyek'], 'sumber_dana')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'sumber_dana', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['proyek'], 'sumber_dana', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['proyek'], 'sumber_dana', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'sumber_dana')->end(); ?>

    <?= $form->field($model['proyek'], 'nilai_pagu_paket')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'nilai_pagu_paket', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['proyek'], 'nilai_pagu_paket', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['proyek'], 'nilai_pagu_paket', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'nilai_pagu_paket')->end(); ?>

    <?= $form->field($model['proyek'], 'nilai_hps_paket')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'nilai_hps_paket', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['proyek'], 'nilai_hps_paket', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['proyek'], 'nilai_hps_paket', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'nilai_hps_paket')->end(); ?>

    <?= $form->field($model['proyek'], 'bobot_teknis')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'bobot_teknis', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['proyek'], 'bobot_teknis', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['proyek'], 'bobot_teknis', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'bobot_teknis')->end(); ?>

    <?= $form->field($model['proyek'], 'bobot_biaya')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'bobot_biaya', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['proyek'], 'bobot_biaya', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['proyek'], 'bobot_biaya', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'bobot_biaya')->end(); ?>

    <!-- <?= $form->field($model['proyek'], 'sharing')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'sharing', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['proyek'], 'sharing', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['proyek'], 'sharing', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'sharing')->end(); ?>

    <?= $form->field($model['proyek'], 'tanggal_mulai')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'tanggal_mulai', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['proyek'], 'tanggal_mulai', ['class' => 'form-control']) ?>
        <?= Html::error($model['proyek'], 'tanggal_mulai', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'tanggal_mulai')->end(); ?>

    <?= $form->field($model['proyek'], 'tanggal_selesai')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'tanggal_selesai', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['proyek'], 'tanggal_selesai', ['class' => 'form-control']) ?>
        <?= Html::error($model['proyek'], 'tanggal_selesai', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'tanggal_selesai')->end(); ?>

    <?= $form->field($model['proyek'], 'uraian_pekerjaan')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'uraian_pekerjaan', ['class' => 'control-label']); ?>
        <?= Html::activeTextArea($model['proyek'], 'uraian_pekerjaan', ['class' => 'form-control', 'rows' => 6]) ?>
        <?= Html::error($model['proyek'], 'uraian_pekerjaan', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'uraian_pekerjaan')->end(); ?>

    <?= $form->field($model['proyek'], 'lingkup_pekerjaan')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'lingkup_pekerjaan', ['class' => 'control-label']); ?>
        <?= Html::activeTextArea($model['proyek'], 'lingkup_pekerjaan', ['class' => 'form-control', 'rows' => 6]) ?>
        <?= Html::error($model['proyek'], 'lingkup_pekerjaan', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'lingkup_pekerjaan')->end(); ?>

    <?= $form->field($model['proyek'], 'nomor_bast')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'nomor_bast', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['proyek'], 'nomor_bast', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['proyek'], 'nomor_bast', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'nomor_bast')->end(); ?>

    <?= $form->field($model['proyek'], 'tanggal_bast')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'tanggal_bast', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['proyek'], 'tanggal_bast', ['class' => 'form-control']) ?>
        <?= Html::error($model['proyek'], 'tanggal_bast', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'tanggal_bast')->end(); ?>

    <?= $form->field($model['proyek'], 'nomor_spmk')->begin(); ?>
        <?= Html::activeLabel($model['proyek'], 'nomor_spmk', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['proyek'], 'nomor_spmk', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['proyek'], 'nomor_spmk', ['class' => 'help-block']); ?>
    <?= $form->field($model['proyek'], 'nomor_spmk')->end(); ?> -->


    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['proyek']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>