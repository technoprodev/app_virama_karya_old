<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-top-15">
    <div class="box-6">
<?php endif; ?>

<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['id'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->id ? $model['proyek']->id : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['kode_lelang'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->kode_lelang ? $model['proyek']->kode_lelang : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['nama_proyek'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->nama_proyek ? $model['proyek']->nama_proyek : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['nomor_kontrak'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->nomor_kontrak ? $model['proyek']->nomor_kontrak : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['tanggal_kontrak'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->tanggal_kontrak ? $model['proyek']->tanggal_kontrak : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['periode_awal'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->periode_awal ? $model['proyek']->periode_awal : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['periode_akhir'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->periode_akhir ? $model['proyek']->periode_akhir : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['status'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->status ? $model['proyek']->status : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['status_keterangan'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->status_keterangan ? $model['proyek']->status_keterangan : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['keterangan'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->keterangan ? $model['proyek']->keterangan : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['id_instansi'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->id_instansi ? $model['proyek']->instansi->nama_instansi : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['id_satminkal'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->id_satminkal ? $model['proyek']->satminkal->nama_satminkal : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['id_satuan_kerja'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->id_satuan_kerja ? $model['proyek']->satuanKerja->nama_satuan_kerja : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['id_sub_bidang'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->id_sub_bidang ? $model['proyek']->subBidang->nama_sub_bidang : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['lokasi'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->lokasi ? $model['proyek']->lokasi : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['metode_pengadaan'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->metode_pengadaan ? $model['proyek']->metode_pengadaan : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['metode_kualifikasi'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->metode_kualifikasi ? $model['proyek']->metode_kualifikasi : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['metode_dokumen'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->metode_dokumen ? $model['proyek']->metode_dokumen : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['metode_evaluasi'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->metode_evaluasi ? $model['proyek']->metode_evaluasi : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['cara_pembayaran'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->cara_pembayaran ? $model['proyek']->cara_pembayaran : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['pembebanan_tahun_anggaran'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->pembebanan_tahun_anggaran ? $model['proyek']->pembebanan_tahun_anggaran : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['sumber_dana'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->sumber_dana ? $model['proyek']->sumber_dana : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['nilai_pagu_paket'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->nilai_pagu_paket ? $model['proyek']->nilai_pagu_paket : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['nilai_hps_paket'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->nilai_hps_paket ? $model['proyek']->nilai_hps_paket : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['nilai_kontrak'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->nilai_kontrak ? $model['proyek']->nilai_kontrak : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['anggaran'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->anggaran ? $model['proyek']->anggaran : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['bobot_teknis'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->bobot_teknis ? $model['proyek']->bobot_teknis : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['bobot_biaya'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->bobot_biaya ? $model['proyek']->bobot_biaya : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['dokumen_pakta_integritas'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->dokumen_pakta_integritas ? $model['proyek']->dokumen_pakta_integritas : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['dokumen_surat_minat'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->dokumen_surat_minat ? $model['proyek']->dokumen_surat_minat : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['dokumen_surat_pernyataan'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->dokumen_surat_pernyataan ? $model['proyek']->dokumen_surat_pernyataan : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['sharing'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->sharing ? $model['proyek']->sharing : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['tanggal_mulai'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->tanggal_mulai ? $model['proyek']->tanggal_mulai : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['tanggal_selesai'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->tanggal_selesai ? $model['proyek']->tanggal_selesai : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['uraian_pekerjaan'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->uraian_pekerjaan ? $model['proyek']->uraian_pekerjaan : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['lingkup_pekerjaan'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->lingkup_pekerjaan ? $model['proyek']->lingkup_pekerjaan : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['nomor_bast'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->nomor_bast ? $model['proyek']->nomor_bast : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['tanggal_bast'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->tanggal_bast ? $model['proyek']->tanggal_bast : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['proyek']->attributeLabels()['nomor_spmk'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['proyek']->nomor_spmk ? $model['proyek']->nomor_spmk : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<?php if (!Yii::$app->request->isAjax) : ?>
        <hr class="margin-y-15">
        <div class="form-group clearfix">
            <?= Html::a('Update', ['update', 'id' => $model['proyek']->id], ['class' => 'btn btn-sm btn-default bg-azure rounded-xs border-azure']) ?>&nbsp;
            <?= Html::a('Delete', ['delete', 'id' => $model['proyek']->id], [
                'class' => 'btn btn-sm btn-default bg-lighter rounded-xs',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item ?',
                    'method' => 'post',
                ],
            ]) ?>
            <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-sm btn-default bg-lighter rounded-xs pull-right']) ?>
        </div>
    </div>
</div>
<?php endif; ?>