<?php
namespace app_virama_karya\controllers;

use Yii;
use app_virama_karya\models\Proyek;
use app_virama_karya\models\ProyekTenagaAhli;
use technosmart\yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

/**
 * ProyekController implements highly advanced CRUD actions for Proyek model.
 */
class ProyekController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => $this->access([
                [['logout'], true, ['@'], ['POST']],
            ]),
        ];
    }
    
    protected function findModel($id)
    {
        if (($model = Proyek::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelProyekTenagaAhli($id)
    {
        if (($model = ProyekTenagaAhli::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionIndex($id)
    {
        // view single data
        $model['proyek'] = $this->findModel($id);
        return $this->render('one', [
            'model' => $model,
            'title' => 'Detail of Proyek ' . $model['proyek']->nama_proyek,
        ]);
    }

    public function actionCreateCalonLelang()
    {
        $render = false;

        $model['proyek'] = isset($id) ? $this->findModel($id) : new Proyek();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['proyek']->load($post);

            $transaction['proyek'] = Proyek::getDb()->beginTransaction();

            try {
                $model['proyek']->status = 'Calon';
                if (!$model['proyek']->save()) {
                    throw new \yii\base\UserException('Data cannot be saved. Please try again.');
                }
                
                $transaction['proyek']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['proyek']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['proyek']->rollBack();
            }
        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form-create-calon-lelang', [
                'model' => $model,
                'title' => 'Input Data Calon Lelang',
            ]);
        else
            return $this->redirect(['list-calon-lelang']);
    }

    public function actionDatatablesListCalonLelang()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'id',
                'kode_lelang',
                'nama_proyek',
                'nomor_kontrak',
                'tanggal_kontrak',
                'periode_awal',
                'periode_akhir',
                'status',
                'status_keterangan',
                'keterangan',
                'id_instansi',
                'id_satuan_kerja',
                'id_sub_bidang',
                'lokasi',
                'metode_pengadaan',
                'metode_kualifikasi',
                'metode_dokumen',
                'metode_evaluasi',
                'cara_pembayaran',
                'pembebanan_tahun_anggaran',
                'sumber_dana',
                'nilai_pagu_paket',
                'nilai_hps_paket',
                'nilai_kontrak',
                'anggaran',
                'bobot_teknis',
                'bobot_biaya',
                'dokumen_pakta_integritas',
                'dokumen_surat_minat',
                'dokumen_surat_pernyataan',
                'sharing',
                'tanggal_mulai',
                'tanggal_selesai',
                'uraian_pekerjaan',
                'lingkup_pekerjaan',
                'nomor_bast',
                'tanggal_bast',
                'nomor_spmk',
            ])
            ->from('proyek')
            ->where('status = "Calon"')
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Proyek::getDb());
    }

    public function actionListCalonLelang()
    {
        return $this->render('list-calon-lelang', [
            'title' => 'Daftar Calon Lelang',
        ]);
    }

    public function actionUpdateShortlist($id)
    {
        $model['proyek'] = $this->findModel($id);

        if (Yii::$app->request->isPost) {
            $transaction['proyek'] = Proyek::getDb()->beginTransaction();

            try {
                $model['proyek']->status = 'Shortlist';
                if (!$model['proyek']->save()) {
                    throw new \yii\base\UserException('Data cannot be saved. Please try again.');
                }
                
                $transaction['proyek']->commit();
                Yii::$app->session->setFlash('success', 'Lelang telah di shortlist.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['proyek']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['proyek']->rollBack();
            }
        }

        return $this->redirect(['list-lelang']);
    }

    public function actionUpdateTidakShortlist($id)
    {
        $model['proyek'] = $this->findModel($id);

        if (Yii::$app->request->isPost) {
            $transaction['proyek'] = Proyek::getDb()->beginTransaction();

            try {
                $model['proyek']->status = 'Tidak Shortlist';
                if (!$model['proyek']->save()) {
                    throw new \yii\base\UserException('Data cannot be saved. Please try again.');
                }
                
                $transaction['proyek']->commit();
                Yii::$app->session->setFlash('success', 'Lelang tidak di shortlist.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['proyek']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['proyek']->rollBack();
            }
        }

        return $this->redirect(['list-tidak-shortlist']);
    }

    public function actionUpdateJadwal($id)
    {
        $render = false;

        $model['proyek'] = $this->findModel($id);
        $model['proyek']->scenario = 'score';

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['proyek']->load($post);

            $transaction['proyek'] = Proyek::getDb()->beginTransaction();

            try {
                if (!$model['proyek']->save()) {
                    throw new \yii\base\UserException('Data cannot be saved. Please try again.');
                }
                
                $transaction['proyek']->commit();
                Yii::$app->session->setFlash('success', 'Jadwal lelang berhasil diinput.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['proyek']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['proyek']->rollBack();
            }
        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form-update-jadwal', [
                'model' => $model,
                'title' => 'Input Jadwal Lelang',
            ]);
        else
            return $this->redirect(['list-kontrak-berlangsung']);
    }

    public function actionDatatablesListTidakShortlist()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'id',
                'kode_lelang',
                'nama_proyek',
                'nomor_kontrak',
                'tanggal_kontrak',
                'periode_awal',
                'periode_akhir',
                'status',
                'status_keterangan',
                'keterangan',
                'id_instansi',
                'id_satuan_kerja',
                'id_sub_bidang',
                'lokasi',
                'metode_pengadaan',
                'metode_kualifikasi',
                'metode_dokumen',
                'metode_evaluasi',
                'cara_pembayaran',
                'pembebanan_tahun_anggaran',
                'sumber_dana',
                'nilai_pagu_paket',
                'nilai_hps_paket',
                'nilai_kontrak',
                'anggaran',
                'bobot_teknis',
                'bobot_biaya',
                'dokumen_pakta_integritas',
                'dokumen_surat_minat',
                'dokumen_surat_pernyataan',
                'sharing',
                'tanggal_mulai',
                'tanggal_selesai',
                'uraian_pekerjaan',
                'lingkup_pekerjaan',
                'nomor_bast',
                'tanggal_bast',
                'nomor_spmk',
            ])
            ->from('proyek')
            ->where('status = "Tidak Shortlist"')
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Proyek::getDb());
    }

    public function actionListTidakShortlist()
    {
        return $this->render('list-tidak-shortlist', [
            'title' => 'Daftar Lelang tidak Shortlist',
        ]);
    }

    //

    public function actionDatatablesListLelang()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'id',
                'kode_lelang',
                'nama_proyek',
                'nomor_kontrak',
                'tanggal_kontrak',
                'periode_awal',
                'periode_akhir',
                'status',
                'status_keterangan',
                'keterangan',
                'id_instansi',
                'id_satuan_kerja',
                'id_sub_bidang',
                'lokasi',
                'metode_pengadaan',
                'metode_kualifikasi',
                'metode_dokumen',
                'metode_evaluasi',
                'cara_pembayaran',
                'pembebanan_tahun_anggaran',
                'sumber_dana',
                'nilai_pagu_paket',
                'nilai_hps_paket',
                'nilai_kontrak',
                'anggaran',
                'bobot_teknis',
                'bobot_biaya',
                'dokumen_pakta_integritas',
                'dokumen_surat_minat',
                'dokumen_surat_pernyataan',
                'sharing',
                'tanggal_mulai',
                'tanggal_selesai',
                'uraian_pekerjaan',
                'lingkup_pekerjaan',
                'nomor_bast',
                'tanggal_bast',
                'nomor_spmk',
            ])
            ->from('proyek')
            ->where('status = "Shortlist"')
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Proyek::getDb());
    }

    public function actionListLelang()
    {
        return $this->render('list-lelang', [
            'title' => 'Daftar Lelang',
        ]);
    }

    public function actionDatatablesListLelangKalah()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'id',
                'kode_lelang',
                'nama_proyek',
                'nomor_kontrak',
                'tanggal_kontrak',
                'periode_awal',
                'periode_akhir',
                'status',
                'status_keterangan',
                'keterangan',
                'id_instansi',
                'id_satuan_kerja',
                'id_sub_bidang',
                'lokasi',
                'metode_pengadaan',
                'metode_kualifikasi',
                'metode_dokumen',
                'metode_evaluasi',
                'cara_pembayaran',
                'pembebanan_tahun_anggaran',
                'sumber_dana',
                'nilai_pagu_paket',
                'nilai_hps_paket',
                'nilai_kontrak',
                'anggaran',
                'bobot_teknis',
                'bobot_biaya',
                'dokumen_pakta_integritas',
                'dokumen_surat_minat',
                'dokumen_surat_pernyataan',
                'sharing',
                'tanggal_mulai',
                'tanggal_selesai',
                'uraian_pekerjaan',
                'lingkup_pekerjaan',
                'nomor_bast',
                'tanggal_bast',
                'nomor_spmk',
            ])
            ->from('proyek')
            ->where('status = "Kalah"')
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Proyek::getDb());
    }

    public function actionListLelangKalah()
    {
        return $this->render('list-lelang-kalah', [
            'title' => 'Daftar Lelang yang Kalah',
        ]);
    }

    public function actionUpdateMenang($id)
    {
        $render = false;

        $model['proyek'] = $this->findModel($id);
        $model['proyek']->scenario = 'score';

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['proyek']->load($post);

            $transaction['proyek'] = Proyek::getDb()->beginTransaction();

            try {
                $model['proyek']->status = 'Kontrak';
                if (!$model['proyek']->save()) {
                    throw new \yii\base\UserException('Data cannot be saved. Please try again.');
                }
                
                $transaction['proyek']->commit();
                Yii::$app->session->setFlash('success', 'Status lelang telah diubah menjadi menang.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['proyek']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['proyek']->rollBack();
            }
        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form-update-menang', [
                'model' => $model,
                'title' => 'Lelang ini Menang',
            ]);
        else
            return $this->redirect(['list-kontrak-berlangsung']);
    }

    public function actionUpdateKalah($id)
    {
        $render = false;

        $model['proyek'] = $this->findModel($id);
        $model['proyek']->scenario = 'score';

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['proyek']->load($post);

            $transaction['proyek'] = Proyek::getDb()->beginTransaction();

            try {
                $model['proyek']->status = 'Kalah';
                if (!$model['proyek']->save()) {
                    throw new \yii\base\UserException('Data cannot be saved. Please try again.');
                }
                
                $transaction['proyek']->commit();
                Yii::$app->session->setFlash('success', 'Status lelang telah diubah menjadi kalah.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['proyek']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['proyek']->rollBack();
            }
        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form-update-kalah', [
                'model' => $model,
                'title' => 'Lelang ini Kalah',
            ]);
        else
            return $this->redirect(['list-lelang-kalah']);
    }

    public function actionUpdateRab($id)
    {
        $render = false;

        $model['proyek'] = isset($id) ? $this->findModel($id) : new Proyek();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['proyek']->load($post);

            $transaction['proyek'] = Proyek::getDb()->beginTransaction();

            try {
                if (!$model['proyek']->save()) {
                    throw new \yii\base\UserException('Data cannot be saved. Please try again.');
                }
                
                $transaction['proyek']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['proyek']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['proyek']->rollBack();
            }
        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form-update-rab', [
                'model' => $model,
                'title' => 'Input Data RAB',
            ]);
        else
            return $this->redirect(['list-lelang']);
    }

    public function actionUpdateEvaluasi($id)
    {
        $render = false;

        $model['proyek'] = isset($id) ? $this->findModel($id) : new Proyek();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['proyek']->load($post);

            $transaction['proyek'] = Proyek::getDb()->beginTransaction();

            try {
                if (!$model['proyek']->save()) {
                    throw new \yii\base\UserException('Data cannot be saved. Please try again.');
                }
                
                $transaction['proyek']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['proyek']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['proyek']->rollBack();
            }
        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form-update-evaluasi', [
                'model' => $model,
                'title' => 'Input Data Evaluasi',
            ]);
        else
            return $this->redirect(['list-lelang']);
    }

    public function actionUpdatePaktaIntegritas($id)
    {
        $render = false;

        $model['proyek'] = isset($id) ? $this->findModel($id) : new Proyek();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['proyek']->load($post);

            $transaction['proyek'] = Proyek::getDb()->beginTransaction();

            try {
                if (!$model['proyek']->save()) {
                    throw new \yii\base\UserException('Data cannot be saved. Please try again.');
                }
                
                $transaction['proyek']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['proyek']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['proyek']->rollBack();
            }
        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form-update-pakta-integritas', [
                'model' => $model,
                'title' => 'Input Dokumen Pakta Integritas',
            ]);
        else
            return $this->redirect(['list-lelang']);
    }

    public function actionUpdateSuratMinat($id)
    {
        $render = false;

        $model['proyek'] = isset($id) ? $this->findModel($id) : new Proyek();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['proyek']->load($post);

            $transaction['proyek'] = Proyek::getDb()->beginTransaction();

            try {
                if (!$model['proyek']->save()) {
                    throw new \yii\base\UserException('Data cannot be saved. Please try again.');
                }
                
                $transaction['proyek']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['proyek']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['proyek']->rollBack();
            }
        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form-update-surat-minat', [
                'model' => $model,
                'title' => 'Input Dokumen Surat Minat',
            ]);
        else
            return $this->redirect(['list-lelang']);
    }

    public function actionUpdateSuratPernyataan($id)
    {
        $render = false;

        $model['proyek'] = isset($id) ? $this->findModel($id) : new Proyek();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['proyek']->load($post);

            $transaction['proyek'] = Proyek::getDb()->beginTransaction();

            try {
                if (!$model['proyek']->save()) {
                    throw new \yii\base\UserException('Data cannot be saved. Please try again.');
                }
                
                $transaction['proyek']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['proyek']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['proyek']->rollBack();
            }
        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form-update-surat-pernyataan', [
                'model' => $model,
                'title' => 'Input Dokumen Surat Pernyataan',
            ]);
        else
            return $this->redirect(['list-lelang']);
    }

    //

    public function actionDatatablesListKontrakBerlangsung()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'id',
                'kode_lelang',
                'nama_proyek',
                'nomor_kontrak',
                'tanggal_kontrak',
                'periode_awal',
                'periode_akhir',
                'status',
                'status_keterangan',
                'keterangan',
                'id_instansi',
                'id_satuan_kerja',
                'id_sub_bidang',
                'lokasi',
                'metode_pengadaan',
                'metode_kualifikasi',
                'metode_dokumen',
                'metode_evaluasi',
                'cara_pembayaran',
                'pembebanan_tahun_anggaran',
                'sumber_dana',
                'nilai_pagu_paket',
                'nilai_hps_paket',
                'nilai_kontrak',
                'anggaran',
                'bobot_teknis',
                'bobot_biaya',
                'dokumen_pakta_integritas',
                'dokumen_surat_minat',
                'dokumen_surat_pernyataan',
                'sharing',
                'tanggal_mulai',
                'tanggal_selesai',
                'uraian_pekerjaan',
                'lingkup_pekerjaan',
                'nomor_bast',
                'tanggal_bast',
                'nomor_spmk',
            ])
            ->from('proyek')
            ->where('status = "Kontrak"')
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Proyek::getDb());
    }

    public function actionListKontrakBerlangsung()
    {
        return $this->render('list-kontrak-berlangsung', [
            'title' => 'Daftar Kontrak yang sedang Berlangsung',
        ]);
    }

    public function actionUpdateKontrak($id)
    {
        $render = false;

        $model['proyek'] = isset($id) ? $this->findModel($id) : new Proyek();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['proyek']->load($post);

            $transaction['proyek'] = Proyek::getDb()->beginTransaction();

            try {
                if (!$model['proyek']->save()) {
                    throw new \yii\base\UserException('Data cannot be saved. Please try again.');
                }
                
                $transaction['proyek']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['proyek']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['proyek']->rollBack();
            }
        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form-update-kontrak', [
                'model' => $model,
                'title' => 'Input Data Kontrak',
            ]);
        else
            return $this->redirect(['list-kontrak-berlangsung']);
    }

    public function actionUpdateTenagaAhli($id)
    {
        $render = false;

        $model['proyek'] = isset($id) ? $this->findModel($id) : new Proyek();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['proyek']->load($post);
            if (isset($post['ProyekTenagaAhli'])) {
                foreach ($post['ProyekTenagaAhli'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $proyekTenagaAhli = $this->findModelProyekTenagaAhli($value['id']);
                        $proyekTenagaAhli->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $proyekTenagaAhli = $this->findModelProyekTenagaAhli(($value['id']*-1));
                        $proyekTenagaAhli->isDeleted = true;
                    } else {
                        $proyekTenagaAhli = new ProyekTenagaAhli();
                        $proyekTenagaAhli->setAttributes($value);
                    }
                    $model['proyek_tenaga_ahli'][] = $proyekTenagaAhli;
                }
            }

            $transaction['proyek'] = Proyek::getDb()->beginTransaction();

            try {
                if (!$model['proyek']->save()) {
                    throw new \yii\base\UserException('Data cannot be saved. Please try again.');
                }

                $error = false;
                if (isset($model['proyek_tenaga_ahli']) and is_array($model['proyek_tenaga_ahli'])) {
                    foreach ($model['proyek_tenaga_ahli'] as $key => $proyekTenagaAhli) {
                        if ($proyekTenagaAhli->isDeleted) {
                            if (!$proyekTenagaAhli->delete()) {
                                $error = true;
                            }
                        } else {
                            $proyekTenagaAhli->id_proyek = $model['proyek']->id;
                            if (!$proyekTenagaAhli->save()) {
                                $error = true;
                            }
                        }
                    }
                }

                if ($error) {
                    throw new \yii\base\UserException('Data cannot be saved. Please try again.');
                }
                
                $transaction['proyek']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['proyek']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['proyek']->rollBack();
            }
        } else {
            foreach ($model['proyek']->proyekTenagaAhlis as $key => $proyekTenagaAhli)
                $model['proyek_tenaga_ahli'][] = $proyekTenagaAhli;

            $render = true;
        }

        if ($render)
            return $this->render('form-update-tenaga-ahli', [
                'model' => $model,
                'title' => 'Input Data Tenaga Ahli',
            ]);
        else
            return $this->redirect(['list-kontrak-berlangsung']);
    }

    public function actionUpdateMitraKerja($id)
    {
        $render = false;

        $model['proyek'] = isset($id) ? $this->findModel($id) : new Proyek();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['proyek']->load($post);

            $transaction['proyek'] = Proyek::getDb()->beginTransaction();

            try {
                if (!$model['proyek']->save()) {
                    throw new \yii\base\UserException('Data cannot be saved. Please try again.');
                }
                
                $transaction['proyek']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['proyek']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['proyek']->rollBack();
            }
        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form-update-mitra-kerja', [
                'model' => $model,
                'title' => 'Input Data Mitra Kerja',
            ]);
        else
            return $this->redirect(['list-kontrak-berlangsung']);
    }

    public function actionUpdatePds($id)
    {
        $render = false;

        $model['proyek'] = isset($id) ? $this->findModel($id) : new Proyek();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['proyek']->load($post);

            $transaction['proyek'] = Proyek::getDb()->beginTransaction();

            try {
                if (!$model['proyek']->save()) {
                    throw new \yii\base\UserException('Data cannot be saved. Please try again.');
                }
                
                $transaction['proyek']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['proyek']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['proyek']->rollBack();
            }
        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form-update-pds', [
                'model' => $model,
                'title' => 'Input Data PDS',
            ]);
        else
            return $this->redirect(['list-kontrak-berlangsung']);
    }

    public function actionDatatablesListKontrakSelesai()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'id',
                'kode_lelang',
                'nama_proyek',
                'nomor_kontrak',
                'tanggal_kontrak',
                'periode_awal',
                'periode_akhir',
                'status',
                'status_keterangan',
                'keterangan',
                'id_instansi',
                'id_satuan_kerja',
                'id_sub_bidang',
                'lokasi',
                'metode_pengadaan',
                'metode_kualifikasi',
                'metode_dokumen',
                'metode_evaluasi',
                'cara_pembayaran',
                'pembebanan_tahun_anggaran',
                'sumber_dana',
                'nilai_pagu_paket',
                'nilai_hps_paket',
                'nilai_kontrak',
                'anggaran',
                'bobot_teknis',
                'bobot_biaya',
                'dokumen_pakta_integritas',
                'dokumen_surat_minat',
                'dokumen_surat_pernyataan',
                'sharing',
                'tanggal_mulai',
                'tanggal_selesai',
                'uraian_pekerjaan',
                'lingkup_pekerjaan',
                'nomor_bast',
                'tanggal_bast',
                'nomor_spmk',
            ])
            ->from('proyek')
            ->where('status = "Selesai"')
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Proyek::getDb());
    }

    public function actionListKontrakSelesai()
    {
        return $this->render('list-kontrak-selesai', [
            'title' => 'Daftar Kontrak yang telah Selesai',
        ]);
    }

    //

    public function actionDatatablesListLaporan()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'id',
                'kode_lelang',
                'nama_proyek',
                'nomor_kontrak',
                'tanggal_kontrak',
                'periode_awal',
                'periode_akhir',
                'status',
                'status_keterangan',
                'keterangan',
                'id_instansi',
                'id_satuan_kerja',
                'id_sub_bidang',
                'lokasi',
                'metode_pengadaan',
                'metode_kualifikasi',
                'metode_dokumen',
                'metode_evaluasi',
                'cara_pembayaran',
                'pembebanan_tahun_anggaran',
                'sumber_dana',
                'nilai_pagu_paket',
                'nilai_hps_paket',
                'nilai_kontrak',
                'anggaran',
                'bobot_teknis',
                'bobot_biaya',
                'dokumen_pakta_integritas',
                'dokumen_surat_minat',
                'dokumen_surat_pernyataan',
                'sharing',
                'tanggal_mulai',
                'tanggal_selesai',
                'uraian_pekerjaan',
                'lingkup_pekerjaan',
                'nomor_bast',
                'tanggal_bast',
                'nomor_spmk',
            ])
            ->from('proyek')
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Proyek::getDb());
    }

    public function actionListLaporan()
    {
        return $this->render('list-laporan', [
            'title' => 'Laporan Proyek/Lelang',
        ]);
    }

    //

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
}
