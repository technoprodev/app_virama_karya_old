<?php
namespace app_virama_karya\controllers;

use Yii;
use app_virama_karya\models\IdentitasPerusahaan;
use technosmart\yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

/**
 * IdentitasPerusahaanController implements highly advanced CRUD actions for IdentitasPerusahaan model.
 */
class IdentitasPerusahaanController extends Controller
{
    public static $permissions = [
        ['view', 'View Identitas perusahaan'], ['create', 'Create Identitas perusahaan'], ['update', 'Update Identitas perusahaan'], ['delete', 'Delete Identitas perusahaan'],
    ];

    public function behaviors()
    {
        return [
            'access' => $this->access([
                [['index'], 'view', ['@']],
                [['index', 'create'], 'create', ['@']],
                [['index', 'update'], 'update', ['@']],
                [['index', 'delete'], 'delete', ['@'], ['POST']],
            ]),
        ];
    }

    /**
     * Finds the IdentitasPerusahaan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return IdentitasPerusahaan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = IdentitasPerusahaan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDatatables()
    {
        $db = IdentitasPerusahaan::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('identitas_perusahaan');
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                'id',
                'nama_perusahaan',
                'status',
                'alamat',
                'nomor_telpon',
                'nomor_fax',
                'email_1',
                'email_2',
                'akta_pendirian_perusahaan_nomor_akta',
                'akta_pendirian_perusahaan_tanggal',
                'akta_pendirian_perusahaan_nama_notaris',
                'akta_pendirian_perusahaan_nomor_pengesahan',
                'akta_pendirian_perusahaan_tanggal_pengesahan',
                'akta_perubahan_terakhir_nomor_akta',
                'akta_perubahan_terakhir_nama_notaris',
                'akta_perubahan_terakhir_tanggal',
                'akta_perubahan_terakhir_nomor_pengesahan',
                'akta_perubahan_terakhir_tanggal_pengesahan',
                'iujk_masa_berlaku_dari',
                'iujk_masa_berlaku_sampai',
                'iujk_instansi_pemberi',
                'iujk_perencanaan',
                'iujk_pengawasan',
                'iujk_konsultansi',
                'sbu_konstruksi_masa_berlaku_dari',
                'sbu_konstruksi_masa_berlaku_sampai',
                'sbu_konstruksi_instansi_pemberi_izin',
                'sbu_konstruksi_perencanaan_arsitektur',
                'sbu_konstruksi_pengawasan_arsitektur',
                'sbu_konstruksi_jasa_konsultansi_lainnya',
                'sbu_konstruksi_perencana_penataan_ruang',
                'sbu_konstruksi_pengawasan_penataan_ruang',
                'sbu_konstruksi_perencanaan_rekayasa',
                'sbu_konstruksi_pengawasan_rekayasa',
                'sbu_konstruksi_konsultan_spesialis',
                'sbu_non_konstruksi_ljspdbt',
                'sbu_non_konstruksi_ljskm',
                'sbu_non_konstruksi_ljskm_masa_berlaku_dari',
                'sbu_non_konstruksi_ljskm_masa_berlaku_sampai',
                'sbu_non_konstruksi_ljskm_instansi_pemberi_izin',
                'sbu_non_konstruksi_pengembangan_pertanian_dan_pedesaan',
                'sbu_non_konstruksi_transportasi',
                'sbu_non_konstruksi_telematika',
                'sbu_non_konstruksi_pendidikan',
                'sbu_non_konstruksi_pendidikan_masa_berlaku_dari',
                'sbu_non_konstruksi_pendidikan_masa_berlaku_sampai',
                'sbu_non_konstruksi_pendidikan_instansi_pemberi_izin',
                'pajak_nomor_pokok_wajib_pajak',
                'pajak_surat_pengukuhan_kena_pajak',
                'pajak_surat_keterangan_terdaftar',
                'pajak_bpptt_nomor',
                'pajak_bpptt_tanggal',
                'pajak_skf_nomor',
                'pajak_skf_tanggal',
            ]);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }

    /**
     * If param(s) is null, display all datas from models.
     * If all param(s) is not null, display a data from model.
     * @param integer $id
     * @return mixed
     */
    public function actionIndex(/*$id = null*/)
    {
        // view single data
        $model['identitas_perusahaan'] = $this->findModel(1);
        return $this->render('one', [
            'model' => $model,
            'title' => 'Identitas perusahaan',
        ]);

        // view all data
        if (!$id) {
            return $this->render('list', [
                'title' => 'List of Identitas perusahaans',
            ]);
        }
    }

    /**
     * Creates new data(s) from model(s).
     * If submission is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function ctionCreate()
    {
        $render = false;

        $model['identitas_perusahaan'] = isset($id) ? $this->findModel($id) : new IdentitasPerusahaan();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['identitas_perusahaan']->load($post);

            $transaction['identitas_perusahaan'] = IdentitasPerusahaan::getDb()->beginTransaction();

            try {
                if (!$model['identitas_perusahaan']->save()) {
                    throw new \yii\base\UserException('Data cannot be saved. Please try again.');
                }
                
                $transaction['identitas_perusahaan']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['identitas_perusahaan']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['identitas_perusahaan']->rollBack();
            }
        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Add New Identitas perusahaan',
            ]);
        else
            return $this->redirect(['index', 'id' => $model['identitas_perusahaan']->id]);
    }

    /**
     * Updates existing data(s) from model(s).
     * If submission is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function ctionUpdate($id = null)
    {
        $render = false;

        $model['identitas_perusahaan'] = isset($id) ? $this->findModel($id) : new IdentitasPerusahaan();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['identitas_perusahaan']->load($post);

            $transaction['identitas_perusahaan'] = IdentitasPerusahaan::getDb()->beginTransaction();

            try {
                if (!$model['identitas_perusahaan']->save()) {
                    throw new \yii\base\UserException('Data cannot be saved. Please try again.');
                }
                
                $transaction['identitas_perusahaan']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['identitas_perusahaan']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['identitas_perusahaan']->rollBack();
            }
        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Update Identitas perusahaan ' . $model['identitas_perusahaan']->id,
            ]);
        else
            return $this->redirect(['index', 'id' => $model['identitas_perusahaan']->id]);
    }

    /**
     * Deletes an existing IdentitasPerusahaan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function ctionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    //

    protected function update()
    {
        $render = false;

        $model['identitas_perusahaan'] = $this->findModel(1);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['identitas_perusahaan']->load($post);

            $transaction['identitas_perusahaan'] = IdentitasPerusahaan::getDb()->beginTransaction();

            try {
                if (!$model['identitas_perusahaan']->save()) {
                    throw new \yii\base\UserException('Data cannot be saved. Please try again.');
                }
                
                $transaction['identitas_perusahaan']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['identitas_perusahaan']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['identitas_perusahaan']->rollBack();
            }
        } else {
            $render = true;
        }

        return [$render, $model];
    }

    public function actionUpdateDataAdministrasi()
    {
        $render = $this->update()[0];
        $model = $this->update()[1];

        if ($render)
            return $this->render('form-update-data-administrasi', [
                'model' => $model,
                'title' => 'Update Data Administrasi ',
            ]);
        else
            return $this->redirect(['index']);
    }
    

    public function actionUpdateLhpbu()
    {
        $render = $this->update()[0];
        $model = $this->update()[1];

        if ($render)
            return $this->render('form-update-lhpbu', [
                'model' => $model,
                'title' => 'Update Landasan Hukum Pendirian Badan Usaha ',
            ]);
        else
            return $this->redirect(['index']);
    }
    

    public function actionUpdatePengurusBadanUsaha()
    {
        $render = $this->update()[0];
        $model = $this->update()[1];

        if ($render)
            return $this->render('form-update-pengurus-badan-usaha', [
                'model' => $model,
                'title' => 'Update Pengurus Badan Usaha ',
            ]);
        else
            return $this->redirect(['index']);
    }
    

    public function actionUpdateIjinUsaha()
    {
        $render = $this->update()[0];
        $model = $this->update()[1];

        if ($render)
            return $this->render('form-update-ijin-usaha', [
                'model' => $model,
                'title' => 'Update Ijin Usaha ',
            ]);
        else
            return $this->redirect(['index']);
    }
    

    public function actionUpdateIjinUsahaLainnya()
    {
        $render = $this->update()[0];
        $model = $this->update()[1];

        if ($render)
            return $this->render('form-update-ijin-usaha-lainnya', [
                'model' => $model,
                'title' => 'Update Ijin Usaha Lainnya ',
            ]);
        else
            return $this->redirect(['index']);
    }
    

    public function actionUpdateSks()
    {
        $render = $this->update()[0];
        $model = $this->update()[1];

        if ($render)
            return $this->render('form-update-sks', [
                'model' => $model,
                'title' => 'Update Susunan Kepemilikan Saham ' . $model['identitas_perusahaan']->nama_perusahaan,
            ]);
        else
            return $this->redirect(['index']);
    }
    

    public function actionUpdatePajak()
    {
        $render = $this->update()[0];
        $model = $this->update()[1];

        if ($render)
            return $this->render('form-update-pajak', [
                'model' => $model,
                'title' => 'Update Pajak ',
            ]);
        else
            return $this->redirect(['index']);
    }
}
