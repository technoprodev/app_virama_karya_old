<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "sub_bidang".
 *
 * @property integer $id
 * @property string $nama_sub_bidang
 * @property integer $id_bidang
 *
 * @property Bidang $bidang
 */
class SubBidang extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'sub_bidang';
    }

    public function rules()
    {
        return [
            //id

            //nama_sub_bidang
            [['nama_sub_bidang'], 'required'],
            [['nama_sub_bidang'], 'string', 'max' => 64],

            //id_bidang
            [['id_bidang'], 'required'],
            [['id_bidang'], 'integer'],
            [['id_bidang'], 'exist', 'skipOnError' => true, 'targetClass' => Bidang::className(), 'targetAttribute' => ['id_bidang' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_sub_bidang' => 'Nama Sub Bidang',
            'id_bidang' => 'Id Bidang',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBidang()
    {
        return $this->hasOne(Bidang::className(), ['id' => 'id_bidang']);
    }
}
