<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "tenaga_ahli".
 *
 * @property integer $id
 * @property string $nama
 * @property string $tanggal_lahir
 * @property string $jabatan_yang_diusulkan
 * @property integer $pengalaman_kerja
 *
 * @property TenagaAhliKeahlian[] $tenagaAhliKeahlians
 * @property TenagaAhliPendidikan[] $tenagaAhliPendidikans
 */
class TenagaAhli extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'tenaga_ahli';
    }

    public function rules()
    {
        return [
            //id

            //nama
            [['nama'], 'required'],
            [['nama'], 'string', 'max' => 128],

            //tanggal_lahir
            [['tanggal_lahir'], 'required'],
            [['tanggal_lahir'], 'safe'],

            //jabatan_yang_diusulkan
            [['jabatan_yang_diusulkan'], 'required'],
            [['jabatan_yang_diusulkan'], 'string', 'max' => 255],

            //pengalaman_kerja
            [['pengalaman_kerja'], 'required'],
            [['pengalaman_kerja'], 'integer'],
        ];
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }
        
        \Yii::$app->dba->createCommand()->delete('tenaga_ahli_pendidikan', 'id_tenaga_ahli = ' . (int) $this->id)->execute();
        \Yii::$app->dba->createCommand()->delete('tenaga_ahli_keahlian', 'id_tenaga_ahli = ' . (int) $this->id)->execute();

        return true;
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'tanggal_lahir' => 'Tanggal Lahir',
            'jabatan_yang_diusulkan' => 'Jabatan Yang Diusulkan',
            'pengalaman_kerja' => 'Pengalaman Kerja',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenagaAhliKeahlians()
    {
        return $this->hasMany(TenagaAhliKeahlian::className(), ['id_tenaga_ahli' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenagaAhliPendidikans()
    {
        return $this->hasMany(TenagaAhliPendidikan::className(), ['id_tenaga_ahli' => 'id']);
    }
}
