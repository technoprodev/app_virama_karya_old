<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "aset".
 *
 * @property integer $id
 * @property string $nama_aset
 * @property integer $jumlah
 * @property string $keterangan
 * @property string $merk
 * @property string $tahun_pembuatan
 * @property string $kondisi
 * @property string $lokasi_sekarang
 * @property string $bukti_kepemilikan
 */
class Aset extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'aset';
    }

    public function rules()
    {
        return [
            //id

            //nama_aset
            [['nama_aset'], 'required'],
            [['nama_aset'], 'string', 'max' => 64],

            //jumlah
            [['jumlah'], 'required'],
            [['jumlah'], 'integer'],

            //keterangan
            [['keterangan'], 'string', 'max' => 64],

            //merk
            [['merk'], 'string', 'max' => 64],

            //tahun_pembuatan
            [['tahun_pembuatan'], 'string', 'max' => 64],

            //kondisi
            [['kondisi'], 'string', 'max' => 16],

            //lokasi_sekarang
            [['lokasi_sekarang'], 'string', 'max' => 128],

            //bukti_kepemilikan
            [['bukti_kepemilikan'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_aset' => 'Nama Aset',
            'jumlah' => 'Jumlah',
            'keterangan' => 'Keterangan',
            'merk' => 'Merk',
            'tahun_pembuatan' => 'Tahun Pembuatan',
            'kondisi' => 'Kondisi',
            'lokasi_sekarang' => 'Lokasi Sekarang',
            'bukti_kepemilikan' => 'Bukti Kepemilikan',
        ];
    }
}
