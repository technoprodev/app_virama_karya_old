<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "tenaga_ahli_pendidikan".
 *
 * @property integer $id
 * @property integer $id_tenaga_ahli
 * @property string $jenjang
 * @property string $jurusan
 * @property string $tahun
 * @property string $nomor_ijazah
 *
 * @property TenagaAhli $tenagaAhli
 */
class TenagaAhliPendidikan extends \technosmart\yii\db\ActiveRecord
{
    public $isDeleted;
    
    public static function tableName()
    {
        return 'tenaga_ahli_pendidikan';
    }

    public function rules()
    {
        return [
            //id

            //id_tenaga_ahli
            [['id_tenaga_ahli'], 'required'],
            [['id_tenaga_ahli'], 'integer'],
            [['id_tenaga_ahli'], 'exist', 'skipOnError' => true, 'targetClass' => TenagaAhli::className(), 'targetAttribute' => ['id_tenaga_ahli' => 'id']],

            //jenjang
            [['jenjang'], 'required'],
            [['jenjang'], 'string', 'max' => 4],

            //jurusan
            [['jurusan'], 'required'],
            [['jurusan'], 'string', 'max' => 64],

            //tahun
            [['tahun'], 'required'],
            [['tahun'], 'safe'],

            //nomor_ijazah
            [['nomor_ijazah'], 'required'],
            [['nomor_ijazah'], 'string', 'max' => 128],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_tenaga_ahli' => 'Id Tenaga Ahli',
            'jenjang' => 'Jenjang',
            'jurusan' => 'Jurusan',
            'tahun' => 'Tahun',
            'nomor_ijazah' => 'Nomor Ijazah',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenagaAhli()
    {
        return $this->hasOne(TenagaAhli::className(), ['id' => 'id_tenaga_ahli']);
    }
}
