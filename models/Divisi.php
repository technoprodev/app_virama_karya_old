<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "divisi".
 *
 * @property integer $id
 * @property string $nama_divisi
 */
class Divisi extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'divisi';
    }

    public function rules()
    {
        return [
            //id

            //nama_divisi
            [['nama_divisi'], 'required'],
            [['nama_divisi'], 'string', 'max' => 64],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_divisi' => 'Nama Divisi',
        ];
    }
}
