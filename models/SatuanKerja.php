<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "satuan_kerja".
 *
 * @property integer $id
 * @property string $nama_satuan_kerja
 * @property integer $id_instansi
 * @property integer $id_satminkal
 * @property string $alamat
 * @property string $telpon
 *
 * @property Instansi $instansi
 * @property Satminkal $satminkal
 */
class SatuanKerja extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'satuan_kerja';
    }

    public function rules()
    {
        return [
            //id

            //nama_satuan_kerja
            [['nama_satuan_kerja'], 'required'],
            [['nama_satuan_kerja'], 'string', 'max' => 64],

            //id_instansi
            [['id_instansi'], 'required'],
            [['id_instansi'], 'integer'],
            [['id_instansi'], 'exist', 'skipOnError' => true, 'targetClass' => Instansi::className(), 'targetAttribute' => ['id_instansi' => 'id']],

            //id_satminkal
            [['id_satminkal'], 'required'],
            [['id_satminkal'], 'integer'],
            [['id_satminkal'], 'exist', 'skipOnError' => true, 'targetClass' => Satminkal::className(), 'targetAttribute' => ['id_satminkal' => 'id']],

            //alamat
            [['alamat'], 'required'],
            [['alamat'], 'string'],

            //telpon
            [['telpon'], 'required'],
            [['telpon'], 'string', 'max' => 16],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_satuan_kerja' => 'Nama Satuan Kerja',
            'id_instansi' => 'Id Instansi',
            'id_satminkal' => 'Id Satminkal',
            'alamat' => 'Alamat',
            'telpon' => 'Telpon',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstansi()
    {
        return $this->hasOne(Instansi::className(), ['id' => 'id_instansi']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSatminkal()
    {
        return $this->hasOne(Satminkal::className(), ['id' => 'id_satminkal']);
    }
}
