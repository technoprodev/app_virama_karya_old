<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "instansi".
 *
 * @property integer $id
 * @property string $nama_instansi
 * @property string $tahun_anggaran
 *
 * @property Satminkal[] $satminkals
 * @property SatuanKerja[] $satuanKerjas
 */
class Instansi extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'instansi';
    }

    public function rules()
    {
        return [
            //id

            //nama_instansi
            [['nama_instansi'], 'required'],
            [['nama_instansi'], 'string', 'max' => 64],

            //tahun_anggaran
            [['tahun_anggaran'], 'required'],
            [['tahun_anggaran'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_instansi' => 'Nama Instansi',
            'tahun_anggaran' => 'Tahun Anggaran',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSatminkals()
    {
        return $this->hasMany(Satminkal::className(), ['id_instansi' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSatuanKerjas()
    {
        return $this->hasMany(SatuanKerja::className(), ['id_instansi' => 'id']);
    }
}
