<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "cabang".
 *
 * @property integer $id
 * @property string $nama_cabang
 */
class Cabang extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'cabang';
    }

    public function rules()
    {
        return [
            //id

            //nama_cabang
            [['nama_cabang'], 'required'],
            [['nama_cabang'], 'string', 'max' => 64],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_cabang' => 'Nama Cabang',
        ];
    }
}
