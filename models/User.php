<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is connector for Yii::$app->user with model User
 */
class User extends \technosmart\models\User
{

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id']);
    }
}