<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "proyek_tenaga_ahli".
 *
 * @property integer $id
 * @property integer $id_proyek
 * @property integer $id_tenaga_ahli
 * @property integer $id_penugasan
 * @property string $periode_dari
 * @property string $periode_hingga
 *
 * @property Proyek $proyek
 * @property TenagaAhli $tenagaAhli
 * @property Penugasan $penugasan
 */
class ProyekTenagaAhli extends \technosmart\yii\db\ActiveRecord
{
    public $isDeleted;
    
    public static function tableName()
    {
        return 'proyek_tenaga_ahli';
    }

    public function rules()
    {
        return [
            //id

            //id_proyek
            [['id_proyek'], 'required'],
            [['id_proyek'], 'integer'],
            [['id_proyek'], 'exist', 'skipOnError' => true, 'targetClass' => Proyek::className(), 'targetAttribute' => ['id_proyek' => 'id']],

            //id_tenaga_ahli
            [['id_tenaga_ahli'], 'required'],
            [['id_tenaga_ahli'], 'integer'],
            [['id_tenaga_ahli'], 'exist', 'skipOnError' => true, 'targetClass' => TenagaAhli::className(), 'targetAttribute' => ['id_tenaga_ahli' => 'id']],

            //id_penugasan
            [['id_penugasan'], 'required'],
            [['id_penugasan'], 'integer'],
            [['id_penugasan'], 'exist', 'skipOnError' => true, 'targetClass' => Penugasan::className(), 'targetAttribute' => ['id_penugasan' => 'id']],

            //periode_dari
            [['periode_dari'], 'required'],
            [['periode_dari'], 'safe'],

            //periode_hingga
            [['periode_hingga'], 'required'],
            [['periode_hingga'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_proyek' => 'Id Proyek',
            'id_tenaga_ahli' => 'Id Tenaga Ahli',
            'id_penugasan' => 'Id Penugasan',
            'periode_dari' => 'Periode Dari',
            'periode_hingga' => 'Periode Hingga',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProyek()
    {
        return $this->hasOne(Proyek::className(), ['id' => 'id_proyek']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenagaAhli()
    {
        return $this->hasOne(TenagaAhli::className(), ['id' => 'id_tenaga_ahli']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPenugasan()
    {
        return $this->hasOne(Penugasan::className(), ['id' => 'id_penugasan']);
    }
}
