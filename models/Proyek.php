<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "proyek".
 *
 * @property integer $id
 * @property string $kode_lelang
 * @property string $nama_proyek
 * @property string $nomor_kontrak
 * @property string $tanggal_kontrak
 * @property string $periode_awal
 * @property string $periode_akhir
 * @property string $status
 * @property string $status_keterangan
 * @property string $keterangan
 * @property integer $id_instansi
 * @property integer $id_satminkal
 * @property integer $id_satuan_kerja
 * @property integer $id_sub_bidang
 * @property string $aanwizjing
 * @property string $penandatanganan_shortlist
 * @property string $lokasi
 * @property string $metode_pengadaan
 * @property string $metode_kualifikasi
 * @property string $metode_dokumen
 * @property string $metode_evaluasi
 * @property string $cara_pembayaran
 * @property string $pembebanan_tahun_anggaran
 * @property string $sumber_dana
 * @property string $nilai_pagu_paket
 * @property string $nilai_hps_paket
 * @property string $nilai_kontrak
 * @property string $anggaran
 * @property string $bobot_teknis
 * @property string $bobot_biaya
 * @property double $score
 * @property string $dokumen_pakta_integritas
 * @property string $dokumen_surat_minat
 * @property string $dokumen_surat_pernyataan
 * @property string $sharing
 * @property string $tanggal_mulai
 * @property string $tanggal_selesai
 * @property string $uraian_pekerjaan
 * @property string $lingkup_pekerjaan
 * @property string $nomor_bast
 * @property string $tanggal_bast
 * @property string $nomor_spmk
 *
 * @property Instansi $instansi
 * @property SatuanKerja $satuanKerja
 * @property SubBidang $subBidang
 * @property Satminkal $satminkal
 * @property ProyekTenagaAhli[] $proyekTenagaAhlis
 */
class Proyek extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'proyek';
    }

    public function rules()
    {
        return [
            //id

            //kode_lelang
            [['kode_lelang'], 'required'],
            [['kode_lelang'], 'string', 'max' => 16],

            //nama_proyek
            [['nama_proyek'], 'required'],
            [['nama_proyek'], 'string', 'max' => 128],

            //nomor_kontrak
            [['nomor_kontrak'], 'string', 'max' => 128],

            //tanggal_kontrak
            [['tanggal_kontrak'], 'safe'],

            //periode_awal
            [['periode_awal'], 'safe'],

            //periode_akhir
            [['periode_akhir'], 'safe'],

            //status
            [['status'], 'required'],
            [['status'], 'string'],

            //status_keterangan
            [['status_keterangan'], 'string'],

            //keterangan
            [['keterangan'], 'required'],
            [['keterangan'], 'string'],

            //id_instansi
            [['id_instansi'], 'required'],
            [['id_instansi'], 'integer'],
            [['id_instansi'], 'exist', 'skipOnError' => true, 'targetClass' => Instansi::className(), 'targetAttribute' => ['id_instansi' => 'id']],

            //id_satminkal
            [['id_satminkal'], 'required'],
            [['id_satminkal'], 'integer'],
            [['id_satminkal'], 'exist', 'skipOnError' => true, 'targetClass' => Satminkal::className(), 'targetAttribute' => ['id_satminkal' => 'id']],

            //id_satuan_kerja
            [['id_satuan_kerja'], 'required'],
            [['id_satuan_kerja'], 'integer'],
            [['id_satuan_kerja'], 'exist', 'skipOnError' => true, 'targetClass' => SatuanKerja::className(), 'targetAttribute' => ['id_satuan_kerja' => 'id']],

            //id_sub_bidang
            [['id_sub_bidang'], 'required'],
            [['id_sub_bidang'], 'integer'],
            [['id_sub_bidang'], 'exist', 'skipOnError' => true, 'targetClass' => SubBidang::className(), 'targetAttribute' => ['id_sub_bidang' => 'id']],

            //aanwizjing
            [['aanwizjing'], 'required'],
            [['aanwizjing'], 'string', 'max' => 64],

            //penandatanganan_shortlist
            [['penandatanganan_shortlist'], 'required'],
            [['penandatanganan_shortlist'], 'string', 'max' => 64],

            //lokasi
            [['lokasi'], 'required'],
            [['lokasi'], 'string'],

            //metode_pengadaan
            [['metode_pengadaan'], 'required'],
            [['metode_pengadaan'], 'string', 'max' => 64],

            //metode_kualifikasi
            [['metode_kualifikasi'], 'required'],
            [['metode_kualifikasi'], 'string', 'max' => 64],

            //metode_dokumen
            [['metode_dokumen'], 'required'],
            [['metode_dokumen'], 'string', 'max' => 64],

            //metode_evaluasi
            [['metode_evaluasi'], 'required'],
            [['metode_evaluasi'], 'string', 'max' => 64],

            //cara_pembayaran
            [['cara_pembayaran'], 'required'],
            [['cara_pembayaran'], 'string', 'max' => 64],

            //pembebanan_tahun_anggaran
            [['pembebanan_tahun_anggaran'], 'required'],
            [['pembebanan_tahun_anggaran'], 'string', 'max' => 64],

            //sumber_dana
            [['sumber_dana'], 'required'],
            [['sumber_dana'], 'string', 'max' => 64],

            //nilai_pagu_paket
            [['nilai_pagu_paket'], 'required'],
            [['nilai_pagu_paket'], 'string', 'max' => 64],

            //nilai_hps_paket
            [['nilai_hps_paket'], 'required'],
            [['nilai_hps_paket'], 'string', 'max' => 64],

            //nilai_kontrak
            [['nilai_kontrak'], 'string', 'max' => 64],

            //anggaran
            [['anggaran'], 'string', 'max' => 64],

            //bobot_teknis
            [['bobot_teknis'], 'required'],
            [['bobot_teknis'], 'string', 'max' => 8],

            //bobot_biaya
            [['bobot_biaya'], 'required'],
            [['bobot_biaya'], 'string', 'max' => 8],

            //score
            [['score'], 'number'],
            [['score'], 'required', 'on' => 'score'],

            //dokumen_pakta_integritas
            [['dokumen_pakta_integritas'], 'string', 'max' => 128],

            //dokumen_surat_minat
            [['dokumen_surat_minat'], 'string', 'max' => 128],

            //dokumen_surat_pernyataan
            [['dokumen_surat_pernyataan'], 'string', 'max' => 128],

            //sharing
            [['sharing'], 'string', 'max' => 128],

            //tanggal_mulai
            [['tanggal_mulai'], 'safe'],

            //tanggal_selesai
            [['tanggal_selesai'], 'safe'],

            //uraian_pekerjaan
            [['uraian_pekerjaan'], 'string'],

            //lingkup_pekerjaan
            [['lingkup_pekerjaan'], 'string'],

            //nomor_bast
            [['nomor_bast'], 'string', 'max' => 64],

            //tanggal_bast
            [['tanggal_bast'], 'safe'],

            //nomor_spmk
            [['nomor_spmk'], 'string', 'max' => 64],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kode_lelang' => 'Kode Lelang',
            'nama_proyek' => 'Nama Proyek',
            'nomor_kontrak' => 'Nomor Kontrak',
            'tanggal_kontrak' => 'Tanggal Kontrak',
            'periode_awal' => 'Periode Awal',
            'periode_akhir' => 'Periode Akhir',
            'status' => 'Status',
            'status_keterangan' => 'Status Keterangan',
            'keterangan' => 'Keterangan',
            'id_instansi' => 'Id Instansi',
            'id_satminkal' => 'Id Satminkal',
            'id_satuan_kerja' => 'Id Satuan Kerja',
            'id_sub_bidang' => 'Id Sub Bidang',
            'aanwizjing' => 'Aanwizjing',
            'penandatanganan_shortlist' => 'Penandatanganan Shortlist',
            'lokasi' => 'Lokasi',
            'metode_pengadaan' => 'Metode Pengadaan',
            'metode_kualifikasi' => 'Metode Kualifikasi',
            'metode_dokumen' => 'Metode Dokumen',
            'metode_evaluasi' => 'Metode Evaluasi',
            'cara_pembayaran' => 'Cara Pembayaran',
            'pembebanan_tahun_anggaran' => 'Pembebanan Tahun Anggaran',
            'sumber_dana' => 'Sumber Dana',
            'nilai_pagu_paket' => 'Nilai Pagu Paket',
            'nilai_hps_paket' => 'Nilai Hps Paket',
            'nilai_kontrak' => 'Nilai Kontrak',
            'anggaran' => 'Anggaran',
            'bobot_teknis' => 'Bobot Teknis',
            'bobot_biaya' => 'Bobot Biaya',
            'score' => 'Score',
            'dokumen_pakta_integritas' => 'Dokumen Pakta Integritas',
            'dokumen_surat_minat' => 'Dokumen Surat Minat',
            'dokumen_surat_pernyataan' => 'Dokumen Surat Pernyataan',
            'sharing' => 'Sharing',
            'tanggal_mulai' => 'Tanggal Mulai',
            'tanggal_selesai' => 'Tanggal Selesai',
            'uraian_pekerjaan' => 'Uraian Pekerjaan',
            'lingkup_pekerjaan' => 'Lingkup Pekerjaan',
            'nomor_bast' => 'Nomor Bast',
            'tanggal_bast' => 'Tanggal Bast',
            'nomor_spmk' => 'Nomor Spmk',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstansi()
    {
        return $this->hasOne(Instansi::className(), ['id' => 'id_instansi']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSatuanKerja()
    {
        return $this->hasOne(SatuanKerja::className(), ['id' => 'id_satuan_kerja']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubBidang()
    {
        return $this->hasOne(SubBidang::className(), ['id' => 'id_sub_bidang']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSatminkal()
    {
        return $this->hasOne(Satminkal::className(), ['id' => 'id_satminkal']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProyekTenagaAhlis()
    {
        return $this->hasMany(ProyekTenagaAhli::className(), ['id_proyek' => 'id']);
    }
}
