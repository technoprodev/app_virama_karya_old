<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "mitra_kerja".
 *
 * @property integer $id
 * @property string $nama_perusahaan
 * @property string $alamat_perusahaan
 */
class MitraKerja extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'mitra_kerja';
    }

    public function rules()
    {
        return [
            //id

            //nama_perusahaan
            [['nama_perusahaan'], 'required'],
            [['nama_perusahaan'], 'string', 'max' => 64],

            //alamat_perusahaan
            [['alamat_perusahaan'], 'required'],
            [['alamat_perusahaan'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_perusahaan' => 'Nama Perusahaan',
            'alamat_perusahaan' => 'Alamat Perusahaan',
        ];
    }
}
