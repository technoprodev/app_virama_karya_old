<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "identitas_perusahaan".
 *
 * @property integer $id
 * @property string $nama_perusahaan
 * @property string $status
 * @property string $alamat
 * @property string $nomor_telpon
 * @property string $nomor_fax
 * @property string $email_1
 * @property string $email_2
 * @property string $akta_pendirian_perusahaan_nomor_akta
 * @property string $akta_pendirian_perusahaan_tanggal
 * @property string $akta_pendirian_perusahaan_nama_notaris
 * @property string $akta_pendirian_perusahaan_nomor_pengesahan
 * @property string $akta_pendirian_perusahaan_tanggal_pengesahan
 * @property string $akta_perubahan_terakhir_nomor_akta
 * @property string $akta_perubahan_terakhir_nama_notaris
 * @property string $akta_perubahan_terakhir_tanggal
 * @property string $akta_perubahan_terakhir_nomor_pengesahan
 * @property string $akta_perubahan_terakhir_tanggal_pengesahan
 * @property string $iujk_masa_berlaku_dari
 * @property string $iujk_masa_berlaku_sampai
 * @property string $iujk_instansi_pemberi
 * @property string $iujk_perencanaan
 * @property string $iujk_pengawasan
 * @property string $iujk_konsultansi
 * @property string $sbu_konstruksi_masa_berlaku_dari
 * @property string $sbu_konstruksi_masa_berlaku_sampai
 * @property string $sbu_konstruksi_instansi_pemberi_izin
 * @property string $sbu_konstruksi_perencanaan_arsitektur
 * @property string $sbu_konstruksi_pengawasan_arsitektur
 * @property string $sbu_konstruksi_jasa_konsultansi_lainnya
 * @property string $sbu_konstruksi_perencana_penataan_ruang
 * @property string $sbu_konstruksi_pengawasan_penataan_ruang
 * @property string $sbu_konstruksi_perencanaan_rekayasa
 * @property string $sbu_konstruksi_pengawasan_rekayasa
 * @property string $sbu_konstruksi_konsultan_spesialis
 * @property string $sbu_non_konstruksi_ljspdbt
 * @property string $sbu_non_konstruksi_ljskm
 * @property string $sbu_non_konstruksi_ljskm_masa_berlaku_dari
 * @property string $sbu_non_konstruksi_ljskm_masa_berlaku_sampai
 * @property string $sbu_non_konstruksi_ljskm_instansi_pemberi_izin
 * @property string $sbu_non_konstruksi_pengembangan_pertanian_dan_pedesaan
 * @property string $sbu_non_konstruksi_transportasi
 * @property string $sbu_non_konstruksi_telematika
 * @property string $sbu_non_konstruksi_pendidikan
 * @property string $sbu_non_konstruksi_pendidikan_masa_berlaku_dari
 * @property string $sbu_non_konstruksi_pendidikan_masa_berlaku_sampai
 * @property string $sbu_non_konstruksi_pendidikan_instansi_pemberi_izin
 * @property string $pajak_nomor_pokok_wajib_pajak
 * @property string $pajak_surat_pengukuhan_kena_pajak
 * @property string $pajak_surat_keterangan_terdaftar
 * @property string $pajak_bpptt_nomor
 * @property string $pajak_bpptt_tanggal
 * @property string $pajak_skf_nomor
 * @property string $pajak_skf_tanggal
 */
class IdentitasPerusahaan extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'identitas_perusahaan';
    }

    public function rules()
    {
        return [
            //id

            //nama_perusahaan
            [['nama_perusahaan'], 'required'],
            [['nama_perusahaan'], 'string', 'max' => 64],

            //status
            [['status'], 'required'],
            [['status'], 'string'],

            //alamat
            [['alamat'], 'required'],
            [['alamat'], 'string'],

            //nomor_telpon
            [['nomor_telpon'], 'required'],
            [['nomor_telpon'], 'string', 'max' => 16],

            //nomor_fax
            [['nomor_fax'], 'required'],
            [['nomor_fax'], 'string', 'max' => 16],

            //email_1
            [['email_1'], 'required'],
            [['email_1'], 'string', 'max' => 32],

            //email_2
            [['email_2'], 'required'],
            [['email_2'], 'string', 'max' => 32],

            //akta_pendirian_perusahaan_nomor_akta
            [['akta_pendirian_perusahaan_nomor_akta'], 'required'],
            [['akta_pendirian_perusahaan_nomor_akta'], 'string', 'max' => 4],

            //akta_pendirian_perusahaan_tanggal
            [['akta_pendirian_perusahaan_tanggal'], 'required'],
            [['akta_pendirian_perusahaan_tanggal'], 'safe'],

            //akta_pendirian_perusahaan_nama_notaris
            [['akta_pendirian_perusahaan_nama_notaris'], 'required'],
            [['akta_pendirian_perusahaan_nama_notaris'], 'string', 'max' => 64],

            //akta_pendirian_perusahaan_nomor_pengesahan
            [['akta_pendirian_perusahaan_nomor_pengesahan'], 'required'],
            [['akta_pendirian_perusahaan_nomor_pengesahan'], 'string', 'max' => 64],

            //akta_pendirian_perusahaan_tanggal_pengesahan
            [['akta_pendirian_perusahaan_tanggal_pengesahan'], 'required'],
            [['akta_pendirian_perusahaan_tanggal_pengesahan'], 'string', 'max' => 64],

            //akta_perubahan_terakhir_nomor_akta
            [['akta_perubahan_terakhir_nomor_akta'], 'required'],
            [['akta_perubahan_terakhir_nomor_akta'], 'string', 'max' => 4],

            //akta_perubahan_terakhir_nama_notaris
            [['akta_perubahan_terakhir_nama_notaris'], 'required'],
            [['akta_perubahan_terakhir_nama_notaris'], 'string', 'max' => 64],

            //akta_perubahan_terakhir_tanggal
            [['akta_perubahan_terakhir_tanggal'], 'required'],
            [['akta_perubahan_terakhir_tanggal'], 'safe'],

            //akta_perubahan_terakhir_nomor_pengesahan
            [['akta_perubahan_terakhir_nomor_pengesahan'], 'required'],
            [['akta_perubahan_terakhir_nomor_pengesahan'], 'string', 'max' => 64],

            //akta_perubahan_terakhir_tanggal_pengesahan
            [['akta_perubahan_terakhir_tanggal_pengesahan'], 'required'],
            [['akta_perubahan_terakhir_tanggal_pengesahan'], 'string', 'max' => 64],

            //iujk_masa_berlaku_dari
            [['iujk_masa_berlaku_dari'], 'required'],
            [['iujk_masa_berlaku_dari'], 'safe'],

            //iujk_masa_berlaku_sampai
            [['iujk_masa_berlaku_sampai'], 'required'],
            [['iujk_masa_berlaku_sampai'], 'safe'],

            //iujk_instansi_pemberi
            [['iujk_instansi_pemberi'], 'required'],
            [['iujk_instansi_pemberi'], 'string', 'max' => 64],

            //iujk_perencanaan
            [['iujk_perencanaan'], 'required'],
            [['iujk_perencanaan'], 'string', 'max' => 64],

            //iujk_pengawasan
            [['iujk_pengawasan'], 'required'],
            [['iujk_pengawasan'], 'string', 'max' => 64],

            //iujk_konsultansi
            [['iujk_konsultansi'], 'required'],
            [['iujk_konsultansi'], 'string', 'max' => 64],

            //sbu_konstruksi_masa_berlaku_dari
            [['sbu_konstruksi_masa_berlaku_dari'], 'required'],
            [['sbu_konstruksi_masa_berlaku_dari'], 'safe'],

            //sbu_konstruksi_masa_berlaku_sampai
            [['sbu_konstruksi_masa_berlaku_sampai'], 'required'],
            [['sbu_konstruksi_masa_berlaku_sampai'], 'safe'],

            //sbu_konstruksi_instansi_pemberi_izin
            [['sbu_konstruksi_instansi_pemberi_izin'], 'required'],
            [['sbu_konstruksi_instansi_pemberi_izin'], 'string', 'max' => 128],

            //sbu_konstruksi_perencanaan_arsitektur
            [['sbu_konstruksi_perencanaan_arsitektur'], 'required'],
            [['sbu_konstruksi_perencanaan_arsitektur'], 'string', 'max' => 128],

            //sbu_konstruksi_pengawasan_arsitektur
            [['sbu_konstruksi_pengawasan_arsitektur'], 'required'],
            [['sbu_konstruksi_pengawasan_arsitektur'], 'string', 'max' => 128],

            //sbu_konstruksi_jasa_konsultansi_lainnya
            [['sbu_konstruksi_jasa_konsultansi_lainnya'], 'required'],
            [['sbu_konstruksi_jasa_konsultansi_lainnya'], 'string', 'max' => 128],

            //sbu_konstruksi_perencana_penataan_ruang
            [['sbu_konstruksi_perencana_penataan_ruang'], 'required'],
            [['sbu_konstruksi_perencana_penataan_ruang'], 'string', 'max' => 128],

            //sbu_konstruksi_pengawasan_penataan_ruang
            [['sbu_konstruksi_pengawasan_penataan_ruang'], 'required'],
            [['sbu_konstruksi_pengawasan_penataan_ruang'], 'string', 'max' => 128],

            //sbu_konstruksi_perencanaan_rekayasa
            [['sbu_konstruksi_perencanaan_rekayasa'], 'required'],
            [['sbu_konstruksi_perencanaan_rekayasa'], 'string', 'max' => 128],

            //sbu_konstruksi_pengawasan_rekayasa
            [['sbu_konstruksi_pengawasan_rekayasa'], 'required'],
            [['sbu_konstruksi_pengawasan_rekayasa'], 'string', 'max' => 128],

            //sbu_konstruksi_konsultan_spesialis
            [['sbu_konstruksi_konsultan_spesialis'], 'required'],
            [['sbu_konstruksi_konsultan_spesialis'], 'string', 'max' => 128],

            //sbu_non_konstruksi_ljspdbt
            [['sbu_non_konstruksi_ljspdbt'], 'required'],
            [['sbu_non_konstruksi_ljspdbt'], 'string', 'max' => 64],

            //sbu_non_konstruksi_ljskm
            [['sbu_non_konstruksi_ljskm'], 'required'],
            [['sbu_non_konstruksi_ljskm'], 'string', 'max' => 64],

            //sbu_non_konstruksi_ljskm_masa_berlaku_dari
            [['sbu_non_konstruksi_ljskm_masa_berlaku_dari'], 'required'],
            [['sbu_non_konstruksi_ljskm_masa_berlaku_dari'], 'safe'],

            //sbu_non_konstruksi_ljskm_masa_berlaku_sampai
            [['sbu_non_konstruksi_ljskm_masa_berlaku_sampai'], 'required'],
            [['sbu_non_konstruksi_ljskm_masa_berlaku_sampai'], 'safe'],

            //sbu_non_konstruksi_ljskm_instansi_pemberi_izin
            [['sbu_non_konstruksi_ljskm_instansi_pemberi_izin'], 'required'],
            [['sbu_non_konstruksi_ljskm_instansi_pemberi_izin'], 'string', 'max' => 64],

            //sbu_non_konstruksi_pengembangan_pertanian_dan_pedesaan
            [['sbu_non_konstruksi_pengembangan_pertanian_dan_pedesaan'], 'required'],
            [['sbu_non_konstruksi_pengembangan_pertanian_dan_pedesaan'], 'string', 'max' => 64],

            //sbu_non_konstruksi_transportasi
            [['sbu_non_konstruksi_transportasi'], 'required'],
            [['sbu_non_konstruksi_transportasi'], 'string', 'max' => 64],

            //sbu_non_konstruksi_telematika
            [['sbu_non_konstruksi_telematika'], 'required'],
            [['sbu_non_konstruksi_telematika'], 'string', 'max' => 64],

            //sbu_non_konstruksi_pendidikan
            [['sbu_non_konstruksi_pendidikan'], 'required'],
            [['sbu_non_konstruksi_pendidikan'], 'string', 'max' => 64],

            //sbu_non_konstruksi_pendidikan_masa_berlaku_dari
            [['sbu_non_konstruksi_pendidikan_masa_berlaku_dari'], 'required'],
            [['sbu_non_konstruksi_pendidikan_masa_berlaku_dari'], 'safe'],

            //sbu_non_konstruksi_pendidikan_masa_berlaku_sampai
            [['sbu_non_konstruksi_pendidikan_masa_berlaku_sampai'], 'required'],
            [['sbu_non_konstruksi_pendidikan_masa_berlaku_sampai'], 'safe'],

            //sbu_non_konstruksi_pendidikan_instansi_pemberi_izin
            [['sbu_non_konstruksi_pendidikan_instansi_pemberi_izin'], 'required'],
            [['sbu_non_konstruksi_pendidikan_instansi_pemberi_izin'], 'string', 'max' => 64],

            //pajak_nomor_pokok_wajib_pajak
            [['pajak_nomor_pokok_wajib_pajak'], 'required'],
            [['pajak_nomor_pokok_wajib_pajak'], 'string', 'max' => 64],

            //pajak_surat_pengukuhan_kena_pajak
            [['pajak_surat_pengukuhan_kena_pajak'], 'required'],
            [['pajak_surat_pengukuhan_kena_pajak'], 'string', 'max' => 64],

            //pajak_surat_keterangan_terdaftar
            [['pajak_surat_keterangan_terdaftar'], 'required'],
            [['pajak_surat_keterangan_terdaftar'], 'string', 'max' => 64],

            //pajak_bpptt_nomor
            [['pajak_bpptt_nomor'], 'required'],
            [['pajak_bpptt_nomor'], 'string', 'max' => 64],

            //pajak_bpptt_tanggal
            [['pajak_bpptt_tanggal'], 'required'],
            [['pajak_bpptt_tanggal'], 'safe'],

            //pajak_skf_nomor
            [['pajak_skf_nomor'], 'required'],
            [['pajak_skf_nomor'], 'string', 'max' => 64],

            //pajak_skf_tanggal
            [['pajak_skf_tanggal'], 'required'],
            [['pajak_skf_tanggal'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_perusahaan' => 'Nama Perusahaan',
            'status' => 'Status',
            'alamat' => 'Alamat',
            'nomor_telpon' => 'Nomor Telpon',
            'nomor_fax' => 'Nomor Fax',
            'email_1' => 'Email 1',
            'email_2' => 'Email 2',

            'akta_pendirian_perusahaan_nomor_akta' => 'Nomor Akta',
            'akta_pendirian_perusahaan_tanggal' => 'Tanggal',
            'akta_pendirian_perusahaan_nama_notaris' => 'Nama Notaris',
            'akta_pendirian_perusahaan_nomor_pengesahan' => 'Nomor Pengesahan',
            'akta_pendirian_perusahaan_tanggal_pengesahan' => 'Tanggal Pengesahan',

            'akta_perubahan_terakhir_nomor_akta' => 'Nomor Akta',
            'akta_perubahan_terakhir_nama_notaris' => 'Nama Notaris',
            'akta_perubahan_terakhir_tanggal' => 'Tanggal',
            'akta_perubahan_terakhir_nomor_pengesahan' => 'Nomor Pengesahan',
            'akta_perubahan_terakhir_tanggal_pengesahan' => 'Tanggal Pengesahan',

            'iujk_masa_berlaku_dari' => 'Masa Berlaku Dari',
            'iujk_masa_berlaku_sampai' => 'Masa Berlaku Sampai',
            'iujk_instansi_pemberi' => 'Instansi Pemberi',
            'iujk_perencanaan' => 'Perencanaan',
            'iujk_pengawasan' => 'Pengawasan',
            'iujk_konsultansi' => 'Konsultansi',

            'sbu_konstruksi_masa_berlaku_dari' => 'Masa Berlaku Dari',
            'sbu_konstruksi_masa_berlaku_sampai' => 'Masa Berlaku Sampai',
            'sbu_konstruksi_instansi_pemberi_izin' => 'Instansi Pemberi Izin',
            'sbu_konstruksi_perencanaan_arsitektur' => 'Perencanaan Arsitektur',
            'sbu_konstruksi_pengawasan_arsitektur' => 'Pengawasan Arsitektur',
            'sbu_konstruksi_jasa_konsultansi_lainnya' => 'Jasa Konsultansi Lainnya',
            'sbu_konstruksi_perencana_penataan_ruang' => 'Perencana Penataan Ruang',
            'sbu_konstruksi_pengawasan_penataan_ruang' => 'Pengawasan Penataan Ruang',
            'sbu_konstruksi_perencanaan_rekayasa' => 'Perencanaan Rekayasa',
            'sbu_konstruksi_pengawasan_rekayasa' => 'Pengawasan Rekayasa',
            'sbu_konstruksi_konsultan_spesialis' => 'Konsultan Spesialis',

            'sbu_non_konstruksi_ljspdbt' => 'Layanan Jasa Studi, Penelitian dan Bantuan Teknik',

            'sbu_non_konstruksi_ljskm' => 'Layanan Jasa Konstruksi Manajemen',
            'sbu_non_konstruksi_ljskm_masa_berlaku_dari' => 'Masa Berlaku Dari',
            'sbu_non_konstruksi_ljskm_masa_berlaku_sampai' => 'Masa Berlaku Sampai',
            'sbu_non_konstruksi_ljskm_instansi_pemberi_izin' => 'Instansi Pemberi Izin',

            'sbu_non_konstruksi_pengembangan_pertanian_dan_pedesaan' => 'Pengembangan Pertanian Dan Pedesaan',
            'sbu_non_konstruksi_transportasi' => 'Transportasi',
            'sbu_non_konstruksi_telematika' => 'Telematika',

            'sbu_non_konstruksi_pendidikan' => 'Pendidikan',
            'sbu_non_konstruksi_pendidikan_masa_berlaku_dari' => 'Masa Berlaku Dari',
            'sbu_non_konstruksi_pendidikan_masa_berlaku_sampai' => 'Masa Berlaku Sampai',
            'sbu_non_konstruksi_pendidikan_instansi_pemberi_izin' => 'Instansi Pemberi Izin',

            'pajak_nomor_pokok_wajib_pajak' => 'Nomor Pokok Wajib Pajak',
            'pajak_surat_pengukuhan_kena_pajak' => 'Surat Pengukuhan Kena Pajak',
            'pajak_surat_keterangan_terdaftar' => 'Surat Keterangan Terdaftar',

            'pajak_bpptt_nomor' => 'Nomor',
            'pajak_bpptt_tanggal' => 'Tanggal',

            'pajak_skf_nomor' => 'Nomor',
            'pajak_skf_tanggal' => 'Tanggal',
        ];
    }
}
