<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "bidang".
 *
 * @property integer $id
 * @property string $nama_bidang
 *
 * @property SubBidang[] $subBidangs
 */
class Bidang extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'bidang';
    }

    public function rules()
    {
        return [
            //id

            //nama_bidang
            [['nama_bidang'], 'required'],
            [['nama_bidang'], 'string', 'max' => 64],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_bidang' => 'Nama Bidang',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubBidangs()
    {
        return $this->hasMany(SubBidang::className(), ['id_bidang' => 'id']);
    }
}
